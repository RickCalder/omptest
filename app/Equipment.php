<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    public function location() {
        return $this->hasOne('App\Company','id', 'location_id');
    }
}

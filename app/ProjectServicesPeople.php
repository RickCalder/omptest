<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectServicesPeople extends Model
{
    public function company_person() {
        return $this->hasOne('App\CompanyPerson','id', 'company_person_id');
    }

    public function service_role() {
        return $this->hasOne('App\ServiceRoles', 'id', 'service_role_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyAddresses extends Model
{

  public function addressToString() {
    $a = $this->address;

    $unit = $a->unit ?? "";
    $civic = $a->civic_number ?? "";
    $street = $a->street_name ?? "";
    $street_type = $a->street_type->name ?? "";
    $city = $a->city->name  ?? "";
    $province = $a->city->province->abbreviation ?? "";
    $postal_code = $a->postal_code ?? "";

    if($unit !== '' ) {
      $address = $unit . '-' . $civic . ' ' . $street .' ' . $street_type . ' ' . $city . ' ' . $province . ' ' . $postal_code;
    } else {
      $address = $civic . ' ' . $street .' ' . $street_type . ' ' . $city . ' ' . $province . ' ' . $postal_code;
    }
    
    return $address;
  }

  public function addressToString2() {
    $a = $this->address;

    $unit = $a->unit ?? "";
    $civic = $a->civic_number ?? "";
    $street = $a->street_name ?? "";
    $street_type = $a->street_type->name ?? "";
    $city = $a->city->name  ?? "";
    $province = $a->city->province->abbreviation ?? "";
    $postal_code = $a->postal_code ?? "";

    if($unit !== '' ) {
      $address = $unit . '-' . $civic . ' ' . $street .' ' . $street_type . '~' . $city . ' ' . $province . ' ' . $postal_code;
    } else {
      $address = $civic . ' ' . $street .' ' . $street_type . '~' . $city . ' ' . $province . ' ' . $postal_code;
    }
    
    return $address;
  }

  public function newAddressToString() {
    $a = $this->address;
    // dd($a);
    $unit = $a->unit_number ?? "";
    $address1 = $a->address1 ?? "";
    $address2 = $a->address2 ?? "";
    $city = $a->city  ?? "";
    $province = $a->state ?? "";
    $postal_code = $a->postcode ?? "";

    if($unit !== '' ) {
      $address = $unit . '-' . $address1 . ' '. $address2. ' ' . $city . ' ' . $province . ' ' . $postal_code;
    } else {
      $address = $address1 . ' '. $address2. ' ' . $city . ' ' . $province . ' ' . $postal_code;
    }
    
    return $address;
  }

  public function address() {
    return $this->hasOne('App\NewAddress', 'id', 'address_id');
  }

}

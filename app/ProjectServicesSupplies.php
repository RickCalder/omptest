<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectServicesSupplies extends Model
{
    public function supply() {
        return $this->hasOne('App\Supplies', 'id', 'supply_id');
    }
}

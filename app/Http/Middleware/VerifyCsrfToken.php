<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'project-service/get-projects',
        'project-service/get-customers',
        'project-service/get-sub-customers',
        'project_service/ajax_add_new_address'
    ];
}

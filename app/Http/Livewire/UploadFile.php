<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\ProjectServicesFiles;


class UploadFile extends Component{
    use WithFileUploads;
 
    public $file, $fileName, $projectId, $serviceId;
 
    public function save()
    {
        // dd($this->projectId);
        $this->validate([
          'file' => 'required|mimes:png,jpg,jpeg,csv,txt,xlx,xls,pdf,docx|max:10240'
        ]);
        $path = 'projects/'.$this->projectId.'/services/'.$this->serviceId;
        // dd($path);
 
        $test = $this->file->storeAs($path, $this->file->getClientOriginalName());
        // $test = $this->file->store('test');
// dd($test);
        $dbUpdate = new ProjectServicesFiles;
        $dbUpdate->file_url =  $path . '/' . $this->file->getClientOriginalName();
        $dbUpdate->old_id = 0;
        $dbUpdate->project_service_id = $this->serviceId;
        $dbUpdate->name = $this->fileName;
        $dbUpdate->owned_by = auth()->user()->id;
        $dbUpdate->mime_type = $this->file->getMimeType();
        $dbUpdate->save();

        session()->flash('message', 'File uploaded successfully.');
    }
}
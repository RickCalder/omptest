<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Equipment;
use App\Company;

class EquipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $equipment = Equipment::orderBy('name', 'ASC')->where('archived', '=', 0)->with('location')->paginate(50);
        return view('equipment.index', ['equipment'=>$equipment]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locations = [];
        $user_location = auth()->user()->primary_location;
        if($user_location === 0){
            $locations = Company::where('type_id', '=', 2)->orderBy('name')->get();
        } else {
            $locations = Company::where('type_id', '=', 2)
                ->where('id', '=', $user_location)
                ->orderBy('name')
                ->get();
        }
        
        return view('equipment.create', ['locations' => $locations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $equipment = New Equipment;
        $equipment->old_id = 0;
        $equipment->name = $request->name;
        $equipment->description = $request->description;
        $equipment->location_id = $request->location;
        $equipment->save();
        
        return view('equipment.show', ['equipment'=>$equipment]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $equipment = Equipment::where('id', '=', $id)->first();
        
        return view('equipment.show', ['equipment'=>$equipment]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit_equipment_ps(Request $request)
    // {
    //     dd($request);
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

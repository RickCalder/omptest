<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Company;
use App\CompanyPerson;
use App\Person;

class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $people = CompanyPerson::where('status', '=', 'active')
        ->with('person')
        ->with('company')
        ->paginate(50);

        $branches = Company::where('type_id', '=', 2)->orderBy('name')->get();

        return view('people.index', ['people'=>$people, 'branches'=>$branches]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('people.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      // dd($request->all());
        $person = new Person;
        $person->old_id = 0;
        $person->title = $request->title;
        $person->first_name = $request->first_name;
        $person->middle_name = $request->middle_name;
        $person->last_name = $request->last_name;
        $person->common_name = $request->first_name . ' ' . $request->last_name;
        $person->email = $request->email;
        $person->phone = $request->phone;
        $person->mobile_phone = $request->mobile_phone;
        $person->notes = $request->notes;
        $person->emerg_contact = $request->emerg_contact;
        // $person->born_on = $request->born_on;
        // $person->emergency_contact_id = $request->emergency_contact_id;
        // $person->is_canadian_resident = $request->is_canadian_resident;
        // $person->license_abstract_permission = $request->license_abstract_permission;
        $person->save();
// dd($person);
        $cPerson = new CompanyPerson;
        $cPerson->old_id = 0;
        $cPerson->company_id = $request->company_id;
        $cPerson->person_id = $person->id;
        $cPerson->emerg_contact = $person->emerg_contact;
        // $cPerson->position = $request->position;
        // $cPerson->reports_to = $request->reports_to_id;
        // $cPerson->phone = $request->phone;
        // $cPerson->phone1 = $request->mobile_phone;
        // $cPerson->email = $request->email;
        // $cPerson->status = $request->status;
        // $cPerson->hired_on = $request->hired_on;
        // $cPerson->is_payroll = $request->is_payroll;
        // $cPerson->is_salary = $request->is_salary;
        // $cPerson->is_wheniwork = $request->is_wheniwork;
        // $cPerson->wheniwork_id = $request->wheniwork_id;
        // $cPerson->is_contractor = $request->is_contractor;
        // $cPerson->payweb_emp_id = $request->payweb_emp_id;
        // $cPerson->department = $request->department;
        $cPerson->save();
        
        return redirect('/people/' . $cPerson->id);
    }

    public function add_person_ajax(Request $request) {
      $person = new Person;
      $person->old_id = 0;
      $person->title = $request->title;
      $person->first_name = $request->first_name;
      $person->middle_name = $request->middle_name;
      $person->last_name = $request->last_name;
      $person->common_name = $request->first_name . ' ' . $request->last_name;
      $person->email = $request->email;
      $person->phone = $request->phone;
      $person->emerg_contact = $request->emerg_contact;
      $person->save();

      
      $cPerson = new CompanyPerson;
      $cPerson->old_id = 0;
      $cPerson->company_id = $request->company_id;
      $cPerson->person_id = $person->id;
      $cPerson->phone = $request->phone;
      $cPerson->email = $request->email;
      $cPerson->emerg_contact = $request->emerg_contact;
      $cPerson->status = 'Active';
      $cPerson->save();
      $returnData = [];
      $returnData['name'] = $person->first_name . ' ' . $person->last_name;
      $returnData['id'] = $cPerson->id;
      return response()->json($returnData);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $person = CompanyPerson::where('id', '=', $id)
        ->with('person')
        ->with('company')
        ->first();
  
        return view('people.show', ['person'=>$person]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $person = CompanyPerson::where('id', '=', $id)
      ->with('person')
      ->with('company')
      ->first();


      // dd($person);

      return view('people.edit', ['person'=>$person]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $cPerson = CompanyPerson::where('id', '=', $id)->first();
      $person = Person::where('id', $cPerson->person_id)->first();

      $cPerson->old_id = 0;
      $cPerson->company_id = $request->company_id;
      $cPerson->person_id = $person->id;
      $cPerson->position = $request->position;
      $cPerson->reports_to = $request->reports_to_id;
      $cPerson->phone = $request->phone;
      $cPerson->phone1 = $request->mobile_phone;
      $cPerson->email = $request->email;
      $cPerson->status = $request->status;
      $cPerson->hired_on = $request->hired_on;
      $cPerson->is_payroll = $request->is_payroll;
      $cPerson->is_salary = $request->is_salary;
      $cPerson->is_wheniwork = $request->is_wheniwork;
      $cPerson->wheniwork_id = $request->wheniwork_id;
      $cPerson->is_contractor = $request->is_contractor;
      $cPerson->payweb_emp_id = $request->payweb_emp_id;
      $cPerson->department = $request->department;
      $cPerson->save();

      $person->old_id = 0;
      $person->title = $request->title;
      $person->first_name = $request->first_name;
      $person->middle_name = $request->middle_name;
      $person->last_name = $request->last_name;
      $person->common_name = $request->first_name . ' ' . $request->last_name;
      $person->email = $request->email;
      $person->phone = $request->phone;
      $person->mobile_phone = $request->mobile_phone;
      $person->notes = $request->notes;
      $person->born_on = $request->born_on;
      $person->emergency_contact_id = $request->emergency_contact_id;
      $person->is_canadian_resident = $request->is_canadian_resident;
      $person->license_abstract_permission = $request->license_abstract_permission;
      $person->save();
      
      return redirect('/people/' . $cPerson->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
  public function get_booked_by(Request $request) {
    $booked_by = CompanyPerson::where('company_id', '=', $request->branch_id)
      ->where('status', '=', 'Active')
      ->with('person')
      ->get();

    $returnData =[];
    $i = 0;
    foreach($booked_by as $person) {
      $returnData[$i]['id'] = $person->id;
      $returnData[$i]['name'] = $person->person->first_name . ' ' .$person->person->last_name;
      $i++;
    }
    usort($returnData, function ($a, $b) {
      if ($a['name'] == $b['name']) {
          return 0;
      }

      return ($a['name'] < $b['name']) ? -1 : 1;
   });
    
    return response()->json(['booked_by'=> $returnData]);
  }

  public function get_managed_by(Request $request) {
    $managed_by = CompanyPerson::where('company_id', '=', $request->branch_id)
      ->orWhere('company_id', '=', $request->customer_id)
      ->where('status', '=', 'Active')
      ->with('person')
      ->get();

    $returnData =[];
    $i = 0;
    foreach($managed_by as $person) {
      $returnData[$i]['id'] = $person->id;
      $returnData[$i]['name'] = $person->person->first_name . ' ' .$person->person->last_name;
      $i++;
    }
    usort($returnData, function ($a, $b) {
      if ($a['name'] == $b['name']) {
          return 0;
      }

      return ($a['name'] < $b['name']) ? -1 : 1;
   });
    
    return response()->json(['managed_by'=> $returnData]);
  }

  
    
  public function person_autocomplete(Request $request) {
    $data = Person::select('id', 'first_name', 'last_name')
    ->where("first_name","LIKE","%{$request->input('query')}%")
    ->orWhere("last_name","LIKE","%{$request->input('query')}%")
    ->with('company_person')
    ->get();

    $returnData = [];
    $i = 0;
    foreach($data as $person) {
      if(isset($person->company_person->id)) {
        $returnData[$i]['id'] = $person->company_person->id;
        $returnData[$i]['name'] = $person->first_name . ' ' . $person->last_name;
        $i++;
      }
    }
    usort($returnData, function ($a, $b) {
      if ($a['name'] == $b['name']) {
          return 0;
      }

      return ($a['name'] < $b['name']) ? -1 : 1;
   });
    
    return response()->json($returnData);
  }

  public function employee_autocomplete(Request $request) {
    $data = Person::select('id', 'first_name', 'last_name', 'common_name')
    // ->where("first_name","LIKE","%{$request->input('query')}%")
    // ->orWhere("last_name","LIKE","%{$request->input('query')}%")
    ->orWhere("common_name","LIKE","%{$request->input('query')}%")
    ->with('company_person')
    ->get();

    $returnData = [];
    $i = 0;
    foreach($data as $person) {
      if(isset($person->company_person->id)) {
        $returnData[$i]['id'] = $person->company_person->id;
        $returnData[$i]['name'] = $person->first_name . ' ' . $person->last_name;
        $i++;
      }
    }
    usort($returnData, function ($a, $b) {
      if ($a['name'] == $b['name']) {
          return 0;
      }

      return ($a['name'] < $b['name']) ? -1 : 1;
   });
    
    return response()->json($returnData);
  }
}

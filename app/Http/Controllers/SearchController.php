<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;

use App\CompanyPeople;
use App\Person;
  
class SearchController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function autocomplete(Request $request)
    {
      

      $data = Person::select("first_name")
        ->where("first_name","LIKE","%{$request->input('query')}%")
        ->get();

      return response()->json((json_encode($data)));
    }
}
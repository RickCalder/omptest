<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Company;
use App\Project;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::orderBy('name')
        ->paginate(50);


        return view('companies.index', ['companies'=>$companies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $company = New Company;
        $company->old_id = 0;
        $company->name = $request->name;
        $company->type_id = $request->type_id;
        $company->email = $request->email;
        $company->url = $request->url;
        $company->phone = $request->phone;
        $company->fax = '';
        $company->parent_company_id = $request->parent_company;
        $company->note = $request->note;
        $company->save();
        return $this->show($company->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::where('id', '=', $id)
        ->first();
        $parent = [];
        $parent = Company::where('id', '=', $company->parent_company_id)->first();
        return view('companies.edit', ['company'=>$company, 'parent'=> $parent]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::where('id', '=', $id)
        ->first();
        $parent = [];
        $parent = Company::where('id', '=', $company->parent_company_id)->first();
        $projects = [];
        $projects = Project::where('customer_id', '=', $company->id)->get();
  
        return view('companies.show', ['company'=>$company, 'parent'=> $parent, 'projects'=>$projects]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $company = Company::where('id', '=', $id)
        ->first();
        $company->old_id = 0;
        $company->name = $request->name;
        $company->type_id = $request->type_id;
        $company->email = $request->email;
        $company->url = $request->url;
        $company->phone = $request->phone;
        $company->fax = '';
        $company->parent_company_id = $request->parent_company;
        $company->note = $request->note;
        
        $company->save();
        return $this->show($company->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
  public function customer_autocomplete(Request $request) {
    $data = Company::select('id', 'name')->where("name","LIKE","%{$request->input('query')}%")->get();
    return response()->json($data);
  }
}

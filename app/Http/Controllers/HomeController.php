<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cookie;

use Carbon\Carbon;
use App\Company;
use App\Project;
use App\ProjectService;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

     public function index(Request $request) { 
      // dd($request->all());
      if($request->session()->has('filters')){
      } else {
        $dashboardFilters = [
          'branch' => null,
          'status' => null,
          'project_service_id' => null,
          'auto_filter' => null,
          'customer' => null,
          'customer_id' => null,
          'sub_customer' => null,
          'collapsed' => null,
          "projects" => null,
          'start_date' => null,
          'end_date' => null,
          'project_service_id'=> null
        ];
        session(['filters' => json_encode($dashboardFilters)]);
      }
      if( count($request->all()) !== 0 && $request->reset !== 'on') {
        $dashboardFilters = [
          'branch' => $request->branch,
          'status' => $request->status,
          'project_service_id' => $request->project_service_id,
          'auto_filter' => $request->auto_filter,
          'customer' => $request->customer,
          'customer_id' => $request->customer_id,
          'sub_customer' => $request->sub_customer,
          'collapsed' => $request->collapsed,
          "projects" => $request->projects,
          'start_date' => $request->start_date,
          'end_date' => $request->end_date,
          'project_service_id' => $request->project_service_id
        ];
        session(['filters' => json_encode($dashboardFilters)]);
      } else if($request->reset ==='on'){
        $dashboardFilters = [
          'branch' => null,
          'status' => null,
          'project_service_id' => null,
          'auto_filter' => null,
          'customer' => null,
          'customer_id' => null,
          'sub_customer' => null,
          'collapsed' => null,
          "projects" => null,
          'start_date' => null,
          'end_date' => null,
          'project_service_id'=> null
        ];
        session(['filters' => json_encode($dashboardFilters)]);

      }
      $filters = json_decode(session('filters'));
      // dd($filters);
      // dd($request->all());

      $collapsed = $request->collapsed ?? 'off';
      // dd($collapsed);

      cookie()->queue(cookie()->forever('preferences', $collapsed));
      if($filters && $filters->branch != null) {
        $current_branch = $filters->branch;
      } else {
        $current_branch = null;
      }
      // dd($current_branch);
      $current_customer = null;
      $user_locations = [];
      
      if(auth()->user()->primary_location === 0) {
        $branches = Company::where('type_id', '=', 2)->with('apis')->orderBy('name')->get()->toArray();
        foreach($branches as $branch) {
          array_push($user_locations, $branch['id']);
        }
      } else {
        $branches = [];
        if(auth()->user()->primary_location !== null) {
          $userBranch = Company::where('id', '=', auth()->user()->primary_location)->first();
          $user_locations[0]=$userBranch['id'];
          $branches[0] = $userBranch;
          $x=1;
        } else {
          $x=0;
        }
        $user_branches = \App\CompanyUser::where('user_id', '=', auth()->user()->id)->with('companies')->get();
        if(count($user_branches) !==0) {
          foreach($user_branches as $branch) {
            foreach($branch->companies as $location) {
              $user_locations[$x] = $location['id'];
              $branches[$x] = $location;
              $x++;
            }
          }
        }
      }
      
      $customers = [];
      $sub_customers = [];
      $start = null;
      $end = null;
      $start_time = strtotime("-2 year", time());
      $end_time = strtotime("+1 year", time());
      $user_location = Auth()->user()->primary_location;
      $all_dates = '';


      // Main index, no filters applied
 
    //   if( empty( $request->except('_token') ) ) {
    //     if($user_location !== null) {
    //       $project_services = ProjectService::orderBy('occurs_on', 'DESC')
    //         ->whereDate('occurs_on', '=', date('Y-m-d'))
    //         ->where('status', '!=', 'cancelled')
    //         ->where('project_id', '>', 0)
    //         ->whereIn('branch_id', $user_locations)
    //         ->with('project')
    //         ->with('notes')
    //         ->paginate(50);
    //         // dd($project_services);

    //     }
    //     // dd(date('Y-m-d'));
    //       $customers = Company::where('type_id', '=', 1)
    //         ->orderBy('name', 'ASC')
    //         ->get();
  
    //     return view('dashboard', ['project_services'=>$project_services, 'branches'=> $branches, 'customers' => $customers,'current_branch' => $current_branch, 'current_customer' => $current_customer, 'sub_customers' => $sub_customers, 'start_date' => $start, 'end_date' => $end, 'current_sub_customer' => '', 'all_dates' =>$all_dates]);
    //  }

     // Filtered by SID, no other filters will applay if this is populated 

     if($filters->project_service_id !== null) {

      $project_services = ProjectService::where('id', '=', $filters->project_service_id)
      ->with('project')
      ->orderBy('occurs_on', 'DESC')
      ->get();
      $current_branch = Company::where('id', '=', $project_services[0]->branch_id)->first();
      // dd($current_branch);
      // dd($filters);
      $project_service_id = $filters->project_service_id;
      return view('dashboard', ['project_services'=>$project_services, 'branches'=> $branches, 'customers' => $customers,'current_branch' => $current_branch, 'current_customer' => $current_customer, 'sub_customers' => $sub_customers, 'start_date' => $start, 'end_date' => $end, 'current_sub_customer' => '', 'all_dates' =>$all_dates,  'status' => '', 'project_service_id' => $project_service_id]);
     }

     // Other filters
     if($filters) {
      $status = $filters->status;
      $branch = $filters->branch;
      $customer = $filters->customer_id ? $filters->customer_id : '';
      $project_req = $filters->projects ? $filters->projects : '';
      $sub_customer = $filters->sub_customer ? $filters->sub_customer : '';
      $start_query = $filters->start_date ? $filters->start_date : date("Y-m-d");
      $projects = [];
      $all_dates = $request->all_dates ? 'checked' : '';
      $subs = [];
      if($filters->start_date > $filters->end_date) {
        $end_query = $filters->end_date ? $filters->end_date : $filters->start_date;
      } else {
        $end_query = $filters->end_date ? $filters->end_date : date("Y-m-d");
      }
      $start = $filters->start_date;
      $project_service_id = $filters->project_service_id;


      if($filters->end_date ===  null ){
        $end = $filters->start_date;
      } else {
        $end = $filters->end_date;
      }
     } else {
      $status = $request->status;
      $branch = $request->branch;
      $customer = $request->customer_id ? $request->customer_id : '';
      $project_req = $request->projects ? $request->projects : '';
      $sub_customer = $request->sub_customer ? $request->sub_customer : '';
      $start_query = $request->start_date ? $request->start_date : date("Y-m-d");
      $projects = [];
      $all_dates = $request->all_dates ? 'checked' : '';
      $subs = [];
      $project_service_id = $filters->project_service_id;
      if($request->start_date > $request->end_date) {
        $end_query = $request->end_date ? $request->end_date : $request->start_date;
      } else {
        $end_query = $request->end_date ? $request->end_date : date("Y-m-d");
      }
      $start = $request->start_date;


      if($request->end_date ===  null ){
        $end = $request->start_date;
      } else {
        $end = $request->end_date;
      }
     }
      
      $customers = Company::where('type_id', '=', 1)
        ->orderBy('name', 'ASC')
        ->get();

      if($branch !== 'all') {
        $current_branch = Company::where('id', '=', $branch)->first();
      } else {
        $current_branch = [];
      }

      if($customer !== '') {
        $current_customer = Company::where('id', '=', $customer)->first();
        $projects = Project::where('customer_id', '=', $customer)->orderBy('name')->get();
        $sub_customers =[];
        $i = 0;
        foreach($projects as $project) {
          if($project->customer_customer_id !== null) {
            $sub_customers[$i] = $project->customer_customer_id;
            $i++;
          }
        }
        $sub_customers = array_unique($sub_customers);

        $subs = DB::table('companies')
        ->whereIn('id',$sub_customers)
        ->orderBy('name')
        ->get();

      } else {
        $current_customer = [];
      }

      if($sub_customer !== '') {
        $current_sub_customer = Company::where('id', '=', $sub_customer)->first();
        $projects = Project::where('customer_id', '=', $customer)
        ->where('customer_customer_id', '=', $sub_customer)
        ->get();
      } else {
        $current_sub_customer = [];
      }
      if($all_dates != 'checked') {
      $project_services = ProjectService::orderBy('occurs_on', 'DESC')
        ->when($status, function ($query, $status) {
            return $query->where('status', $status);
        })
        ->when($branch, function ($query, $branch) {
            if($branch === 'all') return;
            return $query->where('branch_id', $branch);
        })
        ->when($project_req, function ($query, $project_req) {
          // dd($project);
            return $query->where('project_id', $project_req);
        })
        ->where('occurs_on', '>=', $start_query)
        ->where('occurs_on', '<=', $end_query)
        ->whereHas('project', function(Builder $query) use($customer, $sub_customer) {
          if($customer !== '' && $sub_customer !== '') {
            $query->where('customer_id', '=', $customer)
            ->where('customer_customer_id', '=', $sub_customer);
          } else if($customer !== '' && $sub_customer == '') {
            $query->where('customer_id', '=', $customer);
          }
        })
        ->paginate(50);
        
      } else {
        $project_services = ProjectService::orderBy('occurs_on', 'DESC')
          ->when($status, function ($query, $status) {
              return $query->where('status', $status);
          })
          ->when($branch, function ($query, $branch) {
              if($branch === 'all') return;
              return $query->where('branch_id', $branch);
          })
          ->when($project_req, function ($query, $project_req) {
            // dd($project);
              return $query->where('project_id', $project_req);
          })
          ->whereHas('project', function(Builder $query) use($customer, $sub_customer) {
            if($customer !== '' && $sub_customer !== '') {
              $query->where('customer_id', '=', $customer)
              ->where('customer_customer_id', '=', $sub_customer);
            } else if($customer !== '' && $sub_customer == '') {
              $query->where('customer_id', '=', $customer);
            }
          })
          ->paginate(50);
        
      }
        // dd($current_branch);
        // dd($branches[1]->apis[0]->api_name);
        // dd(count($project_services));
      return view('dashboard', ['project_services'=>$project_services, 'branches'=> $branches, 'customers' => $customers,'current_branch' => $current_branch, 'current_customer' => $current_customer, 'current_sub_customer' => $current_sub_customer, 'start_date' => $start, 'end_date' => $end, 'sub_customers'=> $subs, 'projects' => $projects, 'current_project' => $project_req, 'all_dates' => $all_dates, 'status' => $status, 'project_service_id' => $project_service_id]);

    }
}

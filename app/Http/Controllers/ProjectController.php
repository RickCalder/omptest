<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Company;
use App\CompanyPerson;
use App\Person;
use App\Project;
use App\ProjectService;

class ProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request) {

      $user = \App\User::where('id','=',auth()->user()->id)->first();
      
      if(auth()->user()->primary_location === 0) {
        $branches = Company::where('type_id', '=', 2)->with('apis')->orderBy('name')->get();
      } else {
        $branches = [];
        if(auth()->user()->primary_location !== null) {
          $userBranch = Company::where('id', '=', auth()->user()->primary_location)->first();
          $branches[0]=$userBranch;
          $x=1;
        } else {
          $x=0;
        }
        $user_branches = \App\CompanyUser::where('user_id', '=', auth()->user()->id)->with('companies')->get();
        foreach($user_branches as $branch) {
          foreach($branch->companies as $location) {
            $branches[$x] = $location;
            $x++;
          }
        }
      }
      // $branches = collect($branches);
      // dd($branches);

      $current_branch = null;
      $current_customer = null;
      $current_subcustomer = null;
      // $branches = Company::where('type_id', '=', 2)->orderBy('name')->get();
      $branchIds = [];
      foreach($branches as $branch) {
        array_push($branchIds, $branch->id);
      }
      $sub_customers = [];
// dd($request->all());
      if(count($request->all()) ===0) {
        $projects = Project::whereIn('branch_id', $branchIds)
          ->orderBy('id', 'DESC')
          ->with('customer')
          ->with('sub_customer')
          ->with('branch')
          ->paginate(50);
      } else {
        $projects = Project::whereIn('branch_id', $branchIds)
          ->orderBy('id', 'DESC')
          ->with('customer')
          ->with('sub_customer')
          ->with('branch')
          ->get();

        if($request->branch !== null ) {
          $projects = $projects->where('branch_id', '=', $request->branch);
        }

        if($request->customer_id !== null) {
          $projects = $projects->where('customer_id','=', $request->customer_id);
        }
        if($request->sub_customer !== null ) {
          $projects = $projects->where('customer_customer_id', '=', $request->sub_customer);
        }
        $projects = $projects->paginator(50);
      }
      // dd($branchIds);

      return view('projects.index', ['projects'=>$projects, 'branches' => $branches, 'sub_customers' => $sub_customers]);
    }

    // dd($branchIds);

    public function show($id)
    {
      $project = Project::where('id', '=', $id)
      ->with('project_services')
      ->with('entered')
      ->with('booked')
      ->with('managed')
      ->first();
      // dd($project);
    
      // $project_services = ProjectService::orderBy('occurs_on', 'DESC')
      //   ->with('project')
      //   ->paginate(50);

      // dd($project_services[0]);
      return view('projects.project', ['project'=>$project]);
  }

  public function create(Request $request) {
    if($request->customer_id) {
      $customer = Company::where('id', '=', $request->customer_id)->first();
    } else {
      $customer = [];
    }

    if(auth()->user()->primary_location === 0) {
      $branches = Company::where('type_id', '=', 2)->with('apis')->orderBy('name')->get();
    } else {
      $branches = [];
      if(auth()->user()->primary_location !== null) {
        $userBranch = Company::where('id', '=', auth()->user()->primary_location)->first();
        $branches[0]=$userBranch;
        $x=1;
      } else {
        $x=0;
      }
      $user_branches = \App\CompanyUser::where('user_id', '=', auth()->user()->id)->with('companies')->get();
      foreach($user_branches as $branch) {
        foreach($branch->companies as $location) {
          $branches[$x] = $location;
          $x++;
        }
      }
    }
    
    $sub_customers = [];

    return view('projects.create',['branches' => $branches, 'sub_customers' => $sub_customers, 'customer' => $customer]);
  }

  public function store(Request $request) {
    
    // dd(auth()->user()->company_person->id);
    
    $project = new Project;
    $project->entered_by = auth()->user()->company_person->id;
    $project->name = $request->name;
    $project->branch_id = $request->branch;
    $project->customer_id = $request->customer_id;
    $project->customer_customer_id = $request->sub_customer_id;
    if($request->booked_by === null) {
      $project->booked_by = auth()->user()->company_person->id;
    } else {
      $project->booked_by = $request->booked_by;
    }
    
    if($request->managed_by === null) {
      $project->managed_by = auth()->user()->company_person->id;
    } else {
      $project->managed_by = $request->managed_by;
    }
    
   
    $project->po_number = $request->po_number;
    $project->default_quote_type = $request->default_quote_type;
    $project->old_id = 0;

    $project->save();
    return redirect('/projects/' . $project->id);

    dd($request->all(), $project);
  }

  public function edit($id) {
    $project = Project::where('id', '=', $id)
    ->with('project_services')
    ->with('entered')
    ->with('booked')
    ->with('managed')
    ->first();
    
          
    if(auth()->user()->primary_location === 0) {
      $branches = Company::where('type_id', '=', 2)->with('apis')->orderBy('name')->get();
    } else {
      $branches = [];
      if(auth()->user()->primary_location !== null) {
        $userBranch = Company::where('id', '=', auth()->user()->primary_location)->first();
        $branches[0]=$userBranch;
        $x=1;
      } else {
        $x=0;
      }
      $user_branches = \App\CompanyUser::where('user_id', '=', auth()->user()->id)->with('companies')->get();
      foreach($user_branches as $branch) {
        foreach($branch->companies as $location) {
          $branches[$x] = $location;
          $x++;
        }
      }
    }
    $customer = Company::where('id', '=', $project->customer_id)->first();
    $sub_customer = Company::where('id', '=', $project->customer_customer_id)->first();

    return view('projects.edit', ['project'=>$project, 'branches' => $branches, 'customer' => $customer, 'sub_customer' => $sub_customer]);
  }

  public function update(Request $request, $id) {
    // dd(auth()->user()->company_person);
    $project = Project::where('id', '=', $id)->first();
    $project->entered_by = auth()->user()->company_person->id;
    $project->name = $request->name;
    $project->branch_id = $request->branch;
    $project->customer_id = $request->customer_id;
    $project->customer_customer_id = $request->sub_customer_id;
    $project->booked_by = $request->booked_by;
    $project->managed_by = $request->managed_by;
    $project->po_number = $request->po_number;
    $project->default_quote_type = $request->default_quote_type;
    $project->old_id = 0;

    $project->save();
    return redirect('/projects/' . $project->id);
  }

}

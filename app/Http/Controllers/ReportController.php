<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\ProjectService;
use App\Company;

class ReportController extends Controller
{
    public function index() {
        $branches = Company::where('type_id', '=', 2)->orderBy('name')->get();
        return view('reports.index', ['branches' => $branches]);
    }

    public function supervisor_control(Request $request) {
        $project_service = ProjectService::where('id', '=', $request->sc_pid)
            ->with('project')
            ->with('notes')
            ->with('service')
            ->with('branch')
            ->with('files')
            ->with('people')
            ->with('addresses')
            ->with('service_equipment')
            ->with('service_supplies')
            ->first();
            // dd($project_service);
        return view('reports.supervisor_control', [ 'project_service' => $project_service]);
    }

    public function time_sheet(Request $request) {
        $project_service = ProjectService::where('id', '=', $request->ts_pid)
            ->with('project')
            ->with('notes')
            ->with('service')
            ->with('branch')
            ->with('files')
            ->with('people')
            ->with('addresses')
            ->with('service_equipment')
            ->with('service_supplies')
            ->first();
            // dd($project_service);
        return view('reports.time_sheet', [ 'project_service' => $project_service]);        
    }

    public function work_order(Request $request) {
        $project_service = ProjectService::where('id', '=', $request->wo_pid)
            ->with('project')
            ->with('notes')
            ->with('service')
            ->with('branch')
            ->with('files')
            ->with('people')
            ->with('addresses')
            ->with('service_equipment')
            ->with('service_supplies')
            ->first();
            // dd($project_service);
        return view('reports.work_order', [ 'project_service' => $project_service]);        
    }
    

    public function summary_dispatch(Request $request) {
        $branch = Company::where('id', '=', $request->branch)->first();
        $project_services = ProjectService::where('branch_id', '=', $request->branch)->where('occurs_on', '=', $request->date)
            ->with('project')
            ->with('notes')
            ->with('service')
            ->with('branch')
            ->with('files')
            ->with('people')
            ->with('addresses')
            ->with('service_equipment')
            ->with('service_supplies')
            ->get();

        $equipment = DB::table('project_services')
            ->where('branch_id', '=', $request->branch)
            ->where('occurs_on', '=', $request->date)
            ->join('project_services_equipment', 'project_services.id', '=', 'project_services_equipment.project_service_id')
            ->join('equipment', 'equipment.id', '=', 'project_services_equipment.equipment_id')
            // ->select('equipment.name', 'project_services_equipment.quantity_requested')
            ->select(['equipment.name', DB::raw("SUM(project_services_equipment.quantity_requested) as count")])
            ->groupBy('equipment.name')
            ->get();


        $people = DB::table('project_services')
            ->where('branch_id', '=', $request->branch)
            ->where('occurs_on', '=', $request->date)
            ->join('project_services_people', 'project_services_people.project_service_id', '=', 'project_services.id')
            ->join('service_roles', 'service_roles.id', '=', 'project_services_people.service_role_id')
            ->select('service_roles.name', DB::raw('COUNT(project_services_people.service_role_id) as count'))
            ->groupBy('service_roles.name')
            ->get();
        if(count($project_services) == 0) {
            return back()->with('error','No services for the location and date you selected');
        }
        return view('reports.summary_dispatch', [ 'branch' => $branch, 'project_services' => $project_services, 'equipment' => $equipment, 'people' => $people]);        
    }
}

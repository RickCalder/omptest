<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Supplies;

class SuppliesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $supplies = Supplies::orderBy('name')->where('archived', '=', 0)->with('location')->paginate(50);
        return view('supplies.index', ['supplies'=>$supplies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locations = [];
        $user_location = auth()->user()->primary_location;
        if($user_location === 0){
            $locations = Company::where('type_id', '=', 2)->orderBy('name')->get();
        } else {
            $locations = Company::where('type_id', '=', 2)
                ->where('id', '=', $user_location)
                ->orderBy('name')
                ->get();
        }

        return view('supplies.create', ['locations' => $locations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $supplies = New Supplies;
        $supplies->old_id = 0;
        $supplies->name = $request->name;
        $supplies->location_id = $request->location;
        $supplies->save();

        $all = Supplies::orderBy('name')->paginate(50);
        
        return view('supplies.index', ['supplies'=>$all]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

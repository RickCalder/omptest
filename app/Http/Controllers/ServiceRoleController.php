<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\ServiceRoles;

class ServiceRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $service_roles = ServiceRoles::orderBy('name')->where('archived', '=', 0)->with('location')->paginate(50);
        return view('service_roles.index', ['service_roles'=>$service_roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locations = [];
        $user_location = auth()->user()->primary_location;
        if($user_location === 0){
            $locations = Company::where('type_id', '=', 2)->orderBy('name')->get();
        } else {
            $locations = Company::where('type_id', '=', 2)
                ->where('id', '=', $user_location)
                ->orderBy('name')
                ->get();
        }

        return view('service_roles.create', ['locations' => $locations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $service_roles = New ServiceRoles;
        $service_roles->old_id = 0;
        $service_roles->name = $request->name;
        $service_roles->location_id = $request->location;
        $service_roles->rank = $request->rank;
        $service_roles->save();

        $all = ServiceRoles::orderBy('name')->paginate(50);
        
        return view('service_roles.index', ['service_roles'=>$all]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

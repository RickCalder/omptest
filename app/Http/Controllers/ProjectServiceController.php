<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


use App\ApiLocation;
use App\AddressStreetType;
use App\Address;
use App\CompanyAddresses;
use App\City;
use App\CompanyPerson;
use App\Company;
use App\Equipment;
use App\NewAddress;
use App\Person;
use App\Project;
use App\ProjectService;
use App\ProjectServicesAddresses;
use App\ProjectServicesEquipment;
use App\ProjectServicesFiles;
use App\ProjectServicesPeople;
use App\ProjectServicesSupplies;
use App\ProjectServicesNotes;
use App\Service;
use App\ServiceRoles;
use App\Supplies;

class ProjectServiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($id)
    {
      $allowedLocations = \App\CompanyUser::where('user_id', '=', auth()->user()->id)->get();
      // dd(auth()->user());
      $project_service = ProjectService::where('id', '=', $id)
      ->with('project')
      ->with('notes')
      ->with('service')
      ->with('branch')
      ->with('files')
      ->with('people')
      ->with('addresses')
      ->with('service_equipment')
      ->with('service_supplies')
      ->first();
        if(!$allowedLocations->contains('company_id', $project_service->branch->id) &&  (auth()->user()->primary_location != 0) && auth()->user()->primary_location !== $project_service->branch->id)  {
          return redirect()->route('dashboard');
        }
      $other_services = ProjectService::where('project_id', '=', $project_service->project_id)->orderBy('occurs_on', 'DESC')->get();
      $addresses = CompanyAddresses::where('company_id', '=', $project_service->project->customer_id)->get();
      // dd($addresses);
      // foreach($addresses as $address) {
      //   echo $address->newAddressToString() . '<br>';
      // }
      // die;
      $employees = CompanyPerson::all()
      ->where('company_id', '=', $project_service->branch->id)
      ->where('status', '=', 'Active')
      ->load('person')
      ->sortBy('person.first_name');
      $contacts = CompanyPerson::all()
      ->where('company_id', '=', $project_service->project->customer_id)
      ->where('status', '=', 'Active')
      ->load('person')
      ->sortBy('person.first_name');
      // dd($project_service->project->customer_id);
      
      $projects = Project::where('customer_id', '=', $project_service->project->customer_id)->orderBy('name')->get();

      $roles = ServiceRoles::orderBy('name')
      ->where('archived', '=', 0)
      ->where('location_id', '=', 0)
      ->orWhere('location_id', '=',$project_service->branch->id )
      ->get();

      $equipment = Equipment::orderBy('location_id', 'DESC')->orderBy('name')
        ->where('archived', '=', 0)
        ->where('location_id', '=', 0)
        ->orWhere('location_id', '=',$project_service->branch->id )
        ->get();
      $supplies = Supplies::orderBy('name')
      ->where('archived', '=', 0)
      ->where('location_id', '=', 0)
      ->orWhere('location_id', '=',$project_service->branch->id )
      ->get();

      $address_street_types = AddressStreetType::all();
      $cities = City::all();
      // dd($equipment);
      // dd($addresses);
// dd($project_service);
      return view('project_services.project_service', ['project_service'=>$project_service, 'addresses' => $addresses, 'other_services' => $other_services, 'employees' => $employees, 'roles' => $roles, 'equipment' => $equipment, 'contacts' => $contacts, 'supplies' => $supplies, 'projects' => $projects, 'addressStreetTypes' => $address_street_types, 'cities' => $cities]);
  }

  public function ajax_add_new_address(Request $request) {
    // dd($request->all());
    $address = new NewAddress;
    $address->old_id = 0;
    $address->address1 = $request->address1;
    $address->address2 = $request->address2;
    $address->building_name = $request->building_name;
    $address->unit_number = $request->unit_number;
    $address->floor = $request->floor;
    $address->city = $request->city;
    $address->state = $request->state;
    $address->postcode = $request->postal_code;
    $address->description = $request->description;
    $address->notes = $request->notes;
    $address->longitude = $request->longitude;
    $address->latitude = $request->latitude;
    $address->save();

    $c_address= new CompanyAddresses;
    $c_address->old_id = 0;
    $c_address->company_id = $request->company_id;
    $c_address->address_id = $address->id;
    $c_address->save();
    $returnData = [
      'address_id' => $c_address->id,
      'address' => $c_address->newAddressToString()
    ];
    return response()->json(['address' => $returnData]);
    // dd('hello');
  }

  public function ajax_add_new_contact(Request $request) {
    $contact = new Person;
    $contact->old_id = 0;
    $contact->first_name = $request->first_name;
    $contact->last_name = $request->last_name;
    $contact->email = $request->email;
    $contact->phone = $request->phone;
    $contact->save();

    $companyContact = new CompanyPerson;
    $companyContact->old_id = 0;
    $companyContact->company_id = $request->company_contact_id;
    $companyContact->person_id = $contact->id;
    $companyContact->phone = $request->phone;
    $companyContact->email = $request->email;
    $companyContact->status = 'Active';
    $companyContact->save();

    $returnData = [
      'first_name' => $contact->first_name,
      'last_name' => $contact->last_name,
      'contact_id' => $companyContact->id
    ];
    return response()->json(['contact' => $returnData]);
  }

  public function ajax_edit_ps_project(Request $request) {
    $project_service = ProjectService::where('id', '=', $request->project_service_id)->first();
    $project_service->project_id = $request->project;
    $project_service->save();

    return redirect('project_service/' . $request->project_service_id);
  }

  public function create(Request $request) {

    $branches = Company::where('type_id', '=' ,2)->orderBy('name')->get();
    $services = Service::where('archived_on', '=', null)->orderBy('name')->get();
    if(auth()->user()->primary_location !== 0) {
      $user_branch = Company::where('id','=',auth()->user()->primary_location)->first();
    } else {
      $user_branch='';
    }

    // dd($user_branch);
    if($request->project_id === null ) {
      $customers = Company::orderBy('name')->get();
      $services = Service::orderBy('name')->get();
      $sub_customers = [];
      return view('project_services.create', ['branches' => $branches, 'services' => $services, 'customers' => $customers, 'sub_customers' => $sub_customers, 'services' => $services, 'user_branch' => $user_branch]);
    } else {
      $project = Project::where('id', '=', $request->project_id)->first();
      $projects = Project::where('customer_id', '=', $project->customer_id)->orderBy('name')->get();
      $customer = Company::where('id', '=', $project->customer_id);
      if($project->customer_customer_id !== null) {
        $sub_customer = Project::where('customer_customer_id', '=', $project->customer_customer_id)->first();
      } else {
        $sub_customer = null;
      }

      return view('project_services.create', ['project' => $project, 'branches' => $branches, 'services' => $services, 'customer' => $customer, 'sub_customer' => $sub_customer, 'services' => $services, 'user_branch' => $user_branch]);

    }
  }

  
  public function edit($id) {
    $project_service = ProjectService::where('id', '=', $id)
    ->with('project')
    ->first();
    
    $branches = Company::where('type_id', '=', 2)->get();
    $services = Service::where('archived_on', '=', null)->orderBy('name')->get();

    // dd($project_service->project->customer);

    return view('project_services.edit', ['project_service' => $project_service, 'branches' => $branches, 'services'=>$services]);
  }

  public function get_projects_for_branch(Request $request) {
    if($request->sub_customer === null ) {
      $projects = Project::where('branch_id', '=', $request->branch)->where('customer_id', '=', $request->customer)->orderBy('id', 'desc')->get();
    } else {
      $projects = Project::where('branch_id', '=', $request->branch)
      ->where('customer_id', '=', $request->customer)
      ->where('customer_customer_id', '=', $request->sub_customer)
      ->orderBy('id', 'desc')->get();
    }
    return response()->json(['projects'=> $projects]);
  }

  public function get_projects_for_sub_customer(Request $request) {

    if($request->branch !== 'all') {
      $projects = Project::where('branch_id', '=', $request->branch)
        ->where('customer_id', '=', $request->customer)
        ->where('customer_customer_id', '=', $request->sub_customer)
        ->orderBy('id', 'desc')->get();
    } else {
      $projects = Project::where('customer_id', '=', $request->customer)
        ->where('customer_customer_id', '=', $request->sub_customer)
        ->orderBy('id', 'desc')->get();
    }
    return response()->json(['projects'=> $projects]);
  }

  public function get_customers_for_project(Request $request) {
    $project = Project::where('id', '=', $request->project)->with('customer', 'sub_customer')->first();
    return response()->json(['project'=> $project]);
  }

  public function add_person(Request $request) {
    // dd($request->all());
    if( $request->quantity > 1) {
      for( $i = 1; $i <= $request->quantity; $i++) {
        $service = new ProjectServicesPeople;
        $service->service_role_id = $request->service_role_id;
        $service->project_service_id = $request->project_service_id;
        $service->scheduled_in_at = $request->scheduled_in_at;
        $service->scheduled_out_at = $request->scheduled_out_at;
        $service->old_id = 0;
        $service->save();
      }
      $role = ServiceRoles::where('id', '=', $request->service_role_id)->first();
      return response()->json(['role' => $role, 'service'=> $service, 'quantity' => $request->quantity]);

    } else {
      $service = new ProjectServicesPeople;
      $service->old_id = 0;
      $service->company_person_id = $request->company_person_id;
      $service->service_role_id = $request->service_role_id;
      $service->project_service_id = $request->project_service_id;
      $service->scheduled_in_at = $request->scheduled_in_at;
      $service->scheduled_out_at = $request->scheduled_out_at;
      $service->old_id = 0;
      $service->save();
      $person = CompanyPerson::where('id', '=', $request->company_person_id)->with('person')->first();
      $role = ServiceRoles::where('id', '=', $request->service_role_id)->first();
      return response()->json(['person'=> $person, 'role' => $role, 'service'=> $service]);
    }

  }

  public function edit_person(Request $request) {
    // print_r($request->all());
    $person = ProjectServicesPeople::where('id', '=', $request->id)->first();
    if($request->company_person_id) {
      $person->company_person_id = $request->company_person_id;
    }
    if($request->service_role_id) {
      $person->service_role_id = $request->service_role_id;
    }
    if($request->scheduled_in_at) {
      $person->scheduled_in_at = $request->scheduled_in_at;
    } else {
      $person->scheduled_in_at = '';
    }
    if($request->scheduled_out_at) {
      $person->scheduled_out_at = $request->scheduled_out_at;
    } else {
      $person->scheduled_out_at = '';
    }
    $person->save();
    $person2 = CompanyPerson::where('id', '=', $request->company_person_id)->with('person')->first();
    $role = ServiceRoles::where('id', '=', $request->service_role_id)->first();


    return response()->json(['service_person'=> $person, 'person' => $person2, 'role' => $role]);
    
  }

  public function delete_person(Request $request) {
    $person = ProjectServicesPeople::destroy($request->id);
    return response()->json(['result'=> $person]);
  }

  
  public function delete_note(Request $request) {
    $note = ProjectServicesNotes::where('id', '=', $request->id)->first();
    $note->deleted = 1;
    $note->deleted_on = now();
    $note->deleted_by = auth()->user()->id;
    $note->save();
    
    return response()->json(['result' => $note]);
  }

  public function get_sub_customers(Request $request) {

    $projects = Project::where('customer_id', '=', $request->customer)->orderBy('name')->get();
    $sub_customers =[];
    $i = 0;
    foreach($projects as $project) {
      if($project->customer_customer_id !== null) {
        $sub_customers[$i] = $project->customer_customer_id;
        $i++;
      }
    }
    $sub_customers = array_unique($sub_customers);
    $subs = DB::table('companies')
      ->whereIn('id',$sub_customers)
      ->orderBy('name')
      ->get();

    return response()->json(['sub_customers'=> $subs, 'projects' => $projects]);
  }

  public function store(Request $request) {

    // dd($request->all());
    $ps = new ProjectService;
    $ps->project_id = $request->project;
    $ps->nickname = $request->nickname;
    $ps->occurs_on = $request->occurs_on;
    $ps->occurs_at = $request->occurs_at;
    $ps->minutes_estimate = $request->minutes_est * 60;
    $ps->service_id = $request->service;
    $ps->branch_id = $request->branch;
    $ps->status = $request->status;
    $ps->invoice_number = $request->invoice;
    $ps->old_id = 0;
    $ps->created_by = auth()->user()->company_person->id;
    $ps->save();
    return redirect('/project_service/' . $ps->id);
  }

  public function duplicate(Request $request) {

    // dd($request->all());
    $old_ps = ProjectService::where('id', '=', $request->duplicate_id)->first();
    // dd($old_ps. $request->duplicate_id);
    $ps = new ProjectService;
    $ps->project_id = $old_ps->project->id;
    $ps->occurs_on = $request->occurs_on;
    $ps->occurs_at = $request->occurs_at;
    $ps->minutes_estimate = $request->minutes_est * 60;
    $ps->service_id = $old_ps->service->id;
    $ps->branch_id = $old_ps->branch->id;
    $ps->status = $old_ps->status;
    $ps->invoice_number = $old_ps->invoice;
    $ps->old_id = 0;
    $ps->created_by = auth()->user()->company_person->id;

    $ps->save();

    if(isset($request->documents_dup)) {
      $old_docs = ProjectServicesFiles::where('project_service_id', '=', $request->duplicate_id)->get();
      foreach($old_docs as $doc) {
        $nf = new ProjectServicesFiles;
        $nf->old_id = 0;
        $nf->project_service_id = $ps->id;
        $nf->file_url = $doc->file_url;
        $nf->name = $doc->name;
        $nf->mime_type = $doc->mime_type;
        $nf->owned_by = $doc->owned_by;
        $nf->save();
      }
    }

    if(isset($request->people)) {
      $old_people = ProjectServicesPeople::where('project_service_id', '=', $request->duplicate_id)->get();
      foreach( $old_people as $person) {
        $p = new ProjectServicesPeople;
        $p->old_id = 0;
        $p->person_id = $person->person_id;
        $p->project_service_id = $ps->id;
        $p->company_person_id = $person->company_person_id;
        $p->service_role_id = $person->service_role_id;
        $p->save();
      }
    }

    if(isset($request->addresses)) {
      $old_add = ProjectServicesAddresses::where('project_service_id', '=', $request->duplicate_id)->get();
      foreach( $old_add as $add) {
        $a = new ProjectServicesAddresses;
        $a->old_id = 0;
        $a->project_service_id = $ps->id;
        $a->type = $add->type;
        $a->note = $add->note;
        $a->company_address_id = $add->company_address_id;
        $a->company_person_id = $add->company_person_id;
        
        $a->save();
      }
    }

    if(isset($request->equipment_dup)) {
      $old_equip = ProjectServicesEquipment::where('project_service_id', '=', $request->duplicate_id)->get();
      foreach( $old_equip as $equip) {
        $e2 = new ProjectServicesEquipment;
        $e2->old_id = 0;
        $e2->project_service_id = $ps->id;
        $e2->equipment_id = $equip->equipment_id;
        $e2->quantity_requested = $equip->quantity_requested;
        
        $e2->save();
      }
    }

    if(isset($request->supplies)) {
      $old_supplies = ProjectServicesSupplies::where('project_service_id', '=', $request->duplicate_id)->get();
      foreach( $old_supplies as $supplies) {
        $s = new ProjectServicesSupplies;
        $s->old_id = 0;
        $s->project_service_id = $ps->id;
        $s->supply_id = $supplies->supply_id;
        $s->quantity = $supplies->quantity;
        
        $s->save();
      }
    }

    if(isset($request->notes)) {
      $old_notes = ProjectServicesNotes::where('project_service_id', '=', $request->duplicate_id)->get();
      foreach( $old_notes as $notes) {
        $n = new ProjectServicesNotes;
        $n->old_id = 0;
        $n->project_service_id = $ps->id;
        $n->entered_by = $notes->entered_by;
        $n->entered_on = $notes->entered_on;
        $n->content = $notes->content;
        
        $n->save();
      }
    }



    return redirect('/project_service/' . $ps->id);
  }

  public function ajax_get_address(Request $request) {
    // dd($request);
    $address = CompanyAddresses::where('id', '=', $request->address_id)->with('address')->first();
    $psAddress = ProjectServicesAddresses::where('id', '=', $request->psaddress)->first();
    // dd($address);
    return response()->json(['address' => $address, 'psaddress' => $psAddress]);
  }

  public function add_address(Request $request) {
    // print_r($request->all());die;
    $address = new ProjectServicesAddresses;
    $address->company_address_id = $request->company_address_id;
    $address->type = $request->type;
    $address->company_person_id = $request->company_person_id;
    $address->note = $request->note;
    $address->project_service_id = $request->project_service_id;
    $address->old_id = 0;
    $address->save();
    if($request->company_person_id !== '' && $request->company_person_id !== null) {
      $contact = CompanyPerson::where('id', '=', $address->company_person_id)
      ->with('person')
      ->first();

      return response()->json(['address'=> $address, 'contact' => $contact->person]);
    } else {
      $contact = [];
      return response()->json(['address'=> $address, 'contact' => $contact]);
    }
  }

  public function edit_address(Request $request) {
    // print_r($request->all());
   
    $psaddress = ProjectServicesAddresses::where('id', '=', $request->id)->first();
    $psaddress->company_address_id = $request->company_address_id;
    $psaddress->type = $request->type;
    $psaddress->company_person_id = $request->company_person_id;
    $psaddress->note = $request->note;
    $psaddress->save();
    $address = NewAddress::where('id', '=', $request->address_id)->first();
    $address->address1 = $request->address1;
    $address->address2 = $request->address2;
    $address->city = $request->city;
    $address->postcode = $request->postal_code;
    $address->state = $request->state;
    $address->floor = $request->floor;
    $address->building_name = $request->building_name;
    $address->save();
    $caddress = CompanyAddresses::where('id', '=', $psaddress->company_address_id)->first();
    $address_string = $caddress->newAddressToString();
    // dd($address);

    if($psaddress->company_person_id !== null) {
      $contact = CompanyPerson::where('id', '=', $psaddress->company_person_id)
      ->with('person')
      ->first();

      return response()->json(['note' => $psaddress->note, 'type' => $psaddress->type, 'ps_id' => $psaddress->id, 'address'=> $address, 'contact' => $contact->person, 'address_string' =>$address_string]);
    } else {
      $contact = [];
      return response()->json(['note' => $psaddress->note, 'type' => $psaddress->type, 'ps_id' => $psaddress->id,'address'=> $address, 'contact' => $contact, 'address_string' =>$address_string]);
    }
  }

  public function delete_address(Request $request) {
    // print_r($request->all());die;
    $address = ProjectServicesAddresses::destroy($request->id);
    return response()->json(['result'=> $address]);
  }

  public function add_equipment(Request $request) {
    // print_r($request->all());die;
    $equipment = new ProjectServicesEquipment;
    $equipment->project_service_id = $request->project_service_id;
    $equipment->equipment_id = $request->equipment_id;
    $equipment->quantity_requested = $request->quantity_requested;
    $equipment->quantity_actual = $request->quantity_actual;
    $equipment->old_id = 0;
    $equipment->save();
    return response()->json(['equipment'=> $equipment]);
  }

  public function  edit_equipment_ps(Request $request) {
    // dd($request->all());
    $equip =ProjectServicesEquipment::where('id', '=', $request->record_id)->first();
    $equip->quantity_requested = $request->quantity;
    $equip->equipment_id = $request->equipment_id;
    $equip->save();
    $equipment = Equipment::where('id', '=', $equip->equipment_id)->first();
    return response()->json(['result' => $equip, 'equipment' => $equipment]);
  }

  public function delete_equipment(Request $request) {
    // print_r($request->all());die;
    $equipment = ProjectServicesEquipment::destroy($request->id);
    return response()->json(['result'=> $equipment]);
  }

  

  public function  edit_supplies_ps(Request $request) {
    // dd($request->all());
    $supply = ProjectServicesSupplies::where('id', '=', $request->record_id)->first();
    $supply->quantity = $request->quantity;
    $supply->supply_id = $request->supply_id;
    $supply->save();
    $supplies = Supplies::where('id', '=', $supply->supply_id)->first();
    return response()->json(['result' => $supply, 'supplies' => $supplies]);
  }

  public function add_supplies(Request $request) {
    // print_r($request->all());die;
    $supplies = new ProjectServicesSupplies;
    $supplies->project_service_id = $request->project_service_id;
    $supplies->supply_id = $request->supply_id;
    $supplies->quantity = $request->quantity;
    $supplies->old_id = 0;
    $supplies->save();
    return response()->json(['supplies'=> $supplies]);
  }

  public function delete_supplies(Request $request) {
    // print_r($request->all());die;
    $supplies = ProjectServicesSupplies::destroy($request->id);
    return response()->json(['result'=> $supplies]);
  }

  public function add_note(Request $request) {
    // dd($request->all());
    $note = new ProjectServicesNotes;
    if(auth()->user()->person) {
      $note->entered_by = auth()->user()->person_id;
    } else {
      $note->entered_by = 1;
    }
    // $note->entered_by = 47; // need to figureout person to user link
    $note->old_id = 0;
    $note->project_service_id = $request->project_service_id;
    $note->access_level = 'public'; // need to set this?
    $note->entered_on = date('Y-m-d h:i:s');
    $note->content = nl2br($request->note);
    $note->note_type = $request->note_type;
    $note->save();
    $return=[];
    $return['name'] = $note->person->first_name . ' ' . $note->person->last_name;
    $return['entered_on'] = $note->entered_on;
    $return['content'] = $note->content;
    $return['note_type'] = $note->note_type;
    $return['id'] = $note->id;
    return response()->json(['note'=> $return]);
  }

  public function add_document(Request $request) {
    print_r($request->all());die;
  }


  public function update(Request $request, $id) {
    $ps = ProjectService::where('id', '=', $id)->first();
    $ps->nickname = $request->nickname;
    $ps->project_id = $request->project;
    $ps->occurs_on = $request->occurs_on;
    $ps->occurs_at = $request->occurs_at;
    $ps->minutes_estimate = $request->minutes_est * 60;
    $ps->service_id = $request->service;
    $ps->branch_id = $request->branch;
    $ps->status = $request->status;
    $ps->invoice_number = $request->invoice;
    $ps->old_id = 0;
    if(auth()->user()->company_person) {
      $ps->created_by = auth()->user()->company_person->id;
    } else {
      $ps->created_by = 1;
    }
    $ps->save();
    return redirect('/project_service/' . $ps->id);
  }

  public function uploadFile(Request $request) {

    // dd($request->all());
    $file =$request->file('document');
    // $file->validate([
    //   'file' => 'required|mimes:png,jpg,jpeg,csv,txt,xlx,xls,pdf,docx|max:10240'
    // ]);
    $path = 'public/projects/'.$request->project.'/services/'.$request->project_service;
    $filepath = '/storage/projects/'.$request->project.'/services/'.$request->project_service;
    // $path ='new';
    // dd($path);

    $test = $request->file('document')->storeAs($path, $file->getClientOriginalName());
// dd($test);
    $dbUpdate = new ProjectServicesFiles;
    $dbUpdate->file_url =  $filepath . '/' . $request->file('document')->getClientOriginalName();
    $dbUpdate->old_id = 0;
    $dbUpdate->project_service_id = $request->project_service;
    $dbUpdate->name = $request->description;
    $dbUpdate->owned_by = auth()->user()->id;
    $dbUpdate->mime_type = $request->file('document')->getMimeType();
    $dbUpdate->save();
    // dd($dbUpdate);

    return back();
  }

  public function delete_file_ps(Request $request) {
    $file = ProjectServicesFiles::destroy($request->id);
    return response()->json(['result'=> $file]);
  }

  public function ajax_update_status(Request $request) {
    // dd($request->all());
    $project_service = ProjectService::where('id', '=', $request->id)->first();
    $project_service->status = $request->status;
    $project_service->save();
  }

  public function ajax_update_nickname(Request $request) {
    // dd($request->all());
    $project_service = ProjectService::where('id', '=', $request->id)->first();
    $project_service->nickname = $request->nickname;
    $project_service->save();
  }

  public function ajax_send_payroll_api(Request $request) {
    $project_service = ProjectService::where('id', '=', $request->api_pid)->first();
    $payroll_api = ApiLocation::where('location_id', '=', $project_service->branch->id)->where('api_id', '=', 1)->first();
    $data = [];
    $data['Code'] = $request->api_pid;
    $data['Description'] = $project_service->nickname;
    $data['Notes'] = $project_service->project->customer->name . ' - ' .$project_service->project->name . ' - ' . $project_service->service->name;
    $data['Open'] = false;
    $data['OpenDate'] = $project_service->occurs_on;
    $data['CloseDate'] = $project_service->occurs_on;
    $data['BudgetDefault'] = 0;
    $data['TimeOverride'] = -1;
    $data['GL_Reg'] = '';
    $data['GL_T15'] = '';
    $data['GL_T20'] = '';
    $data['ExternalCode'] = '';
    // dd($data);

    $url2 = $payroll_api->api_url . 'Jobs/Create';        
    $ch2 = curl_init(); 
    curl_setopt($ch2, CURLOPT_URL, $url2); 
    curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
      'APIKey: ' . $payroll_api->api_key,
      'X-locale: en_US',
      'Content-Type: application/json'
    ));
    curl_setopt($ch2, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch2, CURLOPT_RETURNTRANSFER , true);
    curl_setopt($ch2, CURLOPT_ENCODING, '');
    curl_setopt($ch2, CURLINFO_HEADER_OUT, true);
    $head2 = curl_exec($ch2); 
    $httpCode2 = curl_getinfo($ch2, CURLINFO_HTTP_CODE); 

    $info = curl_getinfo($ch2);

    
    curl_close($ch2); 

    // dd($info);
    $project_service->api_updated = Date('Y-m-d');
    $project_service->save();
    return redirect()->back();
  }

  
  public function ajax_update_payroll_api(Request $request) {
    $project_service = ProjectService::where('id', '=', $request->api_pid)->first();
    $payroll_api = ApiLocation::where('location_id', '=', $project_service->branch->id)->where('api_id', '=', 1)->first();
    $data = [];
    $data['Code'] = $request->api_pid;
    $data['Description'] = $project_service->nickname;
    $data['Notes'] = $project_service->project->customer->name . ' - ' .$project_service->project->name . ' - ' . $project_service->service->name;
    $data['Open'] = true;
    $data['OpenDate'] = $project_service->occurs_on;
    $data['CloseDate'] = $project_service->occurs_on;
    $data['BudgetDefault'] = 0;
    $data['TimeOverride'] = -1;
    $data['GL_Reg'] = '';
    $data['GL_T15'] = '';
    $data['GL_T20'] = '';
    $data['ExternalCode'] = '';
    // dd($data);

    $url2 = $payroll_api->api_url . 'Jobs/Update';        
    $ch2 = curl_init(); 
    curl_setopt($ch2, CURLOPT_URL, $url2); 
    curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
      'APIKey: ' . $payroll_api->api_key,
      'X-locale: en_US',
      'Content-Type: application/json'
    ));
    curl_setopt($ch2, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch2, CURLOPT_RETURNTRANSFER , true);
    curl_setopt($ch2, CURLOPT_ENCODING, '');
    curl_setopt($ch2, CURLINFO_HEADER_OUT, true);
    $head2 = curl_exec($ch2); 
    $httpCode2 = curl_getinfo($ch2, CURLINFO_HTTP_CODE); 

    $info = curl_getinfo($ch2);

    
    curl_close($ch2); 

    // dd($info);
    $project_service->api_updated = Date('Y-m-d');
    $project_service->save();
    return redirect()->back();
  }

  public function ajax_delete_payroll_api(Request $request) {
    $project_service = ProjectService::where('id', '=', $request->api_pid)->first();
    $payroll_api = ApiLocation::where('location_id', '=', $project_service->branch->id)->where('api_id', '=', 1)->first();

    $url2 = $payroll_api->api_url . 'Jobs/Delete?code=' . $request->api_pid;        
    $ch2 = curl_init(); 
    curl_setopt($ch2, CURLOPT_URL, $url2); 
    curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
      'APIKey: ' . $payroll_api->api_key,
      'X-locale: en_US',
      'Content-Type: application/json'
    ));
    
    curl_setopt($ch2, CURLOPT_RETURNTRANSFER , true);
    curl_setopt($ch2, CURLOPT_ENCODING, '');
    curl_setopt($ch2, CURLINFO_HEADER_OUT, true);
    $head2 = curl_exec($ch2); 
    $httpCode2 = curl_getinfo($ch2, CURLINFO_HTTP_CODE); 

    $info = curl_getinfo($ch2);

    
    curl_close($ch2); 

    // dd($info);
    $project_service->api_updated = null;
    $project_service->save();
    return redirect()->back();
  }

}

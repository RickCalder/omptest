<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectService extends Model
{
    public function service() {
        return $this->hasOne('App\Service', 'id','service_id');
    }
    
    public function project() {
        return $this->belongsTo('App\Project');
    }
    
    public function branch() {
        return $this->hasOne('App\Company', 'id', 'branch_id');
    }

    public function branch_address() {
        return $this->hasOne('App\CompanyAddresses','company_id', 'branch_id');
    }

    public function notes() {
        return $this->hasMany('App\ProjectServicesNotes')->orderBy('entered_on', 'DESC');
    }

    public function files() {
        return $this->hasMany('App\ProjectServicesFiles');
    }

    public function people() {
        return $this->hasMany('App\ProjectServicesPeople');
    }

    public function addresses() {
        return $this->hasMany('App\ProjectServicesAddresses');
    }

    public function service_equipment() {
        return $this->hasMany('App\ProjectServicesEquipment')->with('equip');
    }

    public function service_supplies() {
        return $this->hasMany('App\ProjectServicesSupplies')->with('supply');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    
    public function street_type() {
        return $this->hasOne('App\AddressStreetType', 'id', 'address_street_type_id');
      }

    public function city() {
        return $this->hasOne('App\City', 'id', 'city_id');
    }
}

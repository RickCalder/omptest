<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectServicesEquipment extends Model
{
    public function equip() {
        return $this->hasOne('App\Equipment', 'id', 'equipment_id');
    }
}

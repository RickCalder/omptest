<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Hidden;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;

class ApiLocation extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\ApiLocation::class;
    public static $defaultSort = 'id';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    private function getLocations() {
      $locations = \App\Company::where('type_id', '=', 2)->get();
      $locationOptions = array();
      $locationOptions[0] = 'Main List';
      foreach($locations as $location) {
          $locationOptions[$location->id] = $location->name;
      }
      return $locationOptions;

    }

    private function getApis() {
        $apis = \App\Api::all();
        foreach($apis as $api) {
            $apiOptions[$api->id] = $api->name;
        }
        return $apiOptions;
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),

            Select::make('Location', 'location_id')
            ->options($this->getLocations())
            ->displayUsingLabels(),

            Text::make('api_url')
            ->sortable()
            ->rules('required', 'max:255'),

            Text::make('api_key')
            ->sortable()
            ->rules('required', 'max:255'),

            Select::make('Api', 'api_id')
            ->options($this->getApis())
            ->displayUsingLabels(),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}

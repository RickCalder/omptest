<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Laravel\Nova\Fields\Gravatar;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;

// use \App\Permission;

class User extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\User';
    public static $defaultSort = 'id';
    public static $title = 'name';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    // public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'name', 'email',
    ];

    private function getRoles() {
        $roles = \App\Role::all();
        $x = 1;
        $options = array();
        foreach ($roles as $role) {
            $options[$x] = $role['name'];
            $x++;
        }
        return $options;
    }

    private function getLocations() {
        $locations = \App\Company::where('type_id', '=', 2)->get();
        $locationOptions = array();
        $locationOptions[0] = 'All Locations';
        foreach($locations as $location) {
            $locationOptions[$location->id] = $location->name;
        }
        return $locationOptions;
    }


    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')
                ->sortable(),

            Gravatar::make(),

            Text::make('Name')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('Email')
                ->sortable()
                ->rules('required', 'email', 'max:254')
                ->creationRules('unique:users,email')
                ->updateRules('unique:users,email,{{resourceId}}'),

            Password::make('Password')
                ->onlyOnForms()
                ->creationRules('required', 'string', 'min:8')
                ->updateRules('nullable', 'string', 'min:8'),

            Select::make('Role', 'role_id')
                ->options($this->getRoles())
                ->displayUsingLabels(),

            Select::make('Primary Location', 'primary_location')
                ->options($this->getLocations())
                ->displayUsingLabels(),

            // Text::make('Person','person_id')
            //     ->sortable(),

            BelongsTo::make('Person')->searchable(),

            BelongsToMany::make('Locations','companies','App\Nova\Company'),

            // Select::make('Person', 'person_id')
            //     ->options($this->getPeople())
            //     ->displayUsingLabels(),

            // hasOne::make('Person'),
            
            // BelongsToMany::make('Permissions')
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new filters\UserRoles()
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}

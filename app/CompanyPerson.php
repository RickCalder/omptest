<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Company;
use App\Role;

class CompanyPerson extends Model
{
    public function person() {
        return $this->hasOne('App\Person','id', 'person_id');
    }

    public function company() {
        return $this->hasOne('App\Company', 'id', 'company_id');
    }

    public function is_employee() {
        $company = Company::where('id', '=', $this->company_id)->where('type_id', '=', 2)->first();
    }

}
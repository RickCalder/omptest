<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiLocation extends Model
{
  protected $guarded = [];
  public function api_name() {
    return $this->hasOne('App\Api', 'id', 'api_id');
  }
}

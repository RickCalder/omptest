<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectServicesAddresses extends Model
{
  public function company_address() {
    return $this->hasOne('App\CompanyAddresses', 'id', 'company_address_id');
  }

  public function contact() {
    return $this->hasOne('App\CompanyPerson', 'id', 'company_person_id')->with('person');
  }
}

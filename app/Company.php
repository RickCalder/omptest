<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;


    public function type() {
        return $this->hasOne('App\CompanyType', 'id','type_id');
    }

    public function project() {
        return $this->hasMany('App\Project', 'customer_id');
    }
    
    public function apis() {
        return $this->hasMany('App\ApiLocation', 'location_id');
    }

}

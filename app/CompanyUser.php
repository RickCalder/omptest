<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyUser extends Model
{
    protected $table = 'company_user';
    public function users() {
        return $this->hasMany('\App\User', 'id', 'user_id');
    }

    public function companies() {
        return $this->hasMany('\App\Company', 'id', 'company_id')->where('type_id', '=', 2);
    }
}

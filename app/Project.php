<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function customer() {
        return $this->hasOne('App\Company', 'id','customer_id');
    }
    public function branch() {
        return $this->hasOne('App\Company', 'id','branch_id');
    }
    public function sub_customer() {
        return $this->hasOne('App\Company', 'id','customer_customer_id');
    }
    public function project_services() {
        return $this->hasMany('App\ProjectService');
    }
    public function booked() {
        return $this->hasOne('App\CompanyPerson', 'id','booked_by');
    }
    public function entered() {
        return $this->hasOne('App\CompanyPerson', 'id','entered_by');
    }
    public function managed() {
        return $this->hasOne('App\CompanyPerson', 'id','managed_by');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Company;

class Person extends Model
{
    protected $table = 'persons';

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }
    
    public function full_name() {
        return $this->first_name . " " . $this->last_name;
    }
    public function company_person () {
        return $this->hasOne('App\CompanyPerson', 'person_id', 'id');
    }

    public function user() {
        $this->hasOne('App\User', 'person_id', 'id');
    }
}



<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectServicesNotes extends Model
{

  public function person() {
      return $this->hasOne('App\Person','id', 'entered_by');
  }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceRoles extends Model
{
    public function location() {
        return $this->hasOne('App\Company','id', 'location_id');
    }
}

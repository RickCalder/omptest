<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function permissions() {
        return $this->belongsToMany('App\Permission', 'user_permission');
    }

    public function role() {
        return $this->hasOne('App\Role', 'permission_role');
    }

    public function user_locations() {
        return $this->hasMany('App\UserLocation');
    }

    public function person() {
      return $this->belongsTo('App\Person', 'person_id', 'id');
    }

    public function company_person() {
      return $this->hasOne('App\CompanyPerson', 'person_id', 'person_id');
    }

    public function companies() {
      return $this->belongsToMany('App\Company')->where('type_id', '=', 2);
    }

    public function all_permissions() {
      $user = auth()->user();
      $permissions = array();
      $role_permissions = \App\Role::where('id', '=', $user->id)->with('permissions')->first();
      foreach($role_permissions->permissions as $role) {
          $permissions[$role->id] = $role->name;
      }
      $user_permissions = $user->permissions()->get();
      foreach($user_permissions as $user_p) {
          $permissions[$user_p->id] = $user_p->name;
      }

      return $permissions;
    }

    public function isSuperAdmin() {
      $superAdmin = \App\Role::where('name', '=', 'Super Admin')->first();
      $user = \App\User::where('id','=',auth()->user()->id)->first();
      // dd($superAdmin);
      if($user->role_id === $superAdmin->id) {
        return true;
      } else {
        return false;
      }
    }
}

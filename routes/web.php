<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', [
    'as' => 'login',
    'uses' => 'Auth\LoginController@showLoginForm'
  ]);

Route::post('/', [
  'as' => '',
  'uses' => 'Auth\LoginController@login'
]);

Route::get('/forgot-password', function () {
  return view('auth.forgot-password');
})->middleware('guest')->name('password.request');

Route::post('/forgot-password', function (Request $request) {
  $request->validate(['email' => 'required|email']);

  $status = Password::sendResetLink(
      $request->only('email')
  );

  return $status === Password::RESET_LINK_SENT
              ? back()->with(['status' => __($status)])
              : back()->withErrors(['email' => __($status)]);
})->middleware('guest')->name('password.email');

// Route::get('/{any}', 'SpaController@index')->where('any', '.*');
Route::get('/dashboard', 'HomeController@index')->name('dashboard');
// Route::post('/dashboard', 'HomeController@index')->name('dashboard');

//Project SErvices
Route::get('/project-service/create', 'ProjectServiceController@create')->name('create-service');
Route::get('/project-service/get-projects-subcustomer', 'ProjectServiceController@get_projects_for_sub_customer')->name('get-projects-sub');
Route::post('/project-service/get-projects', 'ProjectServiceController@get_projects_for_branch')->name('get-projects');
Route::post('/project-service/get-customers', 'ProjectServiceController@get_customers_for_project')->name('get-customers');
Route::post('/project-service/get-sub-customers', 'ProjectServiceController@get_sub_customers')->name('get-sub-customers');
Route::post('/project-service/create', 'ProjectServiceController@store')->name('store-project-service');
Route::get('/project_service/{id}', 'ProjectServiceController@index');
Route::post('/project-service/duplicate', 'ProjectServiceController@duplicate')->name('duplicate-project-service');
Route::post('/project_service/add_person_to_project_service', 'ProjectServiceController@add_person')->name('add_person_to_project_service');
Route::post('/project_service/edit_project_service_person', 'ProjectServiceController@edit_person')->name('edit_project_service_person');
Route::delete('/project_service/delete_person_from_project_service', 'ProjectServiceController@delete_person')->name('delete_person_from_project_service');

Route::post('/project_service/delete_note_from_project_service', 'ProjectServiceController@delete_note')->name('delete_note_from_project_service');

Route::post('/project_service/ajax_get_address', 'ProjectServiceController@ajax_get_address')->name('ajax_get_address');
Route::post('/project_service/add_address_to_project_service', 'ProjectServiceController@add_address')->name('add_address_to_project_service');
Route::post('/project_service/edit_address_project_service', 'ProjectServiceController@edit_address')->name('edit_address_project_service');
Route::delete('/project_service/delete_address_from_project_service', 'ProjectServiceController@delete_address')->name('delete_address_from_project_service');
Route::post('/project_service/add_equipment_to_project_service', 'ProjectServiceController@add_equipment')->name('add_equipment_to_project_service');
Route::post('/project_service/edit_equipment_ps', 'ProjectServiceController@edit_equipment_ps')->name('edit_equipment_ps');
Route::delete('/project_service/delete_equipment_from_project_service', 'ProjectServiceController@delete_equipment')->name('delete_equipment_from_project_service');
Route::post('/project_service/add_supplies_to_project_service', 'ProjectServiceController@add_supplies')->name('add_supplies_to_project_service');
Route::post('/project_service/edit_supplies_project_service', 'ProjectServiceController@edit_supplies_ps')->name('edit_supplies_ps');
Route::post('/project_service/add_note_to_project_service', 'ProjectServiceController@add_note')->name('add_note_to_project_service')->middleware(['XSS']);
Route::post('/project_service/upload_project_service_document', 'ProjectServiceController@add_document')->name('upload_project_service_document');
Route::delete('/project_service/delete_supplies_from_project_service', 'ProjectServiceController@delete_supplies')->name('delete_supplies_from_project_service');
Route::get('project_service/{id}/edit', 'ProjectServiceController@edit')->name('edit-project-service');
Route::put('/project_service/{id}', 'ProjectServiceController@update')->name('update-project-service');
Route::post('file/upload', 'ProjectServiceController@uploadFile')->name('file.upload');
Route::delete('file/delete_file_ps', 'ProjectServiceController@delete_file_ps')->name('delete_file_ps');
Route::post('/project_service/ajax_status', 'ProjectServiceController@ajax_update_status')->name('ajax_update_status');
Route::post('/project_service/ajax_send_payroll_api', 'ProjectServiceController@ajax_send_payroll_api')->name('ajax_send_payroll_api');
Route::post('/project_service/ajax_delete_payroll_api', 'ProjectServiceController@ajax_delete_payroll_api')->name('ajax_delete_payroll_api');
Route::post('/project_service/ajax_update_payroll_api', 'ProjectServiceController@ajax_update_payroll_api')->name('ajax_update_payroll_api');
Route::post('/project_service/ajax_update_nickname', 'ProjectServiceController@ajax_update_nickname')->name('ajax_update_nickname');
Route::post('/project_service/{id}/ajax_edit_ps_project', 'ProjectServiceController@ajax_edit_ps_project');
Route::post('/project_service/ajax_add_new_address', 'ProjectServiceController@ajax_add_new_address')->name('ajax_add_new_address');
Route::post('/project_service/ajax_add_new_contact', 'ProjectServiceController@ajax_add_new_contact')->name('ajax_add_new_contact');
//Projects
Route::get('/projects', 'ProjectController@index')->name('projects');
Route::get('/projects/create', 'ProjectController@create')->name('create-project');
Route::get('/projects/{id}/edit', 'ProjectController@edit')->name('edit-project');
Route::get('/projects/{id}', 'ProjectController@show');
Route::put('/projects/{id}', 'ProjectController@update')->name('update-project');
Route::post('/projects', 'ProjectController@store')->name('store-project');
Route::post('/projects/get-booked-by', 'PersonController@get_booked_by')->name('get-booked-by');
Route::post('/projects/get-managed-by', 'PersonController@get_managed_by')->name('get-managed-by');

//People
Route::resource('people', 'PersonController');
Route::get('/people/create', 'PersonController@create')->name('create-person');
Route::post('/people', 'PersonController@store')->name('store-person');
Route::post('/project_service/add_person_ajax', 'PersonController@add_person_ajax')->name('store-person-ajax');
// Route::get('/people/{id}/edit', 'PersonController@edit')->name('edit-person');
// Route::post('/people/{id}/update', 'PersonController@update')->name('update-person');
Route::get('person_autocomplete',array('as'=>'person_autocomplete','uses'=>'PersonController@person_autocomplete'));
Route::get('employee_autocomplete',array('as'=>'employee_autocomplete','uses'=>'PersonController@employee_autocomplete'));

//Companies
Route::resource('companies', 'CompanyController');
Route::get('customer_autocomplete',array('as'=>'customer_autocomplete','uses'=>'CompanyController@customer_autocomplete'));
//Addresses
Route::get('address_autocomplete',array('as'=>'address_autocomplete','uses'=>'AddressController@address_autocomplete'));

// Equipment
Route::resource('addresses', 'AddressController');
Route::resource('equipment', 'EquipmentController');
// Supplies
Route::resource('supplies', 'SuppliesController');
// Service Roles
Route::resource('service_role', 'ServiceRoleController');

//Reports
Route::get('/reports', 'ReportController@index')->name('reports');
Route::post('/reports/supervisor_control', 'ReportController@supervisor_control')->name('supervisor-control');
Route::post('/reports/time_sheet', 'ReportController@time_sheet')->name('time-sheet');
Route::post('/reports/work_order', 'ReportController@work_order')->name('work-order');
Route::post('/reports/summary_dispatch', 'ReportController@summary_dispatch')->name('summary-dispatch');

// Import data routes
if (App::environment('development'))
{
  Route::get('/data', 'DataController@index')->name('data');
  Route::post('/data/import_companies', 'DataController@import_companies')->name('import_companies');
  Route::post('/data/create_company_types', 'DataController@create_company_types')->name('create_company_types');
  Route::post('/data/import_cities', 'DataController@import_cities')->name('import_cities');
  Route::post('/data/import_countries', 'DataController@import_countries')->name('import_countries');
  Route::post('/data/import_provinces', 'DataController@import_provinces')->name('import_provinces');
  Route::post('/data/import_addresses', 'DataController@import_addresses')->name('import_addresses');
  Route::post('/data/import_people', 'DataController@import_people')->name('import_people');
  Route::post('/data/import_company_people', 'DataController@import_company_people')->name('import_company_people');
  Route::post('/data/import_projects_table', 'DataController@import_projects_table')->name('import_projects_table');
  Route::post('/data/import_projectservices_table', 'DataController@import_projectservices_table')->name('import_projectservices_table');
  Route::post('/data/import_services_table', 'DataController@import_services_table')->name('import_services_table');
  Route::post('/data/import_project_services_notes', 'DataController@import_project_service_notes')->name('import_project_service_notes');
  Route::post('/data/import_project_service_files', 'DataController@import_project_services_files')->name('import_project_services_files');
  Route::post('/data/import_project_services_people', 'DataController@import_project_services_people')->name('import_project_services_people');
  Route::post('/data/import_service_roles', 'DataController@import_service_roles')->name('import_service_roles');
  Route::post('/data/import_project_services_addresses', 'DataController@import_project_services_addresses')->name('import_project_services_addresses');
  Route::post('/data/import_company_addresses', 'DataController@import_company_addresses')->name('import_company_addresses');
  Route::post('/data/import_street_type', 'DataController@import_street_type')->name('import_street_type');
  Route::post('/data/import_project_services_equipment', 'DataController@import_project_services_equipment')->name('import_project_services_equipment');
  Route::post('/data/import_equipment', 'DataController@import_equipment')->name('import_equipment');
  Route::post('/data/import_supplies', 'DataController@import_supplies')->name('import_supplies');
  Route::post('/data/import_project_service_supplies', 'DataController@import_project_service_supplies')->name('import_project_service_supplies');
  Route::post('/data/import_users', 'DataController@import_users')->name('import_users');
  Route::post('/data/compile_addresses', 'DataController@compile_addresses')->name('compile_addresses');

  Route::get('/data/test', 'TestController@index');
}

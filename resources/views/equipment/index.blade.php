@extends('layouts.app')

@section('content')

<div class="text-gray-900 mt-5">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  <div>
    <h2 class="mt-4 mb-2 h1 font-weight-bold text-center">Equipment</h2>
    <div class="d-flex justify-content-end mt-2">
      <a href="/equipment/create" class="btn btn-sm btn-info">Add Equipment</a>
    </div>
    {{-- @include('components/people-filter') --}}
    
    <div class="d-flex flex-column">
      <div class="my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
      </div>
    </div>
    <table class="table table-sm table-striped table-hover mt-4">
      <thead>
        <tr>
          <th>
            Name
          </th>
          <th>
            Location
          </th>
          <th>
            
          </th>
        </tr>
      </thead>
      <tbody>
        @foreach($equipment as $item)
        <tr>
          <td>
            {{ $item->name ?? "" }}
          </td>
          <td>
            @if($item->location)
            {{$item->location->name}}
            @endif
          </td>
          <td style="text-align: right">
            <a href="/equipment/{{$item->id}}" class="btn btn-sm btn-primary">View</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <div class="d-flex justify-end my-4">
    @if ($equipment->hasPages())
    {{ $equipment->appends(Request::except('page'))->links() }}
    @endif
  </div>
</div>
@endsection
@extends('layouts.app')

@section('content')

<div class="text-gray-900 mt-5">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  <div>
    <h2 class="mt-4 mb-2 h1 text-center">Add New Equipment</h2>
    <form class="mt-4" id="create-company-form" action="{{route('equipment.store')}}" method="POST">
      @csrf 
      <div class="form-group">
        <label for="location">Location</label>
        @if(count($locations) > 1)
        <select class="form-control" name="location" id="location" required>
          <option value="">Choose Location</option>
          @foreach($locations as $location) 
          <option value="{{$location->id}}">{{$location->name}}</option>
          @endforeach
        </select>
        @elseif(count($locations) == 0)
        <p>You have no primary location set, please contact an administrator</p>
        @else
          <p>
            {{ $locations[0]->name }}
            <input type="hidden" name="location" value="{{ $locations[0]->id }}">
          </p>
        @endif 
      </div>
      @if(count($locations) !== 0)
      <div class="form-group">
        <label for="name">Name</label>
        <input class="form-control" type="text" name="name" id="name" required />
      </div>
      <div class="form-group">
        <label for="description">Description</label>
        <textarea class="form-control" type="text" name="description" id="description"></textarea>
      </div>
      <div class="form-group">
        <input type="submit" class="btn btn-primary" id="create-service">
      </div>
      @endif
    </form>
  </div>
</div>
@endsection
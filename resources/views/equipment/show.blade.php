@extends('layouts.app')

@section('content')

<div class="text-gray-900 mt-5">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  <div>
    <h2 class="mt-4 mb-2 h1 font-weight-bold text-center">
      {{ $equipment->name }}
    </h2>
    <div class="d-flex flex-column">
      <div class="my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
        <div class="align-middle inline-block min-w-full overflow-hidden sm:rounded-lg">
          <div class="border text-left p-4">
            <div class="d-flex justify-content-between align-items-start">
            <table>
              <tr>
                <td>
                  <span class="font-weight-bold col">Name: </span>
                </td>
                <td>
                  <span>{{$equipment->name}} </span>
                </td>
              </tr>
              <tr>
                <td>
                  <span class="font-weight-bold col">Description: </span>
                </td>
                <td>
                  {{$equipment->description}}
                </td>
              </tr>
              <tr>
                <td>
                  <span class="font-weight-bold col">Location: </span>
                </td>
                <td>
                  @if($equipment->location_id === 0)
                  <span>Main List</span>
                  @else 
                  {{$equipment->location->name}}
                  @endif 
                </td>
              </tr>
            </table>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
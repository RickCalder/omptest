@extends('layouts.app')

@section('content')

<div class="text-gray-900 my-5">
  <div class="text-center my-5 no-print">
    <button class="btn btn-lg btn-primary" onclick="window.print()">Print Report</button>
    <a href="/project_service/{{$project_service->id}}" class="btn btn-lg btn-primary ml-3">Back to Project Service</a>
  </div>
  <div class="row">
    <div class="col-3 text-left">
      <p>
        {{$project_service->branch->name}}<br>
        @php 
          foreach(explode("~", $project_service->branch_address->addressToString2()) as $line) {
            echo $line . '<br>';
          }
        @endphp 
      </p>
    </div>
    <div class="col-6 text-center">
      <h1>Time Sheet</h1>
      <p class="bg-dark text-light p-2 rounded w-25 mt-3 mx-auto">SID #{{$project_service->id}}</p>
    </div>
    <div class="col-3 text-right">
      <img src="/img/omp_colour_logo.png" style="max-width:70%">
    </div>
  </div>
  <div class="row my-4">
    <div class="col-12">
      <div class="row">
        <div class="col-6">
          <p class="mb-0"><strong>Occurs On:</strong> {{ \Carbon\Carbon::parse($project_service->occurs_on)->format('l, F d')}} at {{ \Carbon\Carbon::parse($project_service->occurs_at)->format('h:i a')}}</p>
          <p class="mb-0"><strong>Customer:</strong> {{ $project_service->project->customer->name }}</p>
          <p class="mb-0"><strong>Project:</strong> {{ $project_service->project->name }}</p>
          <p class="mb-0"><strong>Booked By:</strong> {{ $project_service->project->booked !== null ? $project_service->project->booked->person->full_name() : "" }}</p>
        </div>
        <div class="col-6">
          <p class="mb-0"><strong>Service:</strong> {{ $project_service->service->name }}</p>
          <p class="mb-0"><strong>Managed By:</strong> {{ $project_service->project->managed !== null ? $project_service->project->managed->person->full_name() : "" }}</p>
          <p class="mb-0"><strong>PO#:</strong> {{ $project_service->project->po_number }}</p>
          <p class="mb-0"><strong>Entered By:</strong> {{ $project_service->project->entered !== null ? $project_service->project->entered->person->full_name() : "" }}</p>
        </div>
      </div>
    </div>
  </div>
  <table class="table table-sm table-striped mt-4" id="people-table">
    <thead>
      <tr>
        <th>Unit #</th>
        <th>Name</th>
        <th>Role</th>
        <th>Arrived</th>
        <th>Left</th>
        <th>Breaks</th>
        <th>Travel</th>
        <th>Total Time</th>
      </tr>
    </thead>
    <tbody>
      @foreach( $project_service->people as $person)
      @php 
        $start = \Carbon\Carbon::parse($person->clocked_in_at);
        $end = \Carbon\Carbon::parse($person->clocked_out_at);
        $diff = $start->diff($end)->format('%H:%I');
      @endphp
      <tr>
        <td></td>
        <td>
          @if(isset($person->company_person))
          {{ $person->company_person->person->full_name() }}
          @endif
        </td>
        <td>{{ $person->service_role->name }}</td>
        <td>{{ $person->clocked_in_at }}</td>
        <td>{{ $person->clocked_out_at }}</td>
        <td>{{ $person->breaked_for }}</td>
        <td>{{ $person->travelled_for }}</td>
        <td>{{ $diff }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>


  <div>
    <div class="row">
      <div class="col-12 text-center mt-5">
        <p class="border-top w-100">
          Supervisor Signature
        </p>
      </div>
    </div>
  </div>
</div>
@endsection
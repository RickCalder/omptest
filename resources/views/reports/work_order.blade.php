@extends('layouts.app')

@section('content')

<div class="text-gray-900 my-5">
  <div class="text-center my-5 no-print">
    <button class="btn btn-lg btn-primary" onclick="window.print()">Print Report</button>
    <a href="/project_service/{{$project_service->id}}" class="btn btn-lg btn-primary ml-3">Back to Project Service</a>
  </div>
  <div class="row">
    <div class="col-3 text-left">
      <p>
        {{$project_service->branch->name}}<br>
        @php 
          foreach(explode("~", $project_service->branch_address->newAddressToString()) as $line) {
            echo $line . '<br>';
          }
        @endphp 
      </p>
    </div>
    <div class="col-6 text-center">
      <h1>{{$project_service->service->name}}</h1>
      <p class="bg-dark text-light p-2 rounded w-25 mt-3 mx-auto">SID #{{$project_service->id}}</p>
    </div>
    <div class="col-3 text-right">
      <img src="/img/omp_colour_logo.png" style="max-width:70%">
    </div>
  </div>
  <div class="row my-4">
    <div class="col-12">
      <div class="row">
        <div class="col-6">
          <p class="mb-0"><strong>Occurs On:</strong> {{ \Carbon\Carbon::parse($project_service->occurs_on)->format('l, F d')}} at {{ \Carbon\Carbon::parse($project_service->occurs_at)->format('h:i a')}}</p>
          <p class="mb-0"><strong>Customer:</strong> {{ $project_service->project->customer->name }}</p>
          <p class="mb-0"><strong>Project:</strong> {{ $project_service->project->name }}</p>
          <p class="mb-0"><strong>Booked By:</strong> {{ $project_service->project->booked !== null ? $project_service->project->booked->person->full_name() : "" }}</p>
        </div>
        <div class="col-6">
          <p class="mb-0"><strong>Service:</strong> {{ $project_service->service->name }}</p>
          <p class="mb-0"><strong>Managed By:</strong> {{ $project_service->project->managed !== null ? $project_service->project->managed->person->full_name() : "" }}</p>
          <p class="mb-0"><strong>PO#:</strong> {{ $project_service->project->po_number }}</p>
          <p class="mb-0"><strong>Entered By:</strong> {{ $project_service->project->entered !== null ? $project_service->project->entered->person->full_name() : "" }}</p>
        </div>
      </div>
    </div>
  </div>
  <div class="border-bottom" style="border-width:2px !important; border-color: #000 !important"><strong class="h3">Special Instructions</strong></div>
  <div class="mt-3">
  <ul>
      @foreach($project_service->notes as $note)
      @if($note->note_type == 2 && $note->deleted !== 1)
        <li>{!! $note->content !!}</li>
      @endif
      @endforeach
    </ul>
  </div>
  <div class="border-bottom" style="border-width:2px !important; border-color: #000 !important"><strong class="h3">Addresses</strong></div>
  <div class="mt-3">
    @foreach($project_service->addresses as $address)
    <div>
      <strong>{{ $address->type }}</strong><br>
      @if($address->company_address)
        {{ $address->company_address->newAddressToString() }}<br>
      @endif
      @if(isset($address->company_person_id))
        {{ $address->contact->person->first_name }} {{ $address->contact->person->last_name }} {{ $address->contact->email }} {{ $address->contact->phone }}
      @endif
    </div>
    @endforeach
  </div>
  <div class="border-bottom mt-4" style="border-width:2px !important; border-color: #000 !important"><strong class="h3">Equipment</strong></div>
  <div class="mt-3">
    <table class="table table-sm table-striped mt-4" id="people-table">
      <thead>
        <tr>
          <th>Requested</th>
          <th>Actual</th>
          <th>Equipment</th>
          <th>Verified By</th>
        </tr>
      </thead>
      <tbody>
        @foreach($project_service->service_equipment as $equip)
        @if(isset($equip->equip))
        <tr>
          <td>
            {{$equip->quantity_requested}}
          </td>
          <td>
            {{$equip->quantity_actual}}
          </td>
          <td>
            {{ $equip->equip->name }}
          </td>
          <td></td>
        </tr>
        @endif
        @endforeach
      </tbody>
    </table>
</div>
@endsection
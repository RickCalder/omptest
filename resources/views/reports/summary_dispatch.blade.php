@extends('layouts.app')

@section('content')

<div class="text-gray-900 my-5">
  <div class="text-center my-5 no-print">
    <button class="btn btn-lg btn-primary" onclick="window.print()">Print Report</button>
    
    <a href="{{ url()->previous() }}" class="btn btn-lg btn-primary ml-3">Back to Dashboard</a>
  </div>
  <div class="row">
    <div class="col-3 text-left">
    </div>
    <div class="col-md-6 text-center">
      <h1>Summary Dispatch</h1>
      <p class="mt-0">{{ $branch->name }}, {{ \Carbon\Carbon::parse($project_services[0]->occurs_on)->format('l, F d, Y') }}</p>
    </div>
    <div class="col-3 text-right">
      <img src="/img/omp_colour_logo.png" style="max-width:70%">
    </div>
  </div>
  <div class="mt-5">
    @foreach($project_services as $project_service)
      <div class="row border-bottom mb-1 p-1" style="background-color:#ddd">
        <div class="col-4 text-center">
          <p class="mb-0">SID # {{ $project_service->id }}</p>
          <p class="mb-0">{{ $project_service->project->customer->name }}</p>
        </div>
        <div class="col-4 text-center">
          <p class="mb-0">{{ $project_service->service->name }}</p>
        </div>
        <div class="col-4 text-center">
          <p class="mb-0">{{ \Carbon\Carbon::parse($project_services[0]->occurs_on)->format('l, F d, Y') }} at {{ \Carbon\Carbon::parse($project_service->occurs_at)->format('h:i a')}}</p>
          <p class="mb-0">{{ $project_service->project->name }}</p>
        </div>
      </div>
      <div class="row mb-3">
        <div class="col-md-6">
          <table class="table-sm table-bordered">
            <tbody>
              @foreach($project_service->service_equipment as $equip)
              @if(isset($equip->equip->name))
              <tr>
                <td>
                  {{ $equip->quantity_requested }}
                </td>
                <td>
                  @if(isset($equip->equip->name))
                  {{ $equip->equip->name }}
                  @endif
                </td>
              </tr>
              @endif
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="col-md-6">
          <table class="table-sm table-bordered">
            <tbody>
              @foreach($project_service->people as $person)
                <tr>
                  <td>
                    @if(isset($person->service_role))
                    {{ $person->service_role->name }}
                    @endif
                  </td>
                <td>
                  @if($person->company_person)
                  {{ $person->company_person->person->first_name }} {{ $person->company_person->person->last_name }}
                  @endif
                </td>
                </tr>
              @endforeach
            </tbody>

          </table>
        </div>
      </div>
      
    @endforeach
  </div>
  <div class="mt-2 bordered">
    <h2 class="text-center h4" style="background-color:#ddd">Equipment, Supplies &amp; Crew Summary</h2>
    <div class="row">
      <div class="col-6">
        <table class="table-sm table-bordered">
          <tbody>
            @foreach($equipment as $equip)

            <tr>
              <td>{{ $equip->count }}</td>
              <td>{{ $equip->name }}</td>
            </tr>

            @endforeach
          </tbody>
        </table>
      </div>
      <div class="col-6">
        <table class="table-sm table-bordered">
          <tbody>
            @foreach($people as $person)

            <tr>
              <td>{{ $person->count }}</td>
              <td>{{ $person->name }}</td>
            </tr>

            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  
@endsection
@extends('layouts.app')

@section('content')

<div class="text-gray-900 my-5">
  <div class="text-center my-5 no-print">
    <button class="btn btn-lg btn-primary" onclick="window.print()">Print Report</button>
    <a href="/project_service/{{$project_service->id}}" class="btn btn-lg btn-primary ml-3">Back to Project Service</a>
  </div>
  <div class="row">
    <div class="col-3 text-left">
      <p>
        {{$project_service->branch->name}}<br>
        @php 
          foreach(explode("~", $project_service->branch_address->addressToString2()) as $line) {
            echo $line . '<br>';
          }
        @endphp 
      </p>
    </div>
    <div class="col-6 text-center">
      <h1>Supervisor Control Sheet</h1>
      <p class="bg-dark text-light p-2 rounded w-25 mt-3 mx-auto">SID #{{$project_service->id}}</p>
    </div>
    <div class="col-3 text-right">
      <img src="/img/omp_colour_logo.png" style="max-width:70%">
    </div>
  </div>
  <div class="row my-5">
    <div class="col-12">
      <div class="row">
        <div class="col-6">
          <p><strong>Occurs On:</strong> {{ \Carbon\Carbon::parse($project_service->occurs_on)->format('l, F d')}} at {{ \Carbon\Carbon::parse($project_service->occurs_at)->format('h:i a')}}</p>
        </div>
        <div class="col-6">
          <p><strong>Customer:</strong> {{ $project_service->project->customer->name }}</p>
        </div>
      </div>
      <div class="row">
        <div class="col-6">
          <p><strong>Project:</strong> {{ $project_service->project->name }}</p>
        </div>
        <div class="col-6">
          <p><strong>PO#:</strong> {{ $project_service->project->po_number }}</p>
        </div>
      </div>
    </div>
  </div>
  <div>
    <div class="d-flex justify-content-between mt-5 border-bottom" style="border-width: 2px !important; border-color: #000 !important">
      <p class="font-weight-bold mb-0 pb-0">Change of Scope (Additional Work)</p>
      <p class="font-weight-bold mb-0 pb-0"><input type="checkbox" class="mr-2">None</p>
    </div>
    <div class="border-bottom mt-4"></div>
    <div class="border-bottom mt-4"></div>
    <div class="border-bottom mt-4"></div>
  </div>

  <div>
    <div class="d-flex justify-content-between mt-2 border-bottom" style="border-width: 2px !important; border-color: #000 !important">
      <p class="font-weight-bold mb-0 pb-0">Deficiencies</p>
      <p class="font-weight-bold mb-0 pb-0"><input type="checkbox" class="mr-2">None</p>
    </div>
    <div class="border-bottom mt-4"></div>
    <div class="border-bottom mt-4"></div>
    <div class="border-bottom mt-4"></div>
  </div>
  
  <div>
    <div class="d-flex justify-content-between mt-2 border-bottom" style="border-width: 2px !important; border-color: #000 !important">
      <p class="font-weight-bold mb-0 pb-0">Damages</p>
      <p class="font-weight-bold mb-0 pb-0"><input type="checkbox" class="mr-2">None</p>
    </div>
    <div class="border-bottom mt-4"></div>
    <div class="border-bottom mt-4"></div>
    <div class="border-bottom mt-4"></div>
  </div>
  
  <div>
    <div class="d-flex justify-content-between mt-2 border-bottom" style="border-width: 2px !important; border-color: #000 !important">
      <p class="font-weight-bold mb-0 pb-0">Items Brought Back</p>
      <p class="font-weight-bold mb-0 pb-0"><input type="checkbox" class="mr-2">None</p>
    </div>
    <div class="border-bottom mt-4"></div>
    <div class="border-bottom mt-4"></div>
    <div class="border-bottom mt-4"></div>
  </div>

  <div class="d-flex justify-content-start my-4">
    <p class="font-weight-bold border-bottom w-25" style="border-width: 2px !important; border-color: #000 !important">Tag Colour:</p>
    <p class="font-weight-bold border-bottom w-25" style="border-width: 2px !important; border-color: #000 !important">Lot #</p>
    <p class="font-weight-bold border-bottom w-25" style="border-width: 2px !important; border-color: #000 !important">From #</p>
    <p class="font-weight-bold border-bottom w-25" style="border-width: 2px !important; border-color: #000 !important">To #</p>
  </div>

  <div class="d-flex justify-content-between my-4">
    <p class="font-weight-bold border-bottom" style="border-width: 2px !important; border-color: #000 !important">Items Destined For:</p>
    <p class="font-weight-bold mb-0 pb-0"><input type="checkbox" class="mr-2">Disposal</p>
    <p class="font-weight-bold mb-0 pb-0"><input type="checkbox" class="mr-2">Redistribution</p>
    <p class="font-weight-bold mb-0 pb-0"><input type="checkbox" class="mr-2">Storage</p>
    <p class="font-weight-bold mb-0 pb-0"><input type="checkbox" class="mr-2">None</p>
  </div>
  
  <div>
    <div class="d-flex justify-content-between mt-2 border-bottom" style="border-width: 2px !important; border-color: #000 !important">
      <p class="font-weight-bold mb-0 pb-0">General Comments</p>
      <p class="font-weight-bold mb-0 pb-0"><input type="checkbox" class="mr-2">None</p>
    </div>
    <div class="border-bottom mt-4"></div>
    <div class="border-bottom mt-4"></div>
    <div class="border-bottom mt-4"></div>
  </div>

  <div>
    <div class="d-flex justify-content-between mt-2 border-bottom" style="border-width: 2px !important; border-color: #000 !important">
      <p class="font-weight-bold mb-0 pb-0">Everything completed to customer satisfaction</p>
    </div>
    <div class="row">
      <div class="col-6 text-center mt-5">
        <p class="border-top w-100">
          Customer Signature
        </p>
      </div>
      <div class="col-6 text-center mt-5">
        <p class="border-top w-100">
          Customer Printed Name
        </p>
      </div>
    </div>
    <div class="row">
      <div class="col-6 text-center mt-5">
        <p class="border-top w-100">
          Contact Number
        </p>
      </div>
      <div class="col-6 text-center mt-5">
        <p class="border-top w-100">
          Supervisor Signature
        </p>
      </div>
    </div>
  </div>
</div>
@endsection
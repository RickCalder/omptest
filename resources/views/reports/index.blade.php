@extends('layouts.app')

@section('content')

<div class="text-gray-900 mt-5">
  <h1>Reports</h1>
  @include('components/flash-message')
  <table class="table table-sm table-striped mt-4">
    <thead>
      <tr>
        <th>
          Name
        </th>
        <th>
          PID
        </th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          Supervisor Control Sheet
        </td>
        <td>
          <form action="{{route('supervisor-control')}}" method="POST" class="form-inline">
            @csrf
            <label for="sc-pid" class="sr-only">Supervisor Control PID</label>
            <input type="text" name="sc_pid" id="sc-pid" class="form-control mr-2">
            <input type="submit" class="btn btn-sm btn-primary" value="submit">
          </form>
        </td>
      </tr>
      <tr>
        <td>
          Time Sheet
        </td>
        <td>
          <form action="{{route('time-sheet')}}" method="POST" class="form-inline">
            @csrf
            <label for="ts-pid" class="sr-only">Time PID</label>
            <input type="text" name="ts_pid" id="ts-pid" class="form-control mr-2">
            <input type="submit" class="btn btn-sm btn-primary" value="submit">
          </form>
        </td>
      </tr>
      <tr>
        <td>
          Work Order
        </td>
        <td>
          <form action="{{route('work-order')}}" method="POST" class="form-inline">
            @csrf
            <label for="wo-pid" class="sr-only">Time PID</label>
            <input type="text" name="wo_pid" id="wo-pid" class="form-control mr-2">
            <input type="submit" class="btn btn-sm btn-primary" value="submit">
          </form>
        </td>
      </tr>
      <tr>
        <td>
          Summary Dispatch
        </td>
        <td>
          <form action="{{route('summary-dispatch')}}" method="POST" class="form-inline">
            @csrf
            <label for="sd-branch" class="sr-only">Summary Dispatch Branch</label>
            <select class="mr-2 form-control" name="branch">
              <option value="">Select Branch</option>
              @foreach($branches as $branch)
                <option value="{{ $branch->id }}">{{ $branch->name }}</option>
              @endforeach
            </select>
            <label for="date" class="sr-only">Date</label>
            <input data-toggle="datepicker1" class="form-control mr-2" name="date" id="date" placeholder="Date" autocomplete="off">
            <div data-toggle="datepicker1"></div>
            <input type="submit" class="btn btn-sm btn-primary" value="submit">
          </form>
        </td>
      </tr>
    </tbody>
  </table>
</div>
@endsection

@push('scripts')

<script>
    $(document).ready(function() {
      $('#date').datetimepicker({
        timepicker: false,
        format:'Y-m-d'
      });
    })
</script>

@endpush
@extends('layouts.app')

@section('content')
<div class="text-center">
  @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
  @endif
  <h2 class=" text-3xl leading-9 font-extrabold text-gray-900 mt-5">Data</h2>
</div>
<div>
  <form method="POST"  action="{{ route('create_company_types') }}">
    @csrf
    <button type="submit" class="btn btn-primary mb-3">Create Company Types</button>
  </form>
  <form method="POST"  action="{{ route('import_companies') }}">
    @csrf
    <button type="submit"  class="btn btn-primary mb-3">Import Companies</button>
  </form>
  <form method="POST"  action="{{ route('import_cities') }}">
    @csrf
    <button type="submit"  class="btn btn-primary mb-3">Import Cities</button>
  </form>
  <form method="POST"  action="{{ route('import_countries') }}">
    @csrf
    <button type="submit"  class="btn btn-primary mb-3">Import Countries</button>
  </form>
  <form method="POST"  action="{{ route('import_provinces') }}">
    @csrf
    <button type="submit"  class="btn btn-primary mb-3">Import Provinces</button>
  </form>
  <form method="POST"  action="{{ route('import_addresses') }}">
    @csrf
    <button type="submit"  class="btn btn-primary mb-3">Import Addresses</button>
  </form> 

  <form method="POST"  action="{{ route('import_people') }}">
    @csrf
    <button type="submit"  class="btn btn-primary mb-3">Import People</button>
  </form>

  <form method="POST"  action="{{ route('import_company_people') }}">
    @csrf
    <button type="submit"  class="btn btn-primary mb-3">Import Company People</button>
  </form>
  <form method="POST"  action="{{ route('import_projects_table') }}">
    @csrf
    <button type="submit"  class="btn btn-primary mb-3">Import Projects Table</button>
  </form>

  <form method="POST"  action="{{ route('import_projectservices_table') }}">
    @csrf
    <button type="submit"  class="btn btn-primary mb-3">Import Project Services Table</button>
  </form>
  <form method="POST"  action="{{ route('import_services_table') }}">
    @csrf
    <button type="submit"  class="btn btn-primary mb-3">Import Services Table</button>
  </form> 
  <form method="POST"  action="{{ route('import_project_service_notes') }}">
    @csrf
    <button type="submit"  class="btn btn-primary mb-3">Import Project Services Notes</button>
  </form> 
  <form method="POST"  action="{{ route('import_project_services_files') }}">
    @csrf
    <button type="submit" class="btn btn-primary mb-3">Import Project Service Files</button>
  </form> 
  <form method="POST"  action="{{ route('import_project_services_people') }}">
    @csrf
    <button type="submit" class="btn btn-primary mb-3">Import Project Service People</button>
  </form>
  <form method="POST"  action="{{ route('import_service_roles') }}">
    @csrf
    <button type="submit" class="btn btn-primary mb-3">Import Service Roles</button>
  </form> 
  <form method="POST"  action="{{ route('import_project_services_addresses') }}">
    @csrf
    <button type="submit" class="btn btn-primary mb-3">Import Project Services Addresses</button>
  </form>
  <form method="POST"  action="{{ route('import_company_addresses') }}">
    @csrf
    <button type="submit" class="btn btn-primary mb-3">Import Company Addresses</button>
  </form>
  <form method="POST"  action="{{ route('import_street_type') }}">
    @csrf
    <button type="submit" class="btn btn-primary mb-3">Import Street Type</button>
  </form>
  <form method="POST"  action="{{ route('import_project_services_equipment') }}">
    @csrf
    <button type="submit" class="btn btn-primary mb-3">Import Project Services Equipment</button>
  </form>
  <form method="POST"  action="{{ route('import_equipment') }}">
    @csrf
    <button type="submit" class="btn btn-primary mb-3">Import Equipment</button>
  </form>
  <form method="POST"  action="{{ route('import_supplies') }}">
    @csrf
    <button type="submit" class="btn btn-primary mb-3">Import Supplies</button>
  </form>
  <form method="POST"  action="{{ route('import_project_service_supplies') }}">
    @csrf
    <button type="submit" class="btn btn-primary mb-3">Import Project Service Supplies</button>
  </form>
  <form method="POST"  action="{{ route('import_users') }}">
    @csrf
    <button type="submit" class="btn btn-primary mb-3">Import Users</button>
  </form>
  <form method="POST"  action="{{ route('compile_addresses') }}">
    @csrf
    <button type="submit" class="btn btn-primary mb-3">Compile Addresses</button>
  </form>

</div>
<div>
</div>
@endsection

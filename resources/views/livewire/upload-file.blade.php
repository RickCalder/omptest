<form wire:submit.prevent="save">
    <div class="form-group mr-2">
      <label for="description">File Description</label>
      <input type="text" id="description" name="description" class="form-control" wire:model="fileName" required />
    </div>
    <div class="form-group mr-2">
      <input type="file" wire:model="file" required>
    </div>

    @error('file')
    <div class="form-group mr-2 alert alert-danger">
      <span>{{ $message }}</span>
    </div>
    @enderror
    
    @if (session()->has('message'))
    <div class="form-group mr-2 alert alert-success">
      <span>{{ session('message') }}</span>
    </div>
    @endif

    <input type="hidden" value="{{$projectId}}" wire:model="project"/>
    <input type="hidden" value="{{$serviceId}}" wire:model="project-service"/>

    <button class="btn btn-primary" type="submit">Save File</button>
</form>

@extends('layouts.app')

@section('content')
<div class="text-gray-900 mt-5 mb-5">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  <h2 class="text-center my-8 text-3xl font-extrabold">Edit {{$person->person->first_name}} {{$person->person->last_name}}</h2>
  <form class="mt-4 w-100" id="create-person-form" method="POST" action="{{route('people.update', $person->id)}}">
  <div class="row">
      @csrf
      @method('PUT')
      <div class="col-lg-6">
        <div class="form-group">
          <label for="title">Title</label>
          <select id="title" name="title" type='text' class="form-control">
            <option value="">Select Title</option>
            <option value="Mr.">Mr.</option>
            <option value="Mrs.">Mrs.</option>
            <option value="Ms.">Ms.</option>
            <option value="Miss">Miss</option>
          </select>
        </div>
        <div class="form-group">
          <label for="first_name">First Name <span>*</span></label>
          <input type="text" name="first_name" id="first_name" class="form-control" value="{{$person->person->first_name}}" required />
        </div>
        <div class="form-group">
          <label for="middle_name">Middle Name</label>
          <input type="text" name="middle_name" id="middle_name" class="form-control" value="{{$person->person->middle_name}}" />
        </div>
        <div class="form-group">
          <label for="last_name">Last Name<span>*</span></label>
          <input type="text" name="last_name" id="last_name" class="form-control" value="{{$person->person->last_name}}" required />
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" name="email" id="email" class="form-control" value="{{$person->person->email}}" />
        </div>
        <div class="form-group">
          <label for="phone">Phone</label>
          <input type="text" name="phone" id="phone" class="form-control" value="{{$person->person->phone}}" />
        </div>
        <div class="form-group">
          <label for="mobile_phone">Mobile Phone</label>
          <input type="text" name="mobile_phone" id="mobile_phone" class="form-control" value="{{$person->person->mobile_phone}}" />
        </div>
        <div class="form-group">
          <label for="emergency">Emergency Contact</label>
          <input class="form-control" type="text" name="emerg_contact" id="emergency" value="{{$person->emerg_contact ?? ''}}"s/>
        </div>
        <div class="form-group">
          <label for="notes">Notes</label>
          <textarea name="notes" id="notes" class="form-control">{{$person->person->notes}}</textarea>
        </div>
        
        <div class="form-group">
          <label for="company">Company</label>
          <input class="typeahead form-control" type="text" name="company" id="company" value="{{$person->company->name ?? ''}}" />
          <input type="hidden" id="company-id" name="company_id" value="{{$person->company->id ?? null}}" />
        </div>
      </div>

    <div class="col-lg-12">
      <input type="submit" class="btn btn-md btn-primary" value="Submit">
      <a href="/people/{{$person->id}}" class="btn btn-md btn-grey">Cancel</a>
    </div>
    <div class="col-lg-12">
      <p class="small mt-">* denotes required field</p>
    </div>
  </div>
</form>
</div>

@endsection

@push('scripts')
<script>
  $(document).ready(function() {
    $('.datepicker').datetimepicker({
      timepicker: false,
      format:'Y-m-d'
    });
  });

let path = "{{ route('customer_autocomplete') }}";
  $('#company').typeahead({
        items: 50,
      source:  function (query, process) {
          return $.get(path, { query: query }, function (data) {
              return process(data);
          });
      },
      items: 15,
      minLength: 3,
      highlighter: function (item, data) {
          var parts = item.split('#'),
              html = '<div class="row">';
              html += '<div class="col-md-2">';
              html += '</div>';
              html += '<div class="col-md-10 pl-0">';
              html += '<span>'+data.name+'</span>';
              html += '</div>';
              html += '</div>';

          return html;
      },
      afterSelect:function(data,value,text){
          $('#company-id').val(data.id)
      },
  })

let path2 = "{{ route('person_autocomplete') }}";
$('#emergency').typeahead({
        items: 50,
      source:  function (query, process) {
          return $.get(path2, { query: query }, function (data) {
              return process(data);
          });
      },
      items: 15,
      minLength: 3,
      highlighter: function (item, data) {
          var parts = item.split('#'),
              html = '<div class="row">';
              html += '<div class="col-md-2">';
              html += '</div>';
              html += '<div class="col-md-10 pl-0">';
              html += '<span>'+data.name+ '</span>';
              html += '</div>';
              html += '</div>';

          return html;
      },
      afterSelect:function(data,value,text){
          $('#emergency_contact_id').val(data.id)
      },
  })  
  $('#reports_to').typeahead({
        items: 50,
      source:  function (query, process) {
          return $.get(path2, { query: query }, function (data) {
              return process(data);
          });
      },
      items: 15,
      minLength: 3,
      highlighter: function (item, data) {
          var parts = item.split('#'),
              html = '<div class="row">';
              html += '<div class="col-md-2">';
              html += '</div>';
              html += '<div class="col-md-10 pl-0">';
              html += '<span>'+data.name+ '</span>';
              html += '</div>';
              html += '</div>';

          return html;
      },
      afterSelect:function(data,value,text){
          $('#reports_to_id').val(data.id)
      },
  })
</script>

@endpush
@extends('layouts.app')

@section('content')
<div class="text-gray-900 mt-5 mb-5">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  <h2 class="text-center my-8 text-3xl font-extrabold">Add New Person</h2>
  <form class="mt-4 w-100" id="create-person-form" method="POST" action="{{route('store-person')}}">
  <div class="row">
      @csrf
      <div class="col-lg-6">
        <div class="form-group">
          <label for="title">Title</label>
          <select id="title" name="title" type='text' class="form-control">
            <option value="">Select Title</option>
            <option value="Mr.">Mr.</option>
            <option value="Mrs.">Mrs.</option>
            <option value="Ms.">Ms.</option>
            <option value="Miss">Miss</option>
          </select>
        </div>
        <div class="form-group">
          <label for="first_name">First Name <span>*</span></label>
          <input type="text" name="first_name" id="first_name" class="form-control" required />
        </div>
        <div class="form-group">
          <label for="middle_name">Middle Name</label>
          <input type="text" name="middle_name" id="middle_name" class="form-control" />
        </div>
        <div class="form-group">
          <label for="last_name">Last Name<span>*</span></label>
          <input type="text" name="last_name" id="last_name" class="form-control" required />
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" name="email" id="email" class="form-control" />
        </div>
        <div class="form-group">
          <label for="phone">Phone</label>
          <input type="text" name="phone" id="phone" class="form-control" />
        </div>
        <div class="form-group">
          <label for="mobile_phone">Mobile Phone</label>
          <input type="text" name="mobile_phone" id="mobile_phone" class="form-control" />
        </div>
        <div class="form-group">
          <label for="phone_email">Phone Email</label>
          <input type="email" name="phone_email" id="phone_email" class="form-control" />
        </div>
        <div class="form-group">
          <label for="born_on">Birth Date</label>
          <input type="text" name="born_on" id="born_on" class="form-control datepicker" />
        </div>
        <div class="form-group">
          <label for="is_canadian_resident">Canadian Resident</label>
          <select class="form-control" name="is_canadian_resident" id="is_canadian_resident">
            <option value="">Select</option>
            <option value="1">Yes</option>
            <option value="0">No</option>
          </select>
        </div>
        <div class="form-group">
          <label for="license_abstract_permission">License Abstract Permission</label>
          <select class="form-control" name="license_abstract_permission" id="license_abstract_permission">
            <option value="">Select</option>
            <option value="1">Yes</option>
            <option value="0">No</option>
          </select>
        </div> 
        <div class="form-group">
          <label for="emergency">Emergency Contact</label>
          <input class="typeahead form-control person" type="text" name="emergency" id="emergency" />
          <input type="hidden" id="emergency_contact_id" name="emergency_contact_id"/>
        </div>
        <div class="form-group">
          <label for="notes">Notes</label>
          <textarea name="notes" id="notes" class="form-control"></textarea>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="form-group">
          <label for="company">Company</label>
          <input class="typeahead form-control" type="text" name="company" id="company" />
          <input type="hidden" id="company-id" name="company_id" />
        </div>
      <div class="form-group">
        <label for="position">Position</label>
        <input type="text" name="position" id="position" class="form-control" />
      </div>
      <div class="form-group">
        <label for="department">Department</label>
        <input type="text" name="department" id="department" class="form-control" />
      </div>
      <div class="form-group">
        <label for="reports_to">Reports To</label>
        <input type="text" name="reports_to" id="reports_to" class="form-control typeahead person" />
        <input type="hidden" id="reports_to_id" name="reports_to_id" />
      </div>
      <div class="form-group">
        <label for="hired_on">Hire Date</label>
        <input type="text" name="hired_on" id="hired_on" class="form-control datepicker" />
      </div>
      <div class="form-group">
        <label for="status">Status</label>
        <select class="form-control" name="status" id="status">
          <option value="">Select</option>
          <option value="Active">Active</option>
          <option value="Inactive">Inactive</option>
        </select>
      </div>
      <div class="form-group">
        <label for="status">Payroll</label>
        <select class="form-control" name="is_payroll" id="is_payroll">
          <option value="">Select</option>
          <option value="1">Yes</option>
          <option value="0">No</option>
        </select>
      </div>
      <div class="form-group">
        <label for="status">Salary</label>
        <select class="form-control" name="is_salary" id="is_salary">
          <option value="">Select</option>
          <option value="true">Yes</option>
          <option value="true+weekends">Yes (weekends)</option>
          <option value="true+evenings">Yes (evenings)</option>
          <option value="true+evenings+weekends">Yes (evenings &amp; weekends)</option>
          <option value="false">No</option>
        </select>
      </div>
      <div class="form-group">
        <label for="is_wheniwork">WhenIWork</label>
        <select class="form-control" name="is_wheniwork" id="is_wheniwork">
          <option value="">Select</option>
          <option value="1">Yes</option>
          <option value="0">No</option>
        </select>
      </div>
      <div class="form-group">
        <label for="wheniwork_id">WhenIWork ID</label>
        <input type="number" name="wheniwork_id" id="wheniwork_id" class="form-control" />
      </div>
      <div class="form-group">
        <label for="is_contractor">Contractor</label>
        <select class="form-control" name="is_contractor" id="is_contractor">
          <option value="">Select</option>
          <option value="1">Yes</option>
          <option value="0">No</option>
        </select>
      </div>
      <div class="form-group">
        <label for="payweb_emp_id">Payweb Employee ID</label>
        <input type="number" name="payweb_emp_id" id="payweb_emp_id" class="form-control" />
      </div>
    </div>
    <div class="col-lg-12">
      <input type="submit" class="btn btn-md btn-primary" value="Submit">
    </div>
    <div class="col-lg-12">
      <p class="small mt-">* denotes required field</p>
    </div>
  </div>
</form>
</div>

@endsection

@push('scripts')
<script>
  $(document).ready(function() {
    $('.datepicker').datetimepicker({
      timepicker: false,
      format:'Y-m-d'
    });
  });

let path = "{{ route('customer_autocomplete') }}";
  $('#company').typeahead({
        items: 50,
      source:  function (query, process) {
          return $.get(path, { query: query }, function (data) {
              return process(data);
          });
      },
      items: 15,
      minLength: 3,
      highlighter: function (item, data) {
          var parts = item.split('#'),
              html = '<div class="row">';
              html += '<div class="col-md-2">';
              html += '</div>';
              html += '<div class="col-md-10 pl-0">';
              html += '<span>'+data.name+'</span>';
              html += '</div>';
              html += '</div>';

          return html;
      },
      afterSelect:function(data,value,text){
          $('#company-id').val(data.id)
      },
  })

let path2 = "{{ route('person_autocomplete') }}";
$('#emergency').typeahead({
        items: 50,
      source:  function (query, process) {
          return $.get(path2, { query: query }, function (data) {
              return process(data);
          });
      },
      items: 15,
      minLength: 3,
      highlighter: function (item, data) {
          var parts = item.split('#'),
              html = '<div class="row">';
              html += '<div class="col-md-2">';
              html += '</div>';
              html += '<div class="col-md-10 pl-0">';
              html += '<span>'+data.name+ '</span>';
              html += '</div>';
              html += '</div>';

          return html;
      },
      afterSelect:function(data,value,text){
          $('#emergency_contact_id').val(data.id)
      },
  })  
  $('#reports_to').typeahead({
        items: 50,
      source:  function (query, process) {
          return $.get(path2, { query: query }, function (data) {
              return process(data);
          });
      },
      items: 15,
      minLength: 3,
      highlighter: function (item, data) {
          var parts = item.split('#'),
              html = '<div class="row">';
              html += '<div class="col-md-2">';
              html += '</div>';
              html += '<div class="col-md-10 pl-0">';
              html += '<span>'+data.name+ '</span>';
              html += '</div>';
              html += '</div>';

          return html;
      },
      afterSelect:function(data,value,text){
          $('#reports_to_id').val(data.id)
      },
  })
</script>

@endpush
@extends('layouts.app')

@section('content')

<div class="text-gray-900 mt-5">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  <div>
    <div class="d-flex justify-content-between align-items-start">
      <h2 class="my-4 mb-2 h1 font-weight-bold text-center">
        {{ $person->person->first_name }} {{ $person->person->middle_name }} {{ $person->person->last_name }}
      </h2>
      <p class="m-0 text-right">
        <a href="/people">Back to People</a><br>
        <a class="btn btn-info mr-0" href="/people/{{$person->id}}/edit">Edit</a>
      </p>
    </div>
    <div class="mt-5">
      <table class="table table-sm">
        <tr>
          <td>
            <strong>Name: </strong>
          </td>
          <td>
            {{$person->person->first_name}} {{$person->person->middle_name}} {{$person->person->last_name}}
          </td>
        </tr>
        <tr>
          <td>
            <strong>email: </strong>
          </td>
          <td>
            <a href="mailto:{{$person->person->email}}">{{$person->person->email}}</a>
          </td>
        </tr>
        <tr>
          <td>
            <strong>Phone: </strong>
          </td>
          <td>
            <a href="tel:{{$person->person->phone}}">{{$person->person->phone}}</a>
          </td>
        </tr>
        <tr>
          <td>
            <strong>Mobile Phone: </strong>
          </td>
          <td>
            <a href="tel:{{$person->person->mobile_phone}}">{{$person->person->mobile_phone}}</a>
          </td>
        </tr>
        <tr>
          <td>
            <strong>Company: </strong>
          </td>
          <td>
            {{$person->company->name ?? ''}}
          </td>
        </tr>
        <tr>
          <td>
            <strong>Emergency Contact: </strong>
          </td>
          <td>
            {{$person->emerg_contact ?? ''}}
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>
@endsection
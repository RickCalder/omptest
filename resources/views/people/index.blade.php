@extends('layouts.app')

@section('content')

<div class="text-gray-900 mt-5">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  <div>
    <h2 class="mt-4 mb-2 h1 font-weight-bold text-center">People</h2>
    @include('components/people-filter')
    <table class="table table-sm table-striped table-hover mt-4">
      <thead>
        <tr>
          <th>
            Name
          </th>
          <th>
            Company
          </th>
          <th>
            Email
          </th>
          <th>
            Phone
          </th>
          <th>

          </th>
        </tr>
      </thead>
      <tbody>
        @foreach($people as $individual)
        <tr>
          <td>
            {{ $individual->person->first_name }} {{ $individual->person->last_name }}
          </td>
          <td>
            {{ $individual->company->name }}
          </td>
          <td>
            <a href="mailto:{{$individual->email}}">
            {{ $individual->email }}
            </a>
          </td>
          <td>
            <a href="tel:{{$individual->phone}}">
              {{ $individual->phone }}
            </a>
          </td>
          <td>
            <a href="/people/{{$individual->id}}" class="btn btn-sm btn-primary">View</a>
            <a href="/people/{{$individual->id}}/edit" class="btn btn-sm btn-info ml-3">Edit</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <div class="d-flex justify-end my-4">
    @if ($people->hasPages())
    {{ $people->appends(Request::except('page'))->links() }}
    @endif
  </div>
</div>
@endsection
@extends('layouts.app')

@section('content')
<div class="text-gray-900 mt-5 mb-5">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  <h2 class="text-center my-8 text-3xl font-extrabold">Add New Person</h2>
  <form class="mt-4 w-100" id="create-person-form" method="POST" action="{{route('store-person')}}">
    <div class="row">
      @csrf
      <div class="col-lg-6">
        <div class="form-group">
          <label for="title">Title</label>
          <select id="title" name="title" type='text' class="form-control">
            <option value="">Select Title</option>
            <option value="Mr.">Mr.</option>
            <option value="Mrs.">Mrs.</option>
            <option value="Ms.">Ms.</option>
            <option value="Miss">Miss</option>
          </select>
        </div>
        <div class="form-group">
          <label for="first_name">First Name <span>*</span></label>
          <input type="text" name="first_name" id="first_name" class="form-control" required />
        </div>
        <div class="form-group">
          <label for="middle_name">Middle Name</label>
          <input type="text" name="middle_name" id="middle_name" class="form-control" />
        </div>
        <div class="form-group">
          <label for="last_name">Last Name<span>*</span></label>
          <input type="text" name="last_name" id="last_name" class="form-control" required />
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" name="email" id="email" class="form-control" />
        </div>
        <div class="form-group">
          <label for="phone">Phone Primary *</label>
          <input type="text" name="phone" id="phone" class="form-control" required/>
        </div>
        <div class="form-group">
          <label for="mobile_phone">Phone Secondary</label>
          <input type="text" name="mobile_phone" id="mobile_phone" class="form-control" />
        </div>
        <div class="form-group">
          <label for="emergency">Emergency Contact</label>
          <input class="form-control" type="text" name="emerg_contact" id="emergency" />
        </div>
        <div class="form-group">
          <label for="notes">Notes</label>
          <textarea name="notes" id="notes" class="form-control"></textarea>
        </div>
        <div class="form-group">
          <label for="company">Company</label>
          <input class="typeahead form-control" type="text" name="company" id="company" />
          <input type="hidden" id="company-id" name="company_id" />
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <input type="submit" class="btn btn-md btn-primary" value="Submit">
      </div>
      <div class="col-lg-12">
        <p class="small mt-">* denotes required field</p>
      </div>
    </div>
</form>
</div>

@endsection

@push('scripts')
<script>
  $(document).ready(function() {
    $('.datepicker').datetimepicker({
      timepicker: false
      , format: 'Y-m-d'
    });
  });

</script>

@endpush

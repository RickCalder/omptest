@extends('layouts.app')

@section('content')
<div class="text-center text-gray-900 mt-5">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  <h2 class="my-8 text-3xl font-extrabold">{{$project->id}} - {{$project->customer->name}} -
    {{$project->name}}</h2>
    
<div class="d-flex flex-column">
  <div class="my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
    <div class="align-middle inline-block min-w-full overflow-hidden sm:rounded-lg">
      <div class="border text-left p-4">
        <div class="d-flex justify-content-between align-items-center">
          <h2 class="mb-4 font-bold text-xl">Project</h2>
          <div class="d-flex flex-column">
            <a href="{{route('create-service', ['project_id' => $project->id])}}" class="btn btn-sm btn-info">Add Service</a>
            <a href="{{route('edit-project', ['id' => $project->id])}}" class="btn btn-sm btn-info mt-2">Edit Project</a>
          </div>
        </div>
        <table>
          <tr>
            <td>
              <span class="font-weight-bold col">Customer: </span>
            </td>
            <td>
              <span><a href="/companies/{{$project->customer->id}}">{{$project->customer->name}}</a> </span>
            </td>
          </tr>
          <tr>
            <td>
              <span class="font-weight-bold col">Sub Customer: </span>
            </td>
            <td>
              @if($project->sub_customer)
              <span>{{$project->sub_customer->name}} </span>
              @endif
            </td>
          </tr>
          <tr>
            <td>
              <span class="font-weight-bold col">Entered By: </span>
            </td>
            <td>
              @if($project->entered)
              <span>{{$project->entered->person->first_name}} {{$project->entered->person->last_name}}</span>
              @endif
            </td>
          </tr>
          <tr>
            <td>
              <span class="font-weight-bold col">Booked By: </span>
            </td>
            <td>
              @if($project->booked)
              <span>{{$project->booked->person->first_name}} {{$project->booked->person->last_name}}</span>
              @endif
            </td>
          </tr>
          <tr>
            <td>
              <span class="font-weight-bold col">Managed By: </span>
            </td>
            <td>
              @if($project->managed)
              <span>{{$project->managed->person->first_name}} {{$project->managed->person->last_name}}</span>
              @endif
            </td>
          </tr>
          <tr>
            <td>
              <span class="font-weight-bold col">Changed On: </span>
            </td>
            <td>
              <span>{{$project->changed_on}} </span>
            </td>
          </tr>
          <tr>
            <td>
              <span class="font-weight-bold col">PO Number: </span>
            </td>
            <td>
              <span>{{$project->po_number}} </span>
            </td>
          </tr>
          <tr>
            <td>
              <span class="font-weight-bold col">Quote Type: </span>
            </td>
            <td>
              <span>{{$project->default_quote_type}} </span>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>

    <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
      <div class="align-middle inline-block min-w-full overflow-hidden sm:rounded-lg">
        <div class="border mb-4 text-left p-4">
          <h2 class="mb-4 font-bold text-xl">Services</h2>
          <ul>
            @foreach($project->project_services as $service)
            <li>
              {{$service->id}} - {{$service->service->name}} - {{$service->occurs_on}} - {{$service->occurs_at}} - <a href="/project_service/{{$service->id}}">View</a>
            </li>
            @endforeach
          </ul>
        </div>
      </div>
    </div>
  </div>
  @endsection
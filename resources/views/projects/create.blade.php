@extends('layouts.app')

@section('add-head')
@endsection 

@section('content')
<div class="text-gray-900 mt-5 mb-5">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  <h2 class="text-center my-8 text-3xl font-extrabold">Create New Project</h2>
  <div class="row">
    <div class="col-lg-8">
      <form class="mt-4" id="create-project-service-form" method="POST" action="{{route('store-project')}}">
        @csrf
        <div class="form-group">
          <label for="name">Name</label>
          <div id="customers-div">
            <input class="form-control" type="text" name="name" id="name" required />
          </div>
        </div>
        <div class="form-group">
          <label for="customer">Customer</label>
          <div id="customers-div">
            @if(isset($customer->id))
            <span>{{$customer->name}}</span>
            <input type="hidden" id="customer-id" name="customer_id" value="{{$customer->id}}"/>
            @else
            <input class="typeahead form-control" type="text" name="customer" id="customer" required />
            <input type="hidden" id="customer-id" name="customer_id"/>
            @endif
          </div>
        </div>
        <div class="form-group">
          <label for="subcustomer">Sub Customer</label>
          <div id="customers-div">
            <input class="typeahead form-control" type="text" name="sub_customer" id="sub_customer" />
            <input type="hidden" id="sub-customer-id" name="sub_customer_id"/>
          </div>
        </div>
        <div class="form-group">
          <label for="branch">Branch</label>
          <select name="branch" class="form-control" id="branch" required>
            <option value="">Select Branch</option>
            @foreach($branches as $branch)
            <option value="{{$branch->id}}">{{$branch->name}}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="booked_by">Booked By (Leave blank to default to you)</label>
          <input class="typeahead form-control" type="text" name="booked" id="booked" />
          <input type="hidden" name="booked_by" id="booked_by" value="{{$project->booked_by ?? ''}}" />
        </div>
        <div class="form-group">
          <label for="managed_by">Managed By (Leave blank to default to you)</label>
          <input class="typeahead form-control" type="text" name="managed" id="managed" />
          <input type="hidden" name="managed_by" id="managed_by" value="{{$project->managed_by ?? ''}}" />
        </div>
        <div class="form-group">
          <label for="default_quote_type">Quote Type</label>
          <select name="default_quote_type" class="form-control" id="default_quote_type" required>
            <option value="">Select Quote Type</option>
            <option value="fixed">Fixed</option>
            <option value="hourly">Hourly</option>
          </select>
        </div>
        <div class="form-group">
          <label for="po_number">PO Number</label>
          <div>
            <input class="form-control" type="text" name="po_number" id="po_number" />
          </div>
        </div>
        <div class="form-group">
          <input type="submit" class="btn btn-primary" id="create-service">
        </div>
      </form>
    </div>
    <div class="col-lg-4 mt-4">
      <p class="mb-0" id="name-detail"></p>
      @if(isset($customer->id))
      <p class="mb-0" id="customer-detail">
        <strong>Customer:</strong> {{$customer->name}}
      </p>
      @else 
      <p class="mb-0" id="customer-detail"></p>
      @endif
      <p class="mb-0" id="sub-customer-detail"></p>
      <p class="mb-0" id="branch-detail"></p>
      <p class="mb-0" id="booked-detail"></p>
      <p class="mb-0" id="managed-detail"></p>
      <p class="mb-0" id="quote-detail"></p>
      <p class="mb-0" id="po-detail"></p>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script>


  $('#branch').change( function(e) {
    if(branch !== '') {
      $('#branch-detail').html('<strong>Branch: </strong>' + $(this).find('option:selected').text())
      let branch_id = $(this).find('option:selected').val()
      $('#booked_by').prop('disabled', true)
      $.ajax({
        type:'POST',
        url:"{{ route('get-booked-by') }}",
        data:{branch_id: branch_id, _token: "{{ csrf_token() }}",},
        success:function(data){
          console.log(data)
          $('#booked_by option').remove()
          $("#booked_by").append('<option value="">Select Booked By</option>')
          $.each(data.booked_by, function(){
            
              $("#booked_by").append('<option value="'+ this.id +'">'+ this.name +'</option>')
          })
          $('#booked_by').prop('disabled', false)
          getManagedBy()
        }
      });
    }
  })
  
  $('#booked_by').change(function() {
    $('#booked-detail').html('<strong>Booked By: </strong>' + $(this).find(':selected').text())
  })

  $('#name').on('blur', function(){
    $('#name-detail').html('<strong>Name: </strong>' + $(this).val())
  })

$('#sub_customer').change(function(){
  $('#sub-customer-detail').html('<strong>Sub Customer: </strong>' + $(this).find('option:selected').text())
})

$('#managed_by').change(function(){
  $('#managed-detail').html('<strong>Managed By: </strong>' + $(this).find('option:selected').text())
})

$('#default_quote_type').change(function(){
  $('#quote-detail').html('<strong>Quote Type: </strong>' + $(this).find('option:selected').text())
})

$('#po_number').blur(function(){
  $('#po-detail').html('<strong>PO Number: </strong>' + $(this).val())
})

let path = "{{ route('customer_autocomplete') }}";
  $('#customer').typeahead({
        items: 50,
      source:  function (query, process) {
          return $.get(path, { query: query }, function (data) {
              return process(data);
          });
      },
      items: 'all',
      highlighter: function (item, data) {
          var parts = item.split('#'),
              html = '<div class="row">';
              html += '<div class="col-md-2">';
              html += '</div>';
              html += '<div class="col-md-10 pl-0">';
              html += '<span>'+data.name+'</span>';
              html += '</div>';
              html += '</div>';

          return html;
      },
      afterSelect:function(data,value,text){
          $('#customer-id').val(data.id)
          $('#customer-detail').html('<strong>Customer: </strong>' + data.name)
      },
  })

  $('#sub_customer').typeahead({
        items: 50,
      source:  function (query, process) {
          return $.get(path, { query: query }, function (data) {
              return process(data);
          });
      },
      highlighter: function (item, data) {
          var parts = item.split('#'),
              html = '<div class="row">';
              html += '<div class="col-md-2">';
              html += '</div>';
              html += '<div class="col-md-10 pl-0">';
              html += '<span>'+data.name+'</span>';
              html += '</div>';
              html += '</div>';

          return html;
      },
      afterSelect:function(data,value,text){
          $('#sub-customer-id').val(data.id)
          $('#sub-customer-detail').html('<strong>Sub Customer: </strong>' + data.name)
      },
  })
  
  let path2 = "{{ route('person_autocomplete') }}";
  $('#booked').typeahead({
        items: 50,
      source:  function (query, process) {
          return $.get(path2, { query: query }, function (data) {
              console.log(data)
              return process(data);
          });
      },
      items: 'all',
      showHintOnFocus: false,
      highlighter: function (item, data) {
          var parts = item.split('#'),
              html = '<div class="row">';
              html += '<div class="col-md-2">';
              html += '</div>';
              html += '<div class="col-md-10 pl-0">';
              html += '<span>'+data.name+'</span>';
              html += '</div>';
              html += '</div>';


          return html;
      },
      afterSelect:function(data,value,text){
          $('#booked_by').val(data.id)
      },
  })
  
  $('#managed').typeahead({
        items: 50,
      source:  function (query, process) {
          return $.get(path2, { query: query }, function (data) {
              return process(data);
          });
      },
      items: 'all',
      showHintOnFocus: false,
      highlighter: function (item, data) {
          var parts = item.split('#'),
              html = '<div class="row">';
              html += '<div class="col-md-2">';
              html += '</div>';
              html += '<div class="col-md-10 pl-0">';
              html += '<span>'+data.name+'</span>';
              html += '</div>';
              html += '</div>';

          return html;
      },
      afterSelect:function(data,value,text){
          $('#managed_by').val(data.id)
      },
  })
  
</script>


@endpush 
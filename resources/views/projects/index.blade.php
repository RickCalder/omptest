@extends('layouts.app')

@section('content')

<div class="text-gray-900 mt-5">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  <div>
    <h2 class="mt-4 mb-2 h1 font-weight-bold text-center">Projects</h2>
    @include('components/project-filter')
    <table class="table table-sm table-striped table-hover mt-4">
      <thead>
        <tr>
          <th>
            Name
          </th>
          <th>
            Customer
          </th>
          <th>
            Branch
          </th>
          <th>

          </th>
        </tr>
      </thead>
      <tbody>
        @foreach($projects as $project)
        <tr>
          <td>
            {{ $project->name }}
          </td>
          <td>
           @if($project->customer) {{ $project->customer->name }}@endif  @if($project->sub_customer) ({{ $project->sub_customer->name}}) @endif
          </td>
          <td>
            {{ $project->branch->name }}
          </td>
          <td>
            <a href="/projects/{{$project->id}}" class="btn btn-sm btn-primary">View</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <div class="d-flex justify-end my-4">
    @if ($projects->hasPages())
    {{ $projects->appends(Request::except('page'))->links() }}
    @endif
  </div>
</div>
@endsection
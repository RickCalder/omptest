@extends('layouts.app')

@section('add-head')
@endsection 

@section('content')
<div class="text-gray-900 mt-5 mb-5">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  <h2 class="text-center my-8 text-3xl font-extrabold">Edit Project</h2>
  <div class="row">
    <div class="col-lg-8">
      <form class="mt-4" id="create-project-service-form" method="POST" action="{{route('update-project', $project->id)}}">
        @method('PUT')
        @csrf
        <div class="form-group">
          <label for="name">Name</label>
          <div id="customers-div">
            <input class="form-control" type="text" name="name" id="name" required value="{{$project->name}}" />
          </div>
        </div>
        <div class="form-group">
          <label for="customer">Customer</label>
          <div id="customers-div">
            <input class="typeahead form-control" type="text" name="customer" id="customer" value="{{$customer->name ?? ''}}" />
            <input type="hidden" id="customer-id" name="customer_id" value="{{$customer->id ?? ''}}"/>
          </div>
        </div>
        <div class="form-group">
          <label for="subcustomer">Sub Customer</label>
          <div id="customers-div">
            <input class="typeahead form-control" type="text" name="sub_customer" id="sub_customer" value="{{$sub_customer->name ?? ''}}" />
            <input type="hidden" id="sub-customer-id" name="sub_customer_id" value="{{$sub_customer->id ?? ''}}"/>
          </div>
        </div>
        <div class="form-group">
          <label for="branch">Branch</label>
          <select name="branch" class="form-control" id="branch" required>
            <option value="">Select Branch</option>
            @foreach($branches as $branch)
            <option value="{{$branch->id}}" @if($project->branch_id == $branch->id)selected @endif>{{$branch->name}}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="booked_by">Booked By</label>
          @if($project->id)
          <input class="typeahead form-control" type="text" name="booked" id="booked" value="{{$project->booked->person->first_name ?? ''}} {{$project->booked->person->last_name ?? ''}}" />
          @else
          <input class="typeahead form-control" type="text" name="booked" id="booked" />
          @endif
          <input type="hidden" name="booked_by" id="booked_by" value="{{$project->booked_by ?? ''}}" />
        </div>
        <div class="form-group">
          <label for="managed_by">Managed By</label>
          @if($project->id)
          <input class="typeahead form-control" type="text" name="managed" id="managed" value="{{$project->managed->person->first_name ?? ''}} {{$project->managed->person->last_name ?? ''}}" />
          @else
          <input class="typeahead form-control" type="text" name="managed" id="managed" />
          @endif
          <input type="hidden" name="managed_by" id="managed_by" value="{{$project->managed_by ?? ''}}" />
        </div>
        <div class="form-group">
          <label for="default_quote_type">Quote Type</label>
          <select name="default_quote_type" class="form-control" id="default_quote_type" required>
            <option value="">Select Quote Type</option>
            <option value="fixed" @if($project->default_quote_type == 'fixed')selected @endif>Fixed</option>
            <option value="hourly" @if($project->default_quote_type == 'hourly')selected @endif>Hourly</option>
          </select>
        </div>
        <div class="form-group">
          <label for="po_number">PO Number</label>
          <div>
            <input class="form-control" type="text" name="po_number" id="po_number" required value="{{$project->po_number}}" />
          </div>
        </div>
        <div class="form-group">
          <input type="submit" class="btn btn-primary" id="create-service">
          <a href="/projects/{{$project->id}}" class="btn btn-danger ml-3">Cancel</a>
        </div>
      </form>
    </div>
    <div class="col-lg-4 mt-4">
      {{-- <p class="mb-0" id="name-detail"></p>
      @if(isset($customer->id))
      <p class="mb-0" id="customer-detail">
        <strong>Customer:</strong> {{$customer->name}}
      </p>
      @else 
      <p class="mb-0" id="customer-detail"></p>
      @endif
      <p class="mb-0" id="sub-customer-detail"></p>
      <p class="mb-0" id="branch-detail"></p>
      <p class="mb-0" id="booked-detail"></p>
      <p class="mb-0" id="managed-detail"></p>
      <p class="mb-0" id="quote-detail"></p>
      <p class="mb-0" id="po-detail"></p> --}}
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script>

let path = "{{ route('customer_autocomplete') }}";
  $('#customer').typeahead({
    items: 100,
      source:  function (query, process) {
          return $.get(path, { query: query }, function (data) {
              return process(data);
          });
      },
      highlighter: function (item, data) {
          var parts = item.split('#'),
              html = '<div class="row">';
              html += '<div class="col-md-2">';
              html += '</div>';
              html += '<div class="col-md-10 pl-0">';
              html += '<span>'+data.name+'</span>';
              html += '</div>';
              html += '</div>';

          return html;
      },
      afterSelect:function(data,value,text){
          $('#customer-id').val(data.id)
      },
  })

  $('#sub_customer').typeahead({
        items: 50,
      source:  function (query, process) {
          return $.get(path, { query: query }, function (data) {
              return process(data);
          });
      },
      highlighter: function (item, data) {
          var parts = item.split('#'),
              html = '<div class="row">';
              html += '<div class="col-md-2">';
              html += '</div>';
              html += '<div class="col-md-10 pl-0">';
              html += '<span>'+data.name+'</span>';
              html += '</div>';
              html += '</div>';

          return html;
      },
      afterSelect:function(data,value,text){
          $('#sub-customer-id').val(data.id)
      },
  })
  
let path2 = "{{ route('person_autocomplete') }}";
  $('#booked').typeahead({
        items: 50,
      source:  function (query, process) {
          return $.get(path2, { query: query }, function (data) {
              return process(data);
          });
      },
      highlighter: function (item, data) {
          var parts = item.split('#'),
              html = '<div class="row">';
              html += '<div class="col-md-2">';
              html += '</div>';
              html += '<div class="col-md-10 pl-0">';
              html += '<span>'+data.name+'</span>';
              html += '</div>';
              html += '</div>';

          return html;
      },
      afterSelect:function(data,value,text){
          $('#booked_by').val(data.id)
      },
  })
  
  $('#managed').typeahead({
        items: 50,
      source:  function (query, process) {
          return $.get(path2, { query: query }, function (data) {
              return process(data);
          });
      },
      highlighter: function (item, data) {
          var parts = item.split('#'),
              html = '<div class="row">';
              html += '<div class="col-md-2">';
              html += '</div>';
              html += '<div class="col-md-10 pl-0">';
              html += '<span>'+data.name+'</span>';
              html += '</div>';
              html += '</div>';

          return html;
      },
      afterSelect:function(data,value,text){
          $('#managed_by').val(data.id)
      },
  })
</script>


@endpush 
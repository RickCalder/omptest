@extends('layouts.app')

@section('content')

<div class="text-gray-900 mt-5">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  <div>
    <h2 class="mt-4 mb-2 h1 font-weight-bold text-center">Service Roles</h2>
    <div class="d-flex justify-content-end mt-2">
      <a href="/service_role/create" class="btn btn-sm btn-info">Add Service Role</a>
    </div>
    {{-- @include('components/people-filter') --}}
    
    <div class="d-flex flex-column">
      <div class="my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
      </div>
    </div>
    <table class="table table-sm table-striped table-hover mt-4">
      <thead>
        <tr>
          <th>
            Name
          </th>
          <th>
            Rank
          </th>
          <th>
            Location
          </th>
        </tr>
      </thead>
      <tbody>
        @foreach($service_roles as $item)
        <tr>
          <td>
            {{ $item->name }}
          </td>
          <td>
            {{$item->rank}}
          </td>
          <td>
            @if($item->location)
            {{ $item->location->name }}
            @endif
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <div class="d-flex justify-end my-4">
    @if ($service_roles->hasPages())
    {{ $service_roles->appends(Request::except('page'))->links() }}
    @endif
  </div>
</div>
@endsection
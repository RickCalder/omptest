@extends('layouts.app')

@section('content')
<div class="text-center my-5">
<input type="text" id="pac-input"><br>
<p id="address1"></p>
<p id="locality"></p>
<p id="state"></p>
<p id="country"></p>
<p id="postal_code"></p>
</div>
<script>
let autocomplete
function initAC() {
  autocomplete = new google.maps.places.Autocomplete(
    document.getElementById('pac-input'),
    {
      fields: ['place_id', 'geometry', 'name', 'address_components']
    }
  )
autocomplete.addListener('place_changed', onPlaceChanged)
}

function onPlaceChanged() {
  var place = autocomplete.getPlace()

  if (!place.geometry) {
    document.getElementById('pac-input').placeholder = 'Enter an address'
  } else {
    console.log(place.address_components)
    fillInAddress()
  }
}

function fillInAddress() {
  // Get the place details from the autocomplete object.
  const place = autocomplete.getPlace();
  let address1 = "";
  let postcode = "";

  // Get each component of the address from the place details,
  // and then fill-in the corresponding field on the form.
  // place.address_components are google.maps.GeocoderAddressComponent objects
  // which are documented at http://goo.gle/3l5i5Mr
  console.log(place.geometry.location.lat())
  for (const component of place.address_components) {
    // @ts-ignore remove once typings fixed
    const componentType = component.types[0];
    console.log(component.long_name)

    switch (componentType) {
      case "street_number": {
        address1 = `${component.long_name} ${address1}`;
        break;
      }

      case "route": {
        address1 += component.short_name;
        break;
      }

      case "postal_code": {
        postcode = `${component.long_name}${postcode}`;
        document.querySelector("#postal_code").innerHTML = component.long_name;
        break;
      }

      case "postal_code_suffix": {
        postcode = `${postcode}-${component.long_name}`;
        break;
      }
      case "locality":
        document.querySelector("#locality").innerHTML = component.short_name;
        break;
      case "administrative_area_level_1": {
        document.querySelector("#state").innerHTML = component.short_name;
        break;
      }
      case "country":
        document.querySelector("#country").innerHTML = component.short_name;
        break;
    }
    document.getElementById('address1').innerHTML = address1
  }

  console.log(address1)
}
</script>
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDIJ1CzwFZ5LOESivAyQvGoKHcRyBG4t6E&loading=async&libraries=places&callback=initAC">
</script>
@endsection

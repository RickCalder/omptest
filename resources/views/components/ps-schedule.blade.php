@if($payroll)
<div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
  <div class="align-middle inline-block min-w-full overflow-hidden sm:rounded-lg">
    <div class="border mb-4 text-left p-4">
      <div class="d-flex justify-content-between">
        <h2 class="font-bold text-xl">Scheduling History</h2> 
      </div>
      <div class="schedule-toggle">
      @if($project_service->api_updated)
        <p><strong>Scheduled on</strong>: {{ $project_service->api_updated}}</p>             
        <form action="{{ route('ajax_update_payroll_api') }}" method="POST" class="form-inline">
          @csrf
          <input type="hidden" name="api_pid" id="api-pid" class="form-control mr-2"
              value="{{ $project_service->id }}">
          <input type="submit" class="btn btn-sm btn-primary" id="update-api-{{ $project_service->id }}"
              value="Update Schedule">
        </form>
        
      <form action="{{ route('ajax_delete_payroll_api') }}" method="POST" class="form-inline">
          @csrf
          <input type="hidden" name="api_pid" id="api-pid" class="form-control mr-2"
              value="{{ $project_service->id }}">
          <input type="submit" class="btn btn-sm btn-danger mt-3" id="delete-api-{{ $project_service->id }}"
                  value="Delete from Schedule">
      </form>
      @else 
        <p>This job has not been scheduled.</p>   
          @if ($project_service->nickname !== null && $project_service->nickname !== '' && $project_service->status === 'booked')
              @if ($project_service->api_updated === null)
                <form action="{{ route('ajax_send_payroll_api') }}" method="POST" class="form-inline">
                    @csrf
                    <input type="hidden" name="api_pid" id="api-pid" class="form-control mr-2"
                        value="{{ $project_service->id }}">
                    <input type="submit" class="btn btn-sm btn-info" id="send-api-{{ $project_service->id }}"
                        value="Schedule">
                </form>
              @endif
          @else
              <form action="{{ route('ajax_send_payroll_api') }}" method="POST" class="form-inline">
                  @csrf
                  <input type="hidden" name="api_pid" id="api-pid" class="form-control mr-2"
                      value="{{ $project_service->id }}">
                  @if ($project_service->api_updated === null)
                      <input type="submit" class="btn btn-sm btn-info" id="send-api-{{ $project_service->id }}"
                          value="Schedule" disabled>
                      <p class="mb-0 ml-4"><em>*Note - service must have a nickname and a status of booked to be scheduled.</em></p>
                  @else
                  <input type="submit" class="btn btn-sm btn-primary" id="send-api-{{ $project_service->id }}"
                      value="Update Schedule" disabled>
                  @endif
              </form>
          @endif
      @endif
      </div>
    </div>
  </div>
</div>
@endif
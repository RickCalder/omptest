@foreach ($project_services as $project_service)
  <div class="border py-2 px-4 mb-2">
    <div class="d-flex justify-content-between">
    <h2 class="font-weight-normal h5 mb-0 text-left"><span class="small">{{ $project_service->occurs_on }} - {{ $project_service->occurs_at }}</span><br> {{ $project_service->id }} -
        {{ $project_service->project->customer->name }} -
        {{ $project_service->branch->name }}</h2>
      <div>
        <a href="/project_service/{{ $project_service->id }}" class="btn btn-sm btn-primary">View Service</a>
        <a href="/projects/{{ $project_service->project_id }}" class="btn btn-sm btn-primary">View Project</a>
        <a href="" class="expand-this ml-3" data-expand="e-{{$project_service->id}}"><i class="fa fa-chevron-down"></i></a>
      </div>
    </div>
    <div class="expand-div e-{{$project_service->id}}">
      <div class="mb-2 text-left d-flex justify-content-between">
        <div>
          @if (isset($project_service->project->sub_customer))
              {{ $project_service->project->sub_customer->name }}<br>
          @endif
          {{ $project_service->project->name }}<br>
          {{ $project_service->service->name }}<br>
          <form class="form-inline">
            <div class="form-group">
              <label for="nickname">Nickname: </label>
              <input type="text" id="nick-{{ $project_service->id }}" name="nickname"
                  class="form-control form-control-sm ml-2" value="{{ $project_service->nickname }}"
                  maxlength="20">
              <button class="btn btn-primary btn-sm ml-2 update-nickname"
                  data-service="{{ $project_service->id }}">Update</button>
              <span class="text-primary ml-3" id="notify-{{ $project_service->id }}"></span>
            </div>
          </form>
      </div>
        <div class="text-right">
          <div class="form-group mb-0 mt-2">
            <select name="status" class="form-control status form-control-sm" data-service="{{ $project_service->id }}">
              <option value="">Select Status</option>
              <option value="entered" @if ($project_service->status == 'entered') selected @endif>Entered</option>
              <option value="pending" @if ($project_service->status == 'pending') selected @endif>Pending</option>
              <option value="booked" @if ($project_service->status == 'booked') selected @endif>Booked</option>
              <option value="incomplete" @if ($project_service->status == 'incomplete') selected @endif>Incomplete</option>
              <option value="completed" @if ($project_service->status == 'completed') selected @endif>Completed</option>
              <option value="cancelled" @if ($project_service->status == 'cancelled') selected @endif>Cancelled</option>
            </select>
          </div>
        </div>
      </div>
      <div class="d-flex justify-content-end flex-wrap">
        @php 
          $payroll = false;
          foreach($project_service->branch->apis as $api) {
            if($api->api_name->name =='Payroll API') {
              $payroll = true;
            }
          }
        @endphp
        @if($payroll)
          @if ($project_service->nickname !== null && $project_service->nickname !== '' && $project_service->status === 'booked')
              @if ($project_service->api_updated === null)
                <form action="{{ route('ajax_send_payroll_api') }}" method="POST" class="form-inline">
                    @csrf
                    <input type="hidden" name="api_pid" id="api-pid" class="form-control mr-2"
                        value="{{ $project_service->id }}">
                    <input type="submit" class="btn btn-sm btn-info" id="send-api-{{ $project_service->id }}"
                        value="Schedule">
                </form>
              @else                
                <form action="{{ route('ajax_update_payroll_api') }}" method="POST" class="form-inline">
                    @csrf
                    <input type="hidden" name="api_pid" id="api-pid" class="form-control mr-2"
                        value="{{ $project_service->id }}">
                    <input type="submit" class="btn btn-sm btn-primary" id="update-api-{{ $project_service->id }}"
                        value="Update Schedule">
                  </form>
              @endif
          @else
              <form action="{{ route('ajax_send_payroll_api') }}" method="POST" class="form-inline">
                  @csrf
                  <input type="hidden" name="api_pid" id="api-pid" class="form-control mr-2"
                      value="{{ $project_service->id }}">
                  @if ($project_service->api_updated === null)
                      <input type="submit" class="btn btn-sm btn-info" id="send-api-{{ $project_service->id }}"
                          value="Schedule" disabled>
                  @else
                  <input type="submit" class="btn btn-sm btn-primary" id="send-api-{{ $project_service->id }}"
                      value="Update Schedule" disabled>
                  @endif
              </form>
          @endif
        @endif
        <form action="{{ route('supervisor-control') }}" method="POST" class="form-inline">
            @csrf
            <input type="hidden" name="sc_pid" id="sc-pid" class="form-control mr-2"
                value="{{ $project_service->id }}">
            <input type="submit" class="ml-1 btn btn-sm btn-dark" value="Supervisor Control Sheet">
        </form>


        <form action="{{ route('time-sheet') }}" method="POST" class="form-inline">
            @csrf
            <input type="hidden" name="ts_pid" id="ts-pid" class="form-control mr-2"
                value="{{ $project_service->id }}">
            <input type="submit" class="btn btn-sm btn-dark ml-1" value="Time Sheet">
        </form>


        <form action="{{ route('work-order') }}" method="POST" class="form-inline">
            @csrf
            <input type="hidden" name="wo_pid" id="wo-pid" class="form-control mr-2"
                value="{{ $project_service->id }}">
            <input type="submit" class="btn btn-sm btn-dark ml-1" value="Work Order">
        </form>

      </div>
    </div>
  </div>
@endforeach

@push('scripts')
<script>
  
  $(".status").on("change", function(e){
      $(".status").attr('disabled', true)
      if($(this).val() == ''){
        $(".status").attr('disabled', false)
        return false
      } 
      let newStatus = $(this).val()
      let service = $(this).data('service')
      let nickname = $('#nick-' + service ).val()
      
      $.ajax({
        type:'POST',
        url:"{{route('ajax_update_status')}}",
        data:{
          id: $(this).data("service"), 
          status: $(this).val(),
          _token: "{{csrf_token()}}"
        },
        success:function(data){
          $(".status").attr('disabled', false)
          if(newStatus === 'booked' && (nickname === null || nickname === '')) {
            console.log('1')
            $("#send-api-" + service).attr('disabled', true)
          } else {
            console.log('2')
            $("#send-api-" + service).attr('disabled', false)
          }
        }
      });
    })

    $('.update-nickname').on('click', function(e) {
      e.preventDefault()
      let serviceId = $(this).data('service')
      let nickname = $("#nick-" + serviceId).val()
      if(nickname !== "") {
        $.ajax({
          type:'POST',
          url:"{{route('ajax_update_nickname')}}",
          data:{
            id: $(this).data("service"), 
            nickname: nickname,
            _token: "{{csrf_token()}}"
          },
          success:function(data){
            let status = $('.status[data-service="' + serviceId +'"]').find(':selected').val()
            if(status === 'booked') {
              $("#send-api-" + serviceId).attr('disabled', false)
            } else {
              $("#send-api-" + serviceId).attr('disabled', true)
            }
            $("#notify-" + serviceId).html('Updated')
          }
        });
      }
    })
</script>
@endpush
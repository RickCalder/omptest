<div>
  <div class="mb-4">
    <div class="border rounded text-left p-4 align-items-center" id="filter-panel">
      <form method="GET" action="{{route('dashboard')}}" class="inline-form" id="filter-form">
        @csrf
        <div class="d-flex row">
          <div class="col-md-4">
          <div class="form-group my-1">
            <label for="branch" class="sr-only">Branch</label>

            <select id="branch" name="branch" class="custom-select">
              @if(Auth()->user()->primary_location !== null && $current_branch === null || $current_branch === 'all')
                @if(auth()->user()->primary_location === 0)
                  <option value="all">All Branches</option>
                @else 
                  <option disabled>Select Branch</option>
                @endif
                @foreach($branches as $branch)
                <option @if(Auth()->user()->primary_location==$branch['id'])selected @endif
                  value="{{ $branch['id'] }}">{{$branch['name']}}</option>
                @endforeach

              @else
                @if(auth()->user()->primary_location === 0)
                  <option value="all">All Branches</option>
                @else 
                  <option disabled>Select Branch</option>
                @endif
                @foreach($branches as $branch)
                @if($current_branch == null) 
                  <option  value="{{ $branch['id'] }}">{{$branch['name']}}</option>
                @else
                  <option @if($current_branch->id == $branch['id'])selected @endif
                    value="{{ $branch['id'] }}">{{$branch['name']}}</option>
                @endif
                @endforeach
              @endif
            </select>
            <label for="status" class="sr-only">Status</label>
            <select id="status" name="status" class="custom-select mt-1">
              <option value="">All Statuses</option>
              <option @if($status=='booked' )selected @endif value="booked">Booked</option>
              <option @if($status=='cancelled' )selected @endif value="cancelled">Cancelled</option>
              <option @if($status=='completed' )selected @endif value="completed">Completed</option>
              <option @if($status=='entered' )selected @endif value="entered">Entered</option>
              <option @if($status=='incomplete' )selected @endif value="incomplete">Incomplete</option>
              <option @if($status=='pending' )selected @endif value="pending">Pending</option>
            </select>
            <label for="project_service_id" class="sr-only">SID</label>
            <input id="project_service_id" type="text" name="project_service_id" class="form-control my-1"
              placeholder="SID" value="{{$project_service_id}}" />
          </div>
          
          <div class="form-group my-1">
            <div class="input-group">
              <div class="input-group-prepend w-100">
                <div class="input-group-text w-100">
                  <label for="auto-filter" class="mb-0">
                    <input type="checkbox" id="auto-filter" name="auto_filter" checked>
                    Auto-filter
                  </label>
                  <div class="bg-dark rounded-circle ml-3 text-light d-flex align-items-center justify-content-center" data-toggle="tooltip" data-placement="top" style="height: 15px; width: 15px" title="When checked, changing Branch or Status will automatically filter the list">
                    <span class="p-1" style="font-size: 0.75rem">?</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group my-1">
            <label for="customer" class="sr-only">Customer</label>
            <div id="customers-div">
              @if(!empty($current_customer->id))
              <div class="input-group">
                <input class="typeahead form-control border-right-0" type="text" name="customer" id="customer" value="{{$current_customer->name}}" />
                <div class="input-group-append">
                  <span class="input-group-text bg-white border-left-0"><button id="customer-clear" class="btn btn-sm" style="padding:0" type="button"><i class="fa fa-times-circle"></i></button></span>
                </div>
              </div>
              <input type="hidden" id="customer-id" name="customer_id" value="{{$current_customer->id}}"/>
              @else
              <div class="input-group">
                <input class="typeahead form-control border-right-0" type="text" name="customer" id="customer" placeholder="Customer"/>
                <div class="input-group-append">
                  <span class="input-group-text bg-white border-left-0"><button id="customer-clear" class="btn btn-sm" style="padding:0" type="button"><i class="fa fa-times-circle"></i></button></span>
                </div>
              </div>
              <input type="hidden" id="customer-id" name="customer_id"/>
              @endif
            </div>
          </div>
          <div class="form-group my-1">
            <label for="subcustomer" class="sr-only">Sub Customer</label>
            <div id="customers-div">
              {{-- @if(!empty($current_sub_customer)) --}}
              <div class="input-group">
                {{-- <input class="typeahead form-control border-right-0" type="text" name="sub_customer" id="sub_customer" value="{{$current_sub_customer->name}}" /> --}}
                <select class="form-control border-right-0" name="sub_customer" id="sub_customers" disabled >
                  <option value="">Select Sub Customer</option>
                  @if(count($sub_customers) > 0)
                    @foreach($sub_customers as $sub)
                      <option value="{{ $sub->id }}"> {{ $sub->name }}</option>
                    @endforeach 
                  @endif 
                </select>
              </div>
            </div>
          </div>
          <div class="form-group my-1">
            <div class="input-group">
              <label for="projects" class="mb-0 sr-only">Projects</label>
                <select name="projects" id="projects" class="form-control">
                  <option value="">Select Project</option>
                  @if(isset($projects) > 0)
                    @foreach($projects as $project)
                      <option value="{{ $project->id }}" @if($project->id == $current_project)selected @endif>{{ $project->po_number}} &emdash; {{ $project->name }}</option>
                    @endforeach 
                  @endif 
                </select>
              </div>
          </div>
          <div class="form-group my-1">
            <div class="input-group">
              <div class="input-group-prepend w-100">
                <div class="input-group-text w-100">
                  <label for="collapsed" class="mb-0">
                    <input type="checkbox" id="collapsed" name="collapsed" checked >
                    Collapsed View
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-2">
          <div class="form-group">
            <label for="start_date"class="sr-only">Start Date</label>
            <input data-toggle="datepicker" class="form-control" name="start_date" id="start_date" 
            value="{{$start_date}}" placeholder="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" autocomplete="off">
            <div data-toggle="datepicker"></div>
            <label for="end_date"class="sr-only">Start Date</label>
            <input data-toggle="datepicker1" class="form-control mt-1" name="end_date" id="end_date" value="{{$end_date}}"  placeholder="End Date" autocomplete="off">
            <div data-toggle="datepicker1"></div>
            <div class="form-group my-1">
              <div class="input-group">
                <div class="input-group-prepend w-100">
                  <div class="input-group-text w-100">
                    <label for="all-dates" class="mb-0">
                      <input type="checkbox" id="all-dates" name="all_dates" {{$all_dates}} @if($current_customer == null) disabled @endif>
                      All Dates
                    </label>
                    <div class="bg-dark rounded-circle ml-3 text-light d-flex align-items-center justify-content-center" data-toggle="tooltip" data-placement="top" style="height: 15px; width: 15px" title="Returns all dates for the current query and ignores the date boxes above. Only available if a customer is selected.">
                      <span class="p-1" style="font-size: 0.75rem">?</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group my-1 d-flex align-items-center">

            <button type="submit" class="btn btn-primary">
              Filter
            </button>

            
            <label for="reset" class="mb-0">
              <input type="checkbox" id="reset" name="reset" class="ml-3">
              Reset
            </label>
            
          </div>
          <div>
            <a href="{{route('create-service')}}" class="btn btn-md btn-info mt-1">Create Service</a>
          </div>
          <div>
            <a href="{{route('create-project')}}" class="btn btn-md btn-info mt-1">Create Project</a>
          </div>
          <div>
            <button type="button" class="btn btn-dark mt-1" id="sd" data-toggle="modal" data-target="#summary-dispatch">Summary Dispatch</button>
          </div>
        </div>
        </div>
      </form>
    </div>
    <a href="" class="filter-toggle text-orange-500 h2" title="Hide/Show Filters"><i class="fa fa-chevron-up"
        aria-hidden="true"></i></a>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="summary-dispatch" tabindex="-1" aria-labelledby="summaryDispatchLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Print Summary Dispatch</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('summary-dispatch')}}" method="POST" class="form-inline" id="sd-form">
          @csrf
          <label for="sd-branch" class="sr-only">Summary Dispatch Branch</label>
          <select class="mr-2 form-control" name="branch" id="sd-branch" required>
            <option value="">Select Branch</option>
            @foreach($branches as $branch)
              <option value="{{ $branch['id'] }}">{{ $branch['name'] }}</option>
            @endforeach
          </select>
          <label for="date" class="sr-only">Date</label>
          <input data-toggle="datepicker1" class="form-control mr-2" name="date" id="sd-date" placeholder="Date" autocomplete="off">
          <div data-toggle="datepicker1"></div>
        </form>
        
      <div class="alert alert-danger mt-2 p-1" style="display: none" role="alert" id="sd-alert-div">
        <span id="sd-alert"></span>
      </div>
      </div>
      <div class="modal-footer">
        <input type="button" id="sd-submit" class="btn btn-primary" value="Submit">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

@php 
  if(request('project_service_id') != null) {
    $sid = request('project_service_id');
  } else {
    $sid='no_sid';
  }
@endphp

@push('scripts')

<script>
    $(document).ready(function() {

          
      $('#reset-link').on('click', function(e) {
        e.preventDefault()
        console.log('hi')
        $('#filter-form')[0].reset()
        window.location = '/dashboard'
      })
      let SID = "{{$sid}}"
      console.log(SID + 'asdfasdf')
      if(SID !== 'no_sid') {
        $('#collapsed')[0].checked = false
        $('.expand-div').slideDown()
        $('.expand-this > i').removeClass('fa-chevron-down').addClass('fa-chevron-up')
      }

      let subs = {!!json_encode($sub_customers)!!}
        //Populate Sub Customers
        
        let current_sub = {!! json_encode($current_sub_customer) !!}
        $('#sub_customers')
          .find('option')
          .remove()
          .end()
          .append('<option value="">Select Sub Customer</option>')
        let html =''
        for(let i =0; i < subs.length; i++ ) {
          if(subs[i].id === current_sub.id){
            html= `<option value="${subs[i].id}" selected>${subs[i].name}</option>`
          } else {
            html= `<option value="${subs[i].id}">${subs[i].name}</option>`
          }
          
          $('#sub_customers').append(html)
        }

        
        $('#sub_customers').prop('disabled', false)

      $('#start_date').datetimepicker({
        timepicker: false,
        format:'Y-m-d',
        date: new Date()
      });
      $('#end_date').datetimepicker({
        timepicker: false,
        format:'Y-m-d'
      });
      $('#sd-date').datetimepicker({
        timepicker: false,
        format:'Y-m-d'
      });
      $("#sd-submit").on("click", function(e){
        e.prevent_default
        // console.log(${"#sd-branch"}.value())
        if($("#sd-branch").val() == "") {
          let message = "Branch is required"
          $("#sd-alert").html(message)
          $("#sd-alert-div").slideDown()
          console.log(message)
          return false
        }
        if($("#sd-date").val() == "") {
          let message = "Date is required"
          $("#sd-alert").html(message)
          $("#sd-alert-div").slideDown()
          console.log(message)
          return false
        }
        $("#sd-form").submit()
      })
    })

    
    let path = "{{ route('customer_autocomplete') }}";
    $('#customer').typeahead({
        items: 50,
      source:  function (query, process) {
          return $.get(path, { query: query }, function (data) {
              return process(data);
          });
      },
      highlighter: function (item, data) {
          var parts = item.split('#'),
              html = '<div class="row">';
              html += '<div class="col-md-2">';
              html += '</div>';
              html += '<div class="col-md-10 pl-0">';
              html += '<span>'+data.name+'</span>';
              html += '</div>';
              html += '</div>';

          return html;
      },
      afterSelect:function(data,value,text){
          $('#customer-id').val(data.id)
          getSubCustomers(data.id)
          $('#all-dates').prop('disabled', false)
      },
  })

  function getSubCustomers(id) {
    $('#sub_customers').prop('disabled', true)
    $.ajax({
      type: "POST",
      url: '/project-service/get-sub-customers',
      data: {'customer' : id},
      success: function(result) {

        //Populate Sub Customers
        $('#sub_customers')
          .find('option')
          .remove()
          .end()
          .append('<option value="">Select Sub Customer</option>')
        let html =''
        for(let i =0; i < result.sub_customers.length; i++ ) {
          html= `<option value="${result.sub_customers[i].id}">${result.sub_customers[i].name}</option>`
          $('#sub_customers').append(html)
        }
        $('#sub_customers').prop('disabled', false)

        //Populste Projects
        
        $('#projects')
          .find('option')
          .remove()
          .end()
          .append('<option value="">Select Project</option>')
        let html2 =''
        for(let i =0; i < result.projects.length; i++ ) {
          html2= `<option value="${result.projects[i].id}">${result.projects[i].po_number} - ${result.projects[i].name}</option>`
          $('#projects').append(html2)
        }
        $('#projects').prop('disabled', false)
          
      }
    })
  }

  $('#sub_customers').change(function() {
    let selected = $(this).val()
    if(selected == '')
    return false
    $.ajax({
      type: "GET",
      url: '/project-service/get-projects-subcustomer',
      data: {
        'customer' : $('#customer-id').val(),
        'branch': $('#branch').val(),
        'sub_customer': selected
      },
      success: function(result) {

        
        $('#projects')
          .find('option')
          .remove()
          .end()
          .append('<option value="">Select Project</option>')
        let html2 =''
        for(let i =0; i < result.projects.length; i++ ) {
          html2= `<option value="${result.projects[i].id}">${result.projects[i].po_number} - ${result.projects[i].name}</option>`
          $('#projects').append(html2)
        }
        $('#projects').prop('disabled', false)
          
      }
    })
  })

$('#customer-clear').on('click', function(e) {
  $('#customer').val('').attr('placeholder', 'Customer')
  $('#customer-id').val('')
  $('#sub_customers').val('')
  $('#sub-customer-id').val('')
  $('#projects').val('')
  $('#all-dates').prop('disabled', true)
  $('#all-dates').prop('checked', false)
})

$('#subcustomer-clear').on('click', function(e) {
  $('#sub_customer').val('')
  $('#sub-customer-id').val('')
})

$("#branch").on("change", function(e) {
  if($("#auto-filter").is(":checked")) {
    $("#filter-form").submit()
  }
})
$("#status").on("change", function(e) {
  if($("#auto-filter").is(":checked")) {
    $("#filter-form").submit()
  }
})

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>

<script>
  $('#collapsed').on('click', function(e) {
    if($(this).is(':checked')) {
      $('.expand-div').slideUp()
      $('.expand-this > i').removeClass('fa-chevron-up').addClass('fa-chevron-down')
    }else {
      $('.expand-div').slideDown()
      $('.expand-this > i').removeClass('fa-chevron-down').addClass('fa-chevron-up')
    }
  })

</script>

@endpush
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="/dashboard">
    <img class="d-block" style="height:50px" src="/img/omp-white.svg" alt="OMP Logo" />
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link regular-link" href="{{ url('/dashboard') }}">Dashboard <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarAdminDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Admin
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ url('/companies') }}">Companies</a>
            <a class="dropdown-item" href="{{ url('/equipment') }}">Equipment</a>
            <a class="dropdown-item" href="{{ url('/people') }}">People</a>
            <a class="dropdown-item" href="{{ url('/projects') }}">Projects</a>
            <a class="dropdown-item" href="{{ url('/service_role') }}">Service Roles</a>
            <a class="dropdown-item" href="{{ url('/supplies') }}">Supplies</a>
            <a class="dropdown-item" href="{{ url('/reports') }}">Reports</a>
          </div>
        </li>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {{$user->name}}
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="/nova">Admin</a>
          <a class="dropdown-item" href="{{ url('/logout') }}"
          onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();">Log Out</a>
        </div>
      </li>
    </ul>
  </div>
  <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
</nav>
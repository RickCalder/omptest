
<div class="d-flex flex-column">
  <div class="my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
    <div class="align-middle inline-block min-w-full overflow-hidden sm:rounded-lg">
      <div class="border text-left p-4">
        <div class="d-flex justify-content-between">
          <h2 class="font-bold text-xl">Documents</h2> 
        </div>
        <div class="row">
          <div class="col-md-12">
            <form class="e-form" method="POST"id="upload-documents-form" action="{{route('file.upload')}}" enctype="multipart/form-data">
              @csrf
              <div class="form-group mr-2">
                <label for="description">File Description</label>
                <input type="text" id="description" name="description" class="form-control" name="fileName" required />
              </div>
              <div class="form-group mr-2">
                <input type="file" name="document" required>
              </div>
          
              @error('file')
              <div class="form-group mr-2 alert alert-danger">
                <span>{{ $message }}</span>
              </div>
              @enderror
              
              @if (session()->has('message'))
              <div class="form-group mr-2 alert alert-success">
                <span>{{ session('message') }}</span>
              </div>
              @endif
          
              <input type="hidden" value="{{$project_service->project->id}}" name="project"/>
              <input type="hidden" value="{{$project_service->id}}" name="project_service"/>
          
              <button class="btn btn-primary" type="submit">Save File</button>
          </form>
          </div>
        </div>
        @if($project_service->files->count() > 0)
        <div class="docs-toggle">
          <table class="table table-sm table-striped table-hover mt-4">
            <tbody>
              @foreach($project_service->files as $file)
              <tr id="row-{{$file->id}}">
                <td>
                  {{ $file->name }}
                </td>
                <td>
                  {{-- <a href="{{Storage::url($file->file_url) }}" target="_blank"> --}}
                    {{-- {{$file->file_url}} --}}
                    <a href="{{$file->file_url}}" target="_blank">
                    View
                  </a>
                </td>
                <td>
                  {{ $file->owned_by }}
                </td>
                <td style="text-align: right">
                  
                  <button data-file="{{$file->id}}" class="btn e-form btn-sm btn-danger delete-file"><i class="fa fa-trash"><span class="sr-only">Delete</span></i></button>
</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        @endif
      </div>
    </div>
  </div>
</div>

@push('scripts')
<script>

$(document).on('click', '.delete-file', function(e) {
    e.preventDefault()
    let id = $(this).data('file')
    let row = $(this).closest('tr')
    let res = confirm('Are you sure you want to remove this file?')
    if(res === true) {
      $.ajax({
        type:'DELETE',
        url:"{{route('delete_file_ps')}}",
        data:{id:id,
          _token: "{{csrf_token()}}"
        },
        success:function(data){
          if(data.result === 1) {
            row.fadeOut()
          }
        }
      });
    }
  })

  </script>
@endpush
<div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
  <div class="align-middle inline-block min-w-full overflow-hidden sm:rounded-lg">
    <div class="border mb-4 text-left p-4">
      <div class="d-flex justify-content-between">
        <h2 class="font-bold text-xl">Related Services</h2> 
      </div>
      <div class="related-toggle">
        <ul>
          @foreach($other_services as $service)
          <li>
            {{$service->id}} - {{$service->service->name}} - {{$service->occurs_on}} - {{$service->occurs_at}} - <a href="/project_service/{{$service->id}}">View</a>
          </li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
</div>
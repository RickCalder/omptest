
<div class="d-flex flex-column">
  <div class="my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
    <div class="align-middle inline-block min-w-full overflow-hidden sm:rounded-lg">
      <div class="border text-left p-4">
        <div class="d-flex justify-content-between align-items-center">
          <h2 class="mb-4 font-bold text-xl">Service</h2>
          <div>
            <a href="/project_service/{{$project_service->id}}/edit" class="btn btn-md btn-info">Edit</a>
            <button class="btn btn-md btn-info" id="duplicate-service" data-toggle="modal" data-target="#duplicate-modal">Duplicate</button>
          </div>
        </div>
        <table>
          <tr>
            <td>
              <span class="font-weight-bold col">Occurs On: </span></span>
            </td>
            <td>
              <span>{{$project_service->occurs_on}} </span></span>
            </td>
            <td>
              <span>{{$project_service->occurs_at}} </span>
            </td>
          </tr>
          <tr>
            <td>
              <span class="font-weight-bold col">Estimated Hours: </span>
            </td>
            <td>
              <span>{{\Carbon\Carbon::parse($project_service->minutes_estimate)->format('H:m')}}</span>
            </td>
          </tr>
          <tr>
            <td>
              <span class="font-weight-bold col">Service: </span>
            </td>
            <td>
              <span>{{$project_service->service->name}}</span>
            </td>
          </tr>
          <tr>
            <td>
              <span class="font-weight-bold col">Nickname: </span>
            </td>
            <td>
              <span>{{$project_service->nickname}}</span>
            </td>
          </tr>
          <tr>
            <td>
              <span class="font-weight-bold col">Branch: </span>
            </td>
            <td>
              <span>{{$project_service->branch->name}}</span>
            </td>
          </tr>
          <tr>
            <td>
              <span class="font-weight-bold col">Customer: </span>
            </td>
            <td>
              <span><a href="/companies/{{$project_service->project->customer->id}}">
              {{$project_service->project->customer->name}}</a></span>
            </td>
          </tr>
          <tr>
            <td>
              <span class="font-weight-bold col">Sub Customer: </span>
            </td>
            <td>
              @if($project_service->project->sub_customer)
              <span><a href="/companies/{{$project_service->project->sub_customer->id}}">
              {{$project_service->project->sub_customer->name}}</a></span>
              @endif
            </td>
          </tr>
          <tr>
            <td>
              <span class="font-weight-bold col">Project: </span>
            </td>
            <td>
              <a href="/projects/{{$project_service->project->id}}"><span>{{$project_service->project->name}}</span></a>
              <a href="#" class="text-info" id="edit-project" data-toggle="modal" data-target="#editProjectModal">(change)</a>
            </td>
          </tr>
          <tr>
            <td>
              <span class="font-weight-bold col">Status: </span>
            </td>
            <td>
              <span>{{$project_service->status}}</span>
            </td>
          </tr>
          <tr>
            <td>
              <span class="font-weight-bold col">Invoice Number: </span>
            </td>
            <td>
              <span>{{$project_service->invoice_number}}</span>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- Edit Project Modal -->
<div class="modal fade" id="editProjectModal"  tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Change Project</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="mt-1" id="editProjectForm" method="POST" action="/project_service/{{$project_service->id}}/ajax_edit_ps_project">
          @csrf
          <div class="form-group">
            <label for="projects" class="sr-only">Projects</label>
            <select class="form-control" id="project" name="project">
              <option value="">Select New Project</option>
              @foreach($projects as $project)
                <option value="{{$project->id}}">{{$project->name}}</option>
              @endforeach
            </select>
            <input name="project_service_id" type="hidden" value="{{$project_service->id}}">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button id="changeProjectSubmit"  type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="duplicate-modal" tabindex="-1" role="dialog" aria-labelledby="duplicateModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="duplicateModalLabel">Duplicate Service - {{$project_service->id}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="mt-1" id="duplicate-project-service-form" method="POST" action="{{route('duplicate-project-service')}}">
          @csrf
          <input type="hidden" value="{{$project_service->id}}" name="duplicate_id">
          <div class="form-group">
            <label for="occurs-on" class="sr-only">Occurs On</label>
            <input id="occurs-on" name="occurs_on" type='text' class="form-control" placeholder="Occurs On" required />
          </div>
          <div class="form-group">
            <label for="occurs-at" class="sr-only">Occurs At</label>
            <input id="occurs-at" name="occurs_at" type='text' class="form-control" placeholder="Occurs At" required />
          </div>
          <div class="form-group">
            <label for="minutes-est" class="sr-only">Occurs At</label>
            <input id="minutes-est" name="minutes_est" type='text' class="form-control" placeholder="Estimated Hours" required />
          </div>
          <div class="form-group">
            <div class="alert alert-danger alert-block" id="alerts" style="display:none">
              <button type="button" class="close" data-dismiss="alert">×</button>	
            </div>
            <h2 class="h4">Duplicate Features</h2>
            <div class="row">
              <div class="input-group col-lg-6 mb-1">
                <div class="input-group-prepend w-100">
                  <div class="input-group-text w-100">
                    <label for="documents" class="mb-0 text-left w-100">
                      <input type="checkbox" id="documents" name="documents_dup" checked>
                      Documents
                    </label>
                  </div>
                </div>
              </div>
              <div class="input-group col-lg-6 mb-1">
                <div class="input-group-prepend w-100">
                  <div class="input-group-text w-100">
                    <label for="people" class="mb-0 text-left w-100">
                      <input type="checkbox" id="people" name="people"  checked>
                      People
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="input-group col-lg-6 mb-1">
                <div class="input-group-prepend w-100">
                  <div class="input-group-text w-100">
                    <label for="addresses" class="mb-0 text-left w-100">
                      <input type="checkbox" id="addresses" name="addresses" checked>
                      Addresses
                    </label>
                  </div>
                </div>
              </div>
              <div class="input-group col-lg-6 mb-1">
                <div class="input-group-prepend w-100">
                  <div class="input-group-text w-100">
                    <label for="equipment-dup" class="mb-0 text-left w-100">
                      <input type="checkbox" id="equipment-dup" name="equipment_dup" checked>
                      Equipment
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="input-group col-lg-6 mb-1">
                <div class="input-group-prepend w-100">
                  <div class="input-group-text w-100">
                    <label for="supplies" class="mb-0 text-left w-100">
                      <input type="checkbox" id="supplies" name="supplies" checked>
                      Supplies
                    </label>
                  </div>
                </div>
              </div>
              <div class="input-group col-lg-6 mb-1">
                <div class="input-group-prepend w-100">
                  <div class="input-group-text w-100">
                    <label for="notes" class="mb-0 text-left w-100">
                      <input type="checkbox" id="notes" name="notes" checked>
                      Notes
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="duplicate-submit-button">Duplicate</button>
      </div>
    </div>
  </div>
</div>

@push('scripts')
<script>

  //datepicker
  $(document).ready(function() {
    $('#occurs-on').datetimepicker({
      timepicker: false,
      format:'Y-m-d'
    });
    $('#occurs-at').datetimepicker({
      datepicker: false,
      format:'H:i'
    });
  })

  //Duplicate submit
  $("#duplicate-submit-button").on('click', function(e) {
    e.preventDefault()
    if( $("#occurs-on").val() == "" ) {
      let message ="Occurs on is required"
      $("#alerts").text(message)
      $("#alerts").slideDown()
      return false
    }
    if( $("#occurs-at").val() == "" ) {
      let message ="Occurs at is required"
      $("#alerts").text(message)
      $("#alerts").slideDown()
      return false
    }
    $("#duplicate-project-service-form").submit()
  })

  $('#changeProjectSubmit').on('click', function(e) {
    e.preventDefault();
    $("#editProjectForm").submit()
  })
</script>
@endpush
<div class="d-flex flex-column">
  <div class="my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
    <div class="align-middle inline-block min-w-full overflow-hidden sm:rounded-lg">
      <div class="border text-left p-4">
        <div class="d-flex align-items-center">
          <h2 class="font-bold text-xl">People</h2>
          <button 
            data-toggle="modal" 
            data-target="#addPersonModal" 
            class=" btn btn-info btn-sm ml-4 e-form">Add New</button>
        </div>
        <form id="add-person-form" class="form-inline add-person-form w-100 e-form" method="POST" action="{{ route('add_person_to_project_service') }}">
          @csrf 
          <input type="hidden" value="{{ $project_service->id}}" name="id">
          <div class="form-group mr-2">
          <div id="customers-div">
            <input class="typeahead form-control person1" type="text" name="employee" id="employee" />
            <input type="hidden" id="person" name="person"/>
          </div>
          </div>
          <div class="form-group mr-2">
            <select name="service_role" id="service_role" class="form-control">
              <option value="">Select Role</option>
              @foreach($roles as $role)
              <option value="{{$role->id}}">{{$role->name}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group mr-2">
            <label for="in-at" class="mr-2 sr-only">In At</label>
            <input type="text" id="in-at" class="form-control" name="scheduled_in_at"  placeholder="In At"/>
          </div>
          <div class="form-group mr-2">
            <label for="out-at" class="mr-2 sr-only">Out At</label>
            <input type="text" id="out-at" class="form-control" name="scheduled_out_at" placeholder="Out At"/>
          </div>
          <div class="form-group mr-2">
            <label for="quantity" class="mr-2 sr-only">Number to add</label>
            <input type="text" id="quantity" class="form-control" name="quantity" style="max-width:50px" value="1" title="Number to Add"/>
          </div>
          
          <div class="bg-dark rounded-circle ml-1 mr-3 text-light d-flex align-items-center justify-content-center" data-toggle="tooltip" data-placement="top" style="height: 15px; width: 15px;" title="Enter the number of roles you would like to add, must be at least one. Best used without adding the person's name.">
            <span class="p-1" style="font-size: 0.75rem">?</span>
          </div>
          <button type="submit" id="add-person" class="btn btn-primary">Add</button>
        </form>
        <div class="people-toggle">
          <table class="table table-sm table-striped table-hover mt-4" id="people-table">
            <thead>
              <tr>
                <th>
                  Name
                </th>
                <th>
                  Role
                </th>
                <th>
                  In at
                </th>
                <th>
                  Out at
                </th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @if($project_service->people->count() > 0)
              @foreach($project_service->people as $person)
              <tr id="row-{{$person->id}}">
                <td>
                  @if($person->company_person)
                  {{ $person->company_person->person->first_name }} {{ $person->company_person->person->last_name }}
                  @endif
                </td>
                <td>
                  @if(isset($person->service_role))
                  {{ $person->service_role->name }} ({{ $person->service_role->rank }})
                  @endif
                </td>
                <td>
                  {{ $person->scheduled_in_at }}
                </td>
                <td>
                  {{ $person->scheduled_out_at }}
                </td>
                <td style="text-align:right">
                  <button 
                    data-toggle="modal" 
                    data-target="#editModal" 
                    class="btn btn-sm btn-info mr-3 edit-button e-form"
                    data-id="{{$person->id}}"
                    @if($person->company_person)
                    data-person="{{$person->company_person->id}}"
                    @endif
                    @if($person->service_role)
                    data-service_role="{{$person->service_role->id}}"
                    @endif
                    data-in_at="{{ $person->scheduled_in_at }}"
                    data-out_at="{{ $person->scheduled_out_at }}"
                    >
                    <i class="fa fa-edit"><span class="sr-only">Edit</span></i>
                  </button>
                  <button data-person="{{$person->id}}" class="btn btn-sm btn-danger delete-person e-form"><i class="fa fa-trash"><span class="sr-only">Delete</span></i></button>
                </td>
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Edit Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="Edit person" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">Edit Person</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="edit-person-form" class="add-person-form w-100 e-form" method="POST" action="{{ route('edit_project_service_person') }}">
          @csrf 
          <input type="hidden" id="edit-id" name="edit-id">
          {{-- <div class="form-group mb-2">
            <select name="person_edit" id="person-edit" class="form-control">
              <option value="">Select Person</option>
              @foreach($employees as $employee)
              <option value="{{$employee->id}}">
                {{$employee->person->first_name}}
                {{$employee->person->last_name}}
              </option>
              @endforeach
            </select>
          </div> --}}
          
          <div class="form-group mr-2">
          <div id="customers-div">
            <input class="typeahead form-control person2" type="text" name="employee2" id="employee2" />
            <input type="hidden" id="person2" name="person2"/>
          </div>
          </div>
          <div class="form-group mb-2">
            <select name="service_role-edit" id="service_role_edit" class="form-control">
              <option value="">Select Role</option>
              @foreach($roles as $role)
              <option value="{{$role->id}}">{{$role->name}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group mb-2">
            <label for="in-at">In At: </label>
            <input id="in-at-edit" name="scheduled_in_at_edit" type="text" class="form-control"/>
          </div>
          <div class="form-group mb-2">
            <label for="out-at">Out At: </label>
            <input id="out-at-edit" name="scheduled_out_at" type="text" class="form-control"/>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button id="edit-submit-person" type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- Add Person Modal -->
<div class="modal fade" id="addPersonModal" tabindex="-1" role="dialog" aria-labelledby="Add person" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">Add New Person</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="add-new-person-form" class="w-100 e-form mt-5" method="POST" action="{{ route('store-person-ajax') }}">
          @csrf 
          <input type="hidden" id="company_id" name="company_id" value="{{$project_service->branch->id}}">
          <input type="hidden" id="emerg_contact" name="emerg_contact" value="">
          <div class="form-group">
          <label for="title">Title</label>
          <select id="title" name="title" type='text' class="form-control">
            <option value="">Select Title</option>
            <option value="Mr.">Mr.</option>
            <option value="Mrs.">Mrs.</option>
            <option value="Ms.">Ms.</option>
            <option value="Miss">Miss</option>
          </select>
        </div>
        <div class="form-group">
          <label for="first_name">First Name <span>*</span></label>
          <input type="text" name="first_name" id="first_name" class="form-control" required />
        </div>
        <div class="form-group">
          <label for="last_name">Last Name<span>*</span></label>
          <input type="text" name="last_name" id="last_name" class="form-control" required />
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" name="email" id="email" class="form-control" />
        </div>
        <div class="form-group">
          <label for="phone">Phone</label>
          <input type="text" name="phone" id="phone" class="form-control" />
        </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button id="add-person-submit" type="submit" class="btn btn-primary">Add</button>
      </div>
    </div>
  </div>
</div>

@push('scripts')
<script>

$(document).on('click', '#add-person-submit', function(e) {
  e.preventDefault();
      $.ajax({
        type:'POST',
        url:"{{route('store-person-ajax')}}",
        data: $('#add-new-person-form').serialize(),
        success: function(data) {
          $('#employee').val(data.name)
          $('#person').val(data.id)
          $('#add-new-person-form')[0].reset()
          $('#addPersonModal').modal('hide')
        }
    })
})

  $(document).on('click', '.edit-button', function(e) {
    $('#edit-id').val($(this).data('id'))
    $('#person-edit option[value="' + $(this).data('person') + '"]').prop('selected', true)
    $('#service_role_edit option[value="' + $(this).data('service_role') + '"]').prop('selected', true)
    $('#in-at-edit').val($(this).data('in_at'))
    $('#out-at-edit').val($(this).data('out_at'))

  } )

  $('#edit-submit-person').on('click', function(e) {
    let id = $('#edit-id').val()
    let person = $('#person2').val()
    let role = $('#service_role_edit option:selected').val()
    let inAt = $('#in-at-edit').val()
    let outAt = $('#out-at-edit').val()
    $.ajax({
        type:'POST',
        url:"{{route('edit_project_service_person')}}",
        data:{
          company_person_id: person, 
          id: id,
          service_role_id: role, 
          project_service_id: {{$project_service->id}},
          scheduled_in_at: inAt,
          scheduled_out_at: outAt,
          _token: "{{csrf_token()}}"
        },
        success: function(data) {
          $('#row-' + data.service_person.id).html(`
          <td>${data.person.person.first_name} ${data.person.person.last_name}</td>
          <td>${data.role.name}</td>
          <td>${data.service_person.scheduled_in_at}</td>
          <td>${data.service_person.scheduled_out_at}</td>
          <td style='text-align:right'>
            <button 
                data-toggle="modal" 
                data-target="#editModal" 
                class="btn btn-sm btn-info mr-3 edit-button"
                data-id="${data.service_person.id}"
                data-person="${data.service_person.company_person_id}"
                data-service_role="${data.role.id}"
                data-in_at="${data.service_person.scheduled_in_at}"
                data-out_at="${ data.service_person.scheduled_out_at }"
                >
                <i class="fa fa-edit"><span class="sr-only">Edit</span></i>
              </button>
          <button data-person="${data.service_person.id}" class='btn btn-sm btn-danger delete-person'><i class='fa fa-trash'><span class='sr-only'>Delete</span></i></button></td>
        `)
        $('#editModal').modal('hide')
        }
    })
  })


  $('#add-person').on('click', function(e) {
    e.preventDefault();
    let person = $('#person').val()
    let service_role = $('#service_role').val()
    let inAt = $('#in-at').val()
    let outAt = $('#out-at').val()
    if(service_role == '') {
      alert('You must select a service role')
      return
    }
    if($('#quantity').val() > 1 && person) {
      alert('Please do not select a person when creating multiple roles at once!')
      $('#employee').val('')
      $('#person').val('')
      return
    }
    $.ajax({
        type:'POST',
        url:"{{route('add_person_to_project_service')}}",
        data:{
          company_person_id: person, 
          quantity: $('#quantity').val(),
          service_role_id: service_role, 
          project_service_id: {{$project_service->id}},
          scheduled_in_at: inAt,
          scheduled_out_at: outAt,
          _token: "{{csrf_token()}}"
        },
        success:function(data){
          let role;
          let name;
          let id;
          if(data.person) {
            let first = data.person.person.first_name ? data.person.person.first_name : ''
            let last = data.person.person.last_name ? data.person.person.last_name : ''
            id = data.service.id
            name = first + ' ' + last
          } else {
            name = ' '
            id = ' '
          }
          if(data.role) {
            role = `${data.role.name} (${data.role.rank})`
          } else {
            role = ''
          }
          if(data.quantity > 1){
            for($i = 1; $i <= data.quantity; $i++){
              $('#people-table tbody').append(`<tr id="row-${data.service.id}">
              <td>${name}</td>
              <td>${role}</td>
              <td>${data.service.scheduled_in_at}</td>
              <td>${data.service.scheduled_out_at}</td>
              <td style='text-align:right'>
                <button 
                    data-toggle="modal" 
                    data-target="#editModal" 
                    class="btn btn-sm btn-info mr-3 edit-button"
                    data-id="${data.service.id}"
                    data-person="${data.service.company_person_id}"
                    data-service_role="${data.role.id}"
                    data-in_at="${data.service.scheduled_in_at}"
                    data-out_at="${ data.service.scheduled_out_at }"
                    >
                    <i class="fa fa-edit"><span class="sr-only">Edit</span></i>
                  </button>
              <button data-person="${id}" class='btn btn-sm btn-danger delete-person'><i class='fa fa-trash'><span class='sr-only'>Delete</span></i></button></td></tr>`)
            }
          } else {          
            $('#people-table tbody').append(`<tr id="row-${data.service.id}">
            <td>${name}</td>
            <td>${role}</td>
            <td>${data.service.scheduled_in_at}</td>
            <td>${data.service.scheduled_out_at}</td>
            <td style='text-align:right'>
              <button 
                  data-toggle="modal" 
                  data-target="#editModal" 
                  class="btn btn-sm btn-info mr-3 edit-button"
                  data-id="${data.service.id}"
                  data-person="${data.service.company_person_id}"
                  data-service_role="${data.role.id}"
                  data-in_at="${data.service.scheduled_in_at}"
                  data-out_at="${ data.service.scheduled_out_at }"
                  >
                  <i class="fa fa-edit"><span class="sr-only">Edit</span></i>
                </button>
            <button data-person="${id}" class='btn btn-sm btn-danger delete-person'><i class='fa fa-trash'><span class='sr-only'>Delete</span></i></button></td></tr>`)

          }

          $('#add-person-form')[0].reset()
          $('#person').val(null)
        }
      });
  })

  $(document).on('click', '.delete-person', function(e) {
    e.preventDefault()
    let person = $(this).data('person')
    let row = $(this).closest('tr')
    let res = confirm('Are you sure you want to remove this person?')
    if(res === true) {
      $.ajax({
        type:'DELETE',
        url:"{{route('delete_person_from_project_service')}}",
        data:{id:person,
          _token: "{{csrf_token()}}"
        },
        success:function(data){
          if(data.result === 1) {
            row.fadeOut()
          }
        }
      });
    }
  })

  $(document).ready(function() {
    $('#in-at').datetimepicker({
      datepicker: false,
      format:'H:i'
    });
    $('#out-at').datetimepicker({
      datepicker: false,
      format:'H:i'
    });
    $('#in-at-edit').datetimepicker({
      datepicker: false,
      format:'H:i'
    });
    $('#out-at-edit').datetimepicker({
      datepicker: false,
      format:'H:i'
    });
  })

   let path = "{{ route('employee_autocomplete') }}";
    $('input.person1').typeahead({
        items: 50,
        source:  function (query, process) {
            return $.get(path, { query: query }, function (data) {
                return process(data);
            });
        },
        highlighter: function (item, data) {
            var parts = item.split('#'),
                html = '<div class="row">';
                html += '<div class="col-md-2">';
                html += '</div>';
                html += '<div class="col-md-10 pl-0">';
                html += '<span>'+data.name+'</span>';
                html += '</div>';
                html += '</div>';

            return html;
        },
        afterSelect:function(data,value,text){
            $('#person').val(data.id)
        },
    })
    
    $('input.person2').typeahead({
        items: 50,
        source:  function (query, process) {
            return $.get(path, { query: query }, function (data) {
                return process(data);
            });
        },
        highlighter: function (item, data) {
            var parts = item.split('#'),
                html = '<div class="row">';
                html += '<div class="col-md-2">';
                html += '</div>';
                html += '<div class="col-md-10 pl-0">';
                html += '<span>'+data.name+'</span>';
                html += '</div>';
                html += '</div>';

            return html;
        },
        afterSelect:function(data,value,text){
            $('#person2').val(data.id)
        },
    })

</script>
@endpush

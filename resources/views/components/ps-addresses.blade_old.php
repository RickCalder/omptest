<div class="d-flex flex-column">
  <div class="my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
    <div class="align-middle inline-block min-w-full overflow-hidden sm:rounded-lg">
      <div class="border text-left p-4">
        <div class="d-flex justify-content-between">
          <h2 class="font-bold text-xl">Addresses</h2>
        </div>
          <form id="add-address-form" class="form-inline add-address-form w-100 e-form" method="POST" action="{{ route('add_address_to_project_service') }}">
            @csrf 
            <input type="hidden" value="{{ $project_service->id}}" name="id">
            <div class="form-group mr-2">
              <select class="form-control" name="type" id="type">
                <option value="">Select Type</option>
                <option value="source">Source</option>
                <option value="destination">Destination</option>
              </select>
            </div>
            <div class="form-group mr-2">
              <select class="form-control" name="address" id="address" style="max-width: 375px">
                <option value="">Select Address</option>
                @foreach($addresses as $address)
                <option value="{{ $address->id }}">{{ $address->newAddressToString() }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group mr-2">
              <select class="form-control" name="contact" id="contact">
                <option value="">Select Contact</option>
                @foreach($contacts as $contact)
                <option value="{{ $contact->id }}">{{ $contact->person->first_name }} {{ $contact->person->last_name }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group mr-2">
              <input type="text" id="note" class="form-control" name="note" placeholder="Note">
            </div>
            <button type="submit" id="add-address" class="btn btn-primary">Add</button>&nbsp;&nbsp;&nbsp;
            <div style="display:flex; justify-content:space-around; margin-top: 1rem;width: 100%">
            <button 
                    data-toggle="modal" 
                    data-target="#addAddressModal" 
                    type="button" id="new-address" class="btn btn-info">New Address</button>
            <button 
                    data-toggle="modal" 
                    data-target="#addContactModal" 
                    type="button" id="new-contact" class="btn btn-info">New Contact</button>
                    </div>
          </form>
        <div class="address-toggle">
          <table class="table table-sm table-striped table-hover mt-4" id="address-table">
            <thead>
              <tr>
                <th>Type</th>
                <th>Address</th>
                <th>Contact</th>
                <th>Note</th>
              </tr>
            </thead>
            <tbody>
              @foreach($project_service->addresses as $address)
              <tr id="row-{{$address->id}}">
                <td>
                  {{ $address->type }}
                </td>
                <td>
                  @if($address->company_address)
                  {{ $address->company_address->addressToString() }}
                  @endif
                </td>
                <td>
                  @if(isset($address->contact))
                  {{ $address->contact->person->first_name }} {{ $address->contact->person->last_name }}
                  @endif
                </td>
                <td>
                  {{ $address->note }}
                </td>
                <td style="text-align:right">
                  <button 
                    data-toggle="modal" 
                    data-target="#editAddressModal" 
                    class="btn btn-sm btn-info mr-3 edit-button e-form"
                    data-type="{{$address->type}}"
                    data-address_id="{{$address->company_address->id ?? ""}}"
                    data-contact="{{$address->company_person_id}}"
                    data-note="{{$address->note}}"
                    data-id="{{$address->id}}"
                  >
                    <i class="fa fa-edit"><span class="sr-only">Edit</span></i>
                  </button>
                  <button data-address="{{$address->id}}" class="btn btn-sm btn-danger delete-address e-form"><i class="fa fa-trash"><span class="sr-only">Delete</span></i></button>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editAddressModal" tabindex="-1" role="dialog" aria-labelledby="Edit address" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">Edit Address</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="edit-address-form" class="add-address-form w-100 e-form" method="POST" action="{{ route('edit_address_project_service') }}">
          @csrf 
          <input type="hidden" id="id-edit" name="id">
          <div class="form-group mr-2">
            <select class="form-control" name="type_edit" id="type-edit">
              <option value="">Select Type</option>
              <option value="source">Source</option>
              <option value="destination">Destination</option>
            </select>
          </div>
          <div class="form-group mr-2">
            <select class="form-control" name="address_edit" id="address-edit">
              <option value="">Select Address</option>
              @foreach($addresses as $address)
              <option value="{{ $address->id }}">{{ $address->addressToString() }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group mr-2">
            <select class="form-control" name="contact_edit" id="contact-edit">
              <option value="">Select Contact</option>
              @foreach($contacts as $contact)
              <option value="{{ $contact->id }}">{{ $contact->person->first_name }} {{ $contact->person->last_name }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group mr-2">
            <input type="text" id="note-edit" class="form-control" name="note_edit" placeholder="Note">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button id="edit-address-submit" type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- Add Address Modal -->
<div class="modal fade" id="addAddressModal" tabindex="-1" role="dialog" aria-labelledby="Add person" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">Add New Address</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="add-new-address-form" class="w-100 e-form" method="POST" action="{{ route('store-person-ajax') }}">
          @csrf 
          <input type="hidden" id="company_id" name="company_id" value="{{$project_service->project->customer_id}}">
        <div class="form-group">
          <label for="building_name">Building Name</label>
          <input type="text" name="building_name" id="building_name" class="form-control"  />
        </div>
        <div class="form-group">
          <label for="unit_designator">Unit Designator</label>
          <select id="unit_designator" name="unit_designator" class="custom-select">
            <option value="">None</option>
            <option value="Apartment">Apartment</option>
            <option value="Suite">Suite</option>
            <option value="Unit">Unit</option>
          </select>
        </div>
        <div class="form-group">
          <label for="unit">Unit Number</label>
          <input type="text" name="unit" id="unit" class="form-control" />
        </div>
        <div class="form-group">
          <label for="floor">Floor</label>
          <input type="text" name="floor" id="floor" class="form-control" />
        </div>
        <div class="form-group">
          <label for="civic_number">Civic Number</label>
          <input type="text" name="civic_number" id="civic_number" class="form-control" />
        </div>
        <div class="form-group">
          <label for="civic_number_suffix">Civic Number Suffix</label>
          <select id="civic_number_suffix" name="civic_number_suffix" class="custom-select">
            <option value="">None</option>
            <option value="¼">¼</option>
            <option value="½">½</option>
            <option value="¾">¾</option>
            <option value="A">A</option>
            <option value="B">B</option>
            <option value="C">C</option>
            <option value="D">D</option>
            <option value="E">E</option>
            <option value="F">F</option>
            <option value="G">G</option>
            <option value="H">H</option>
            <option value="I">I</option>
            <option value="J">J</option>
            <option value="K">K</option>
            <option value="L">L</option>
            <option value="M">M</option>
            <option value="N">N</option>
            <option value="O">O</option>
            <option value="P">P</option>
            <option value="Q">Q</option>
            <option value="R">R</option>
            <option value="S">D</option>
            <option value="T">T</option>
            <option value="U">U</option>
            <option value="V">V</option>
            <option value="W">W</option>
            <option value="X">X</option>
            <option value="Y">Y</option>
            <option value="Z">Z</option>
          </select>
        </div>
        <div class="form-group">
          <label for="street_name">Street Name</label>
          <input type="text" name="street_name" id="street_name" class="form-control" />
        </div>
        <div class="form-group">
          <label for="address_street_type_id">Street Type</label>
          <select class="custom-select" name="address_street_type_id" id="address_street_type_id">
            <option value="">Select Street Type</option>
            @foreach($addressStreetTypes as $streetType)
              <option value="{{$streetType->id}}">{{$streetType->name}}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="street_direction">Street Direction</label>
          <select id="street_direction" name="street_direction" class="custom-select">
            <option value="">None</option>
            <option value="E">E</option>
            <option value="N">N</option>
            <option value="NE">NE</option>
            <option value="NW">NW</option>
            <option value="S">S</option>
            <option value="SE">SE</option>
            <option value="SW">SW</option>
            <option value="W">W</option>
          </select>
        </div>
        <div class="form-group">
          <label for="city">City</label>
          <select class="custom-select" name="city" id="city">
            <option value="">Select city</option>
            @foreach($cities as $city)
              <option value="{{$city->id}}">{{$city->name}}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="postal_code">Postal Code</label>
          <input type="text" name="postal_code" id="postal_code" class="form-control" />
        </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button id="add-new-address" type="submit" class="btn btn-primary">Add</button>
      </div>
    </div>
  </div>
</div>


<!-- Add Contact Modal -->
<div class="modal fade" id="addContactModal" tabindex="-1" role="dialog" aria-labelledby="Add contact" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">Add New Contact</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="add-new-contact-form" class="w-100 e-form" method="POST">
          @csrf 
          <input type="hidden" id="company_contact_id" name="company_contact_id" value="{{$project_service->project->customer_id}}">
          <div class="form-group">
            <label for="contact_name">First Name*</label>
            <input type="text" name="first_name" id="first_name" class="form-control" required  />
          </div>
          <div class="form-group">
            <label for="contact_name">Last Name*</label>
            <input type="text" name="last_name" id="last_name" class="form-control" required  />
          </div>
          <div class="form-group">
            <label for="contact_name">Email</label>
            <input type="email" name="email" id="email" class="form-control"  />
          </div>
          <div class="form-group">
            <label for="contact_name">Phone Number*</label>
            <input type="text" name="phone_number" id="phone_number" class="form-control" required  />
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button id="add-new-contact" type="submit" class="btn btn-primary">Add</button>
      </div>
    </div>
  </div>
</div>

@push('scripts')
<script>

  $('#add-new-contact').on('click', (e) => {
    e.preventDefault()
    $.ajax({
      type: 'POST',
      url: "{{route('ajax_add_new_contact')}}",
      data: $("#add-new-contact-form").serialize(),
      success: function(data) {
        console.log(data)
        
        let optionHTML = `
        <option value="${data.contact.contact_id}" selected> 
            ${data.contact.first_name} ${data.contact.last_name} 
        </option>`;
        console.log(optionHTML)
        $('#contact').append(optionHTML)
        $('#addContactModal').modal('hide')
      }
    })
  })
    $('#add-new-address').on('click', function(e) {
      e.preventDefault();
      $.ajax({
        type: 'POST',
        url:"{{route('ajax_add_new_address')}}",
        data:$("#add-new-address-form").serialize()
      ,
      success:function(data){
        console.log(data.address)
        let optionHTML = `
        <option value="${data.address.address_id}" selected> 
            ${data.address.address} 
        </option>`;
        console.log(optionHTML)
        $('#address').append(optionHTML)
        $('#addAddressModal').modal('hide')
      }})
    })

  $(document).on('click', '.edit-button', function(e) {
    $('#id-edit').val($(this).data('id'))
    $('#address-edit option[value="' + $(this).data('address_id') + '"]').prop('selected', true)
    $('#type-edit option[value="' + $(this).data('type') + '"]').prop('selected', true)
    $('#contact-edit option[value="' + $(this).data('contact') + '"]').prop('selected', true)
    $('#note-edit').val($(this).data('note'))
  })

  $(document).on('click', '#edit-address-submit', function(e) {


    e.preventDefault()
    $.ajax({
        type:'POST',
        url:"{{route('edit_address_project_service')}}",
        data:{
          type: $('#type-edit option:selected').val(), 
          company_address_id: $('#address-edit option:selected').val(), 
          note: $('#note-edit').val(),
          company_person_id: $('#contact-edit').val(),
          project_service_id: {{$project_service->id}},
          id: $('#id-edit').val(),
          _token: "{{csrf_token()}}"
        },
        success:function(data){
          console.log(data)
          let addressString = $('#address-edit').find(':selected').text()
          $('#row-' + data.address.id).html(`
            <td>${data.address.type}</td>
            <td>${addressString}</td>
            <td>${data.contact.first_name} ${data.contact.last_name}</td>
            <td>${data.address.note}</td>
            <td style='text-align:right'>
            <button class='btn btn-sm btn-info mr-3'
              data-toggle="modal" 
              data-target="#editAddressModal" 
              class="btn btn-sm btn-info mr-3 edit-button"
              data-type="${data.address.type}"
              data-address_id="${data.address.id}"
              data-contact="${data.address.company_person_id}"
              data-note="${data.address.note}"
              data-id="${data.address.id}"
            >
            <i class='fa fa-edit'><span class='sr-only'>Edit</span></i></button>
            <button data-address="${data.address.id}" class='btn btn-sm btn-danger delete-address'><i class='fa fa-trash'><span class='sr-only'>Delete</span></i></button>
            </td>
          `)
          $('#editAddressModal').modal('hide')

        }

    })
  })

    $('#add-address').on('click', function(e) {
    e.preventDefault();
    let type = $('#type').val()
    let address = $('#address').val()
    let note = $('#note').val()
    let contact = $('#contact').val()
    let addressString = $('#address').find(':selected').text()
    console.log(address)
    if(type == '' || address == '') {
      alert('You must select a type and address to continue')
      return
    }
    $.ajax({
        type:'POST',
        url:"{{route('add_address_to_project_service')}}",
        data:{type:type, 
          company_address_id: address, 
          note: note,
          company_person_id: contact,
          project_service_id: {{$project_service->id}},
          _token: "{{csrf_token()}}"
        },
        success:function(data){
          console.log(data.address)
          let note;
          let name;
          let id;
          let type;
          if(data.contact) {
            let first = data.contact.first_name ? data.contact.first_name : ''
            let last = data.contact.last_name ? data.contact.last_name : ''
            id = data.address.id
            name = first + ' ' + last
          } else {
            name = ' '
            id = ' '
          }
          if(typeof(data.address.note) == 'undefined' || data.address.note === null) {
            note=''
          } else {
            note = data.address.note
          }
          
          $('#address-table tbody').append(`<tr id="row-${data.address.id}"><td>${data.address.type}</td><td>${addressString}</td><td>${name}</td><td>${note}</td><td style='text-align:right'>
          <button
            data-toggle="modal" 
              data-target="#editAddressModal" 
              class="btn btn-sm btn-info mr-3 edit-button"
              data-type="${data.address.type}"
              data-address_id="${data.address.id}"
              data-contact="${data.address.company_person_id}"
              data-note="${data.address.note}"
              data-id="${data.address.id}"
          ><i class='fa fa-edit'><span class='sr-only'>Edit</span></i></button>
          <button data-address="${id}" class='btn btn-sm btn-danger delete-address'><i class='fa fa-trash'><span class='sr-only'>Delete</span></i></button></td></tr>`)
          $('#add-address-form')[0].reset()
        }
      });
  })
  $(document).on('click', '.delete-address', function(e) {
    e.preventDefault()
    let id = $(this).data('address')
    let row = $(this).closest('tr')
    let res = confirm('Are you sure you want to remove this address?')
    if(res === true) {
      $.ajax({
        type:'DELETE',
        url:"{{route('delete_address_from_project_service')}}",
        data:{id:id,
          _token: "{{csrf_token()}}"
        },
        success:function(data){
          if(data.result === 1) {
            row.fadeOut()
          }
        }
      });
    }
  })
</script>
@endpush
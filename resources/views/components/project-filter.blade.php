<div>
  <div class="mb-4">
    <div class="border rounded text-left p-4 align-items-center" id="filter-panel">
      <form method="GET" action="{{route('projects')}}" class="inline-form">
        @csrf
        <div class="d-flex row">
          <div class="col-md-4">
          <div class="form-group my-1">
            <label for="branch" class="sr-only">Branch</label>
            <select id="branch" name="branch" class="custom-select">
              <option value="">All Branches</option>
              @foreach($branches as $branch)
              <option @if(request('branch')==$branch->id)selected @endif
                value="{{ $branch->id }}">{{$branch->name}}</option>
              @endforeach
            </select>
            <label for="customer" class="sr-only">Customer</label>
            <input class="typeahead form-control mt-1" placeholder="Customer" type="text" name="customer" id="customer" />
            <input type="hidden" id="customer-id" name="customer_id"/>
            <label for="sub_customer" class="sr-only">Sub Customer</label>
            <select id="sub_customer" name="sub_customer" class="custom-select mt-1" disabled>
              <option value="">Sub Customers</option>
              @foreach($sub_customers as $sub_customer)
              <option @if(request('sub_customer')==$sub_customer->id)selected @endif
                value="{{ $sub_customer->id }}">{{$sub_customer->name}}</option>
              @endforeach
            </select>
          </div>
        </div>
        
        <div class="col-md-2">
          <div class="form-group my-1 d-flex align-items-center">

            <button type="submit" class="btn btn-primary ml-3">
              Filter
            </button>

            <a href="{{route('projects')}}" class="ml-3 regular-link">
              Reset
            </a>
          </div>
          <div>
            <a href="{{route('create-project')}}" class="btn btn-md btn-info ml-3 mt-4">Create Project</a>
          </div>
        </div>
        </div>
      </form>
    </div>
    <a href="" class="filter-toggle text-orange-500 h2" title="Hide/Show Filters"><i class="fa fa-chevron-up"
        aria-hidden="true"></i></a>
  </div>
</div>

@push('scripts')
<script>  
let path = "{{ route('customer_autocomplete') }}";
  $('input.typeahead').typeahead({
        items: 50,
      source:  function (query, process) {
          return $.get(path, { query: query }, function (data) {
              return process(data);
          });
      },
      highlighter: function (item, data) {
          var parts = item.split('#'),
              html = '<div class="row">';
              html += '<div class="col-md-2">';
              html += '</div>';
              html += '<div class="col-md-10 pl-0">';
              html += '<span>'+data.name+'</span>';
              html += '</div>';
              html += '</div>';

          return html;
      },
      afterSelect:function(data,value,text){
          $('#customer-id').val(data.id)
          subcustomers()
      },
  })

  function subcustomers() {
    let customer = $('#customer-id').val()
    if(customer !== '') {
      $('#sub_customer').prop('disabled', true)
      $.ajax({
        type:'POST',
        url:"{{ route('get-sub-customers') }}",
        data:{customer: customer},
        success:function(data){
          $('#sub_customers option').remove()
          if(data.sub_customers.length > 0 ){
            $("#sub_customers").append('<option value="">Select Sub Customer</option>')
            $.each(data.sub_customers, function(){
                $("#sub_customer").append('<option value="'+ this.id +'">'+ this.name +'</option>')
            })
            $('#sub_customer').prop('disabled', false)
          }
        }
      });
    }
  }

  $('.filter-toggle').on('click', function(e) {
      e.preventDefault()
      $('#filter-panel').toggle('fast')
      if($('.filter-toggle i').hasClass('fa-chevron-up')) {
        $('.filter-toggle i').removeClass('fa-chevron-up').addClass('fa-chevron-down')
      } else {
        $('.filter-toggle i').removeClass('fa-chevron-down').addClass('fa-chevron-up')
      }
    })

  </script>
  @endpush
@section('add-head')
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection

<div class="my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
  <div class="align-middle inline-block min-w-full overflow-hidden sm:rounded-lg">
    <div class="border text-left p-4">
      <div>
        <h2 class="h3">Special Instructions</h2> 
        <p>Notes are limited to 500 characters, if you require more space please upload a document</p>
      </div>
      <div class="row e-form">
        <div class="col-md-12 mt-3 mb-4">
          <p class="sr-only" id="ps-id">{{$project_service->id}}</p>
            <div class="form-group">
              <label for="noteLabel">Note</label>
              <textarea class="form-control" id="notetextspecial" rows="3" maxlength="500"></textarea>
            </div>
          <div class="alert alert-danger mt-2" style="display:none" id="note-error"></div>
          <button id="add-note2" class="btn btn-primary my-2">Add Note</button>
        </div>
        <div class="text-right w-100">
          <a href="" id="notes-sort2">Showing Newest first</a>
        </div>
      </div>
      <table class="notes-toggle2-desc table table-sm">
        @foreach($project_service->notes as $note)
        @if($note->note_type===2)
        @php

        if(auth()->user()->role_id === 1 && $note->deleted === 1) {
        $show_deleted = 'display: table-row; background: rgba(255,0,0, .2)';
        } else if($note->deleted === 1) {
        $show_deleted = 'display: none;';
        } else {
        $show_deleted = 'display: table-row;';
        }
        @endphp
        <tr style="{{$show_deleted}}" class="@if($note->deleted===1)deleted @endif">
          <td>{{ $note->person->first_name ?? ''}} {{ $note->person->last_name ?? ''}} noted on {{$note->entered_on}}:<br>
            {{ strip_tags($note->content) }}</td>
          <td>@if($note->deleted !== 1)<button data-note="{{$note->id}}" class="btn btn-sm btn-danger delete-note e-form"><i class="fa fa-trash"><span class="sr-only">Delete</span></i></button>@endif</td>
        </tr>
        @endif
        @endforeach
      </table>
      <table class="notes-toggle2-asc table table-sm" style="display: none;">
        @foreach($project_service->notes->reverse() as $note)
        @if($note->note_type===2)
        @php

        if(auth()->user()->role_id === 1 && $note->deleted === 1) {
        $show_deleted = 'display: table-row; background: rgba(255,0,0, .2)';
        } else if($note->deleted === 1) {
        $show_deleted = 'display: none;';
        } else {
        $show_deleted = 'display: table-row;';
        }
        @endphp
        <tr style="{{$show_deleted}}" class="@if($note->deleted===1)deleted @endif">
          <td>{{ $note->person->first_name ?? ''}} {{ $note->person->last_name ?? ''}} noted on {{$note->entered_on}}:<br>
            {!! $note->content !!}</td>
          <td>@if($note->deleted !== 1)<button data-note="{{$note->id}}" class="btn btn-sm btn-danger delete-note e-form"><i class="fa fa-trash"><span class="sr-only">Delete</span></i></button>@endif</td>
        </tr>
        @endif
        @endforeach
      </table>
    </div>
  </div>
</div>

@push('scripts')
<script>
$(document).ready(function() {
  let notesDesc2 = true

  $('#add-note2').on('click', function(e) {
    e.preventDefault();

    let project_service = $('#ps-id').html()
    let note = document.getElementById('notetextspecial').value

    if (note != ''){
      $.ajax({
        type:'POST',
        url:"{{route('add_note_to_project_service')}}",
        data:{
          project_service_id: project_service, 
          note: note,
          note_type: 2,
          _token: "{{csrf_token()}}"
        },
        success:function(data){
          console.log(data.note)
          if(data.note.note_type == 2) {
            
            $('.notes-toggle2-desc').prepend(`<tr><td>${data.note.name} ${data.note.entered_on}: <br>${data.note.content}</td><td><button data-note="${data.note.id}" class="btn btn-sm btn-danger delete-note e-form"><i class="fa fa-trash"><span class="sr-only">Delete</span></i></button></td></tr>`)
            $('.notes-toggle2-asc').append(`<tr><td>${data.note.name} ${data.note.entered_on}: <br>${data.note.content}</td><td><button data-note="${data.note.id}" class="btn btn-sm btn-danger delete-note e-form"><i class="fa fa-trash"><span class="sr-only">Delete</span></i></button></td></tr>`)
          }

        }
      });
    } else {
      $('#note-error').slideDown()
      $('#note-error').html('Notes cannot be blank')
    }
  })

    $('#notes-sort2').on('click', function(e) {
    e.preventDefault();
    notesDesc2 = !notesDesc2

    if(!notesDesc2) {
      $('#notes-sort2').html('Showing Oldest first')
      $('.notes-toggle2-desc').hide()
      $('.notes-toggle2-asc').show()
    } else {
      $('#notes-sort2').html('Showing Newest first')
      $('.notes-toggle2-asc').hide()
      $('.notes-toggle2-desc').show()
    }
  })  
});
</script>
@endpush
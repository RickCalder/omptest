<div class="d-flex flex-column">
  <div class="my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
    <div class="align-middle inline-block min-w-full overflow-hidden sm:rounded-lg">
      <div class="border text-left p-4">
        <h3 class="h5 font-weight-normal">Find</h3>
        <input class="form-control" type="text" name="person" id="person" required />
        <div class="d-flex justify-content-end mt-2">
          <a href="/people/create" class="btn btn-sm btn-info">Add Person</a>
        </div>
      </div>
    </div>
  </div>
</div>
@push('scripts')
<script>
  let path = "{{ route('person_autocomplete') }}";
  $('#person').typeahead({
    items: 15,
      source:  function (query, process) {
          return $.get(path, { query: query }, function (data) {
              return process(data);
          });
      },
      highlighter: function (item, data) {
          var parts = item.split('#'),
              html = '<div class="row">';
              html += '<div class="col-md-2">';
              html += '</div>';
              html += '<div class="col-md-10 pl-0">';
              html += '<span>'+data.name+'</span>';
              html += '</div>';
              html += '</div>';

          return html;
      },
      afterSelect:function(data,value,text){
        console.log(data)
          window.location.href = "/people/" + data.id
      },
  })

</script>
@endpush 
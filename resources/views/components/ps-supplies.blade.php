
<div class="d-flex flex-column">
  <div class="my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
    <div class="align-middle inline-block min-w-full overflow-hidden sm:rounded-lg">
      <div class="border text-left p-4">        
        <div class="d-flex justify-content-between">
          <h2 class="font-bold text-xl">Supplies</h2> 
        </div>
        <form id="add-supply-form" class="form-inline add-supply-form w-100 e-form" method="POST" action="{{ route('add_supplies_to_project_service') }}">
          @csrf 
          <input type="hidden" value="{{ $project_service->id}}" name="id">
          <div class="form-group mr-2">
            <select class="form-control" name="supply_id" id="supply_id">
              <option value="">Select Supplies</option>
              @foreach($supplies as $item)
              <option value="{{ $item->id }}">{{ $item->name }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group mr-2">
            <input type="number" name="quanity" id="supply_quantity" class="form-control">
          </div>
          <button type="submit" id="add-supplies" class="btn btn-primary">Add</button>
        </form>
        <div class="supply-toggle">
          <table id="supply-table" class="table table-sm table-striped table-hover mt-4">
            <thead>
              <tr>
                <th>
                  Supply
                </th>
                <th>
                  Quantity
                </th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @if($project_service->service_supplies->count() > 0)
              @foreach($project_service->service_supplies as $supply)

              @if(isset($supply->supply->name))
              <tr id="row-{{$supply->id}}">
                <td>
                  {{ $supply->supply->name }}
                </td>
                <td>
                  {{ $supply->quantity }}
                </td>
                <td style="text-align:right">
                  <button 
                  data-toggle="modal" 
                  data-target="#editSupply" 
                  data-supply="{{$supply->id}}" 
                  data-quantity="{{$supply->quantity}}"
                  data-supplyid="{{$supply->supply_id}}"
                  class="btn btn-sm btn-info mr-3 e-form edit-supply">
                  <i class="fa fa-edit"><span class="sr-only">Edit</span></i></button>
                  <button data-supply="{{$supply->id}}" class="btn btn-sm btn-danger e-form delete-supply"><i class="fa fa-trash"><span class="sr-only">Delete</span></i></button>
                </td>
              </tr>
              @endif
              @endforeach
              @endif
            </tbody>
          </table>
        </div>
        <table>
      </table>
      </div>
    </div>
  </div>
</div>
<!-- Edit Supplies Modal -->
<div class="modal fade" id="editSupply" tabindex="-1" role="dialog" aria-labelledby="edit supplies" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">Edit Supplies</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form id="edit-supplies-form" class="form-inline edit-supplies-form w-100 e-form" method="POST" action="{{ route('edit_supplies_ps') }}">
          @csrf 
          <input type="hidden" value="{{ $project_service->id}}" name="id">
          <div class="form-group mt-2">
            <input type="hidden" value="" id="edit-supply-record-id">
            <select class="form-control" name="supplies-edit" id="supplies-edit">
              <option value="">Select Supplies</option>
              @foreach($supplies as $item)
              <option value="{{ $item->id }}">{{ $item->name }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group mt-2">
            <input type="number" name="quantity_requested-supplies-edit" id="quantity_requested-supplies-edit" class="form-control" placeholder="Quantity">
          </div>
        </form>
      

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button id="edit-supplies-submit" type="submit" class="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
</div>
@push('scripts')
<script>
    $('#add-supplies').on('click', function(e) {
    e.preventDefault();
    let quantity = $('#supply_quantity').val()
    let supply_id = $('#supply_id').val()
    if(quantity == '' || supply_id == '') {
      alert('You must select supplies and quantity to continue')
      return
    }
    $.ajax({
        type:'POST',
        url:"{{route('add_supplies_to_project_service')}}",
        data:{
          supply_id: supply_id, 
          quantity: quantity,
          project_service_id: {{$project_service->id}},
          _token: "{{csrf_token()}}"
        },
        success:function(data){
          console.log(data)
          let supply = $('#supply_id').find(':selected').text();
          let quantity = $('#supply_quantity').val();
          let id = data.supplies.id
          $('#supply-table tbody').append(`
              <tr id="row-${data.supplies.id}">
                <td>${supply}</td><td>${quantity}</td>
                <td style="text-align:right">
                  <button 
                  data-toggle="modal" 
                  data-target="#editSupply" 
                  data-supply="${data.supplies.id}" 
                  data-quantity="${data.supplies.quantity}"
                  data-supplyid="${data.supplies.supply_id}"
                  class="btn btn-sm btn-info mr-3 e-form edit-supply">
                  <i class="fa fa-edit"><span class="sr-only">Edit</span></i></button>
                  <button data-supply="${data.supplies.id}" class="btn e-form btn-sm btn-danger delete-supply"><i class="fa fa-trash"><span class="sr-only">Delete</span></i></button>
                </td>
              </tr>`)
          $('#edit-supply-form')[0].reset()
        }
      });
  })
  $(document).on('click', '.delete-supply', function(e) {
    e.preventDefault()
    let id = $(this).data('supply')
    let row = $(this).closest('tr')
    let res = confirm('Are you sure you want to remove these supplies?')
    if(res === true) {
      $.ajax({
        type:'DELETE',
        url:"{{route('delete_supplies_from_project_service')}}",
        data:{id:id,
          _token: "{{csrf_token()}}"
        },
        success:function(data){
          if(data.result === 1) {
            row.fadeOut()
          }
        }
      });
    }
  })

  $('body').on('click', '.edit-supply', function(e) {
    let quantity = $(this).data('quantity')
    $('#edit-supply-record-id').val($(this).data('supply'))
    let supply_id = $(this).data('supplyid')
    console.log(supply_id)
    console.log(quantity)
    console.log($(this).data('supply'))
    $('#supplies-edit option[value=' + supply_id + ']').attr('selected','selected');
    $('#quantity_requested-supplies-edit').val(quantity)
  })

  $('#edit-supplies-submit').on('click', function(e) {
    $.ajax({
      type: 'POST',
      url:"{{route('edit_supplies_ps')}}",
      data: {
        supply_id: $('#supplies-edit').val(),
        record_id: $('#edit-supply-record-id').val(),
        quantity: $('#quantity_requested-supplies-edit').val(),
          _token: "{{csrf_token()}}"
      },
      success: function(data) {
        console.log(data)
        $(`#row-${data.result.id}`).html(
          ` <td>
              ${data.supplies.name}
            </td>
            <td>
              ${data.result.quantity}
            </td>
            <td style="text-align:right">
              <button 
              data-toggle="modal" 
              data-target="#editSupply" 
              data-supply="${data.result.id}" 
              data-quantity="${data.result.quantity}"
              data-supplyid="${data.result.supply_id}"
              class="btn btn-sm btn-info mr-3 e-form edit-equip">
              <i class="fa fa-edit"><span class="sr-only">Edit</span></i></button>
              <button data-supply="${data.result.id}" class="btn e-form btn-sm btn-danger delete-supply"><i class="fa fa-trash"><span class="sr-only">Delete</span></i></button>
            </td>`
        )
        $('#editSupply').modal('hide')
      }
    })
  })
</script>
@endpush

<div class="d-flex flex-column">
  <div class="my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
    <div class="align-middle inline-block min-w-full overflow-hidden sm:rounded-lg">
      <div class="border text-left p-4">
        <div class="d-flex justify-content-between">
          <h2 class="font-bold text-xl">Equipment</h2> 
        </div>
        @php 
        $equip = $equipment;
        @endphp
        <form id="add-equipment-form" class="form-inline add-equipment-form w-100 e-form" method="POST" action="{{ route('add_equipment_to_project_service') }}">
          @csrf 
          <input type="hidden" value="{{ $project_service->id}}" name="id">
          <div class="form-group mr-2">
            <select class="form-control" name="equipment" id="equipment">
              <option value="">Select Equipment</option>
              @foreach($equipment as $item)
              <option value="{{ $item->id }}">{{ $item->name }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group mr-2">
            <input type="number" name="quantity_requested" id="quantity_requested" class="form-control" placeholder="Quantity">
          </div>
          <button type="submit" id="add-equipment" class="btn btn-primary">Add</button>
        </form>
        <div class="equip-toggle">
          <table id="equipment-table" class="table table-sm table-striped table-hover mt-4">
            <thead>
              <tr>
                <th>
                  Equipment
                </th>
                <th>
                  Quantity Requested
                </th>
                <th>
                  Quantity Actual
                </th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @if($project_service->service_equipment->count() > 0)
              @foreach($project_service->service_equipment as $equipment)
              @if(isset($equipment->equip))
              @if(isset($equipment->equip->name))
              <tr id="row-{{$equipment->id}}">
                <td>
                  {{ $equipment->equip->name }}
                </td>
                <td>
                  {{ $equipment->quantity_requested }}
                </td>
                <td>
                  {{ $equipment->quantity_actual }}
                </td>
                <td style="text-align:right">
                  <button 
                  data-toggle="modal" 
                  data-target="#editEquip" 
                  data-equipment="{{$equipment->id}}" 
                  data-quantity="{{$equipment->quantity_requested}}"
                  data-equipmentid="{{$equipment->equipment_id}}"
                  class="btn btn-sm btn-info mr-3 e-form edit-equip">
                  <i class="fa fa-edit"><span class="sr-only">Edit</span></i></button>
                  <button data-equipment="{{$equipment->id}}" class="btn e-form btn-sm btn-danger delete-equipment"><i class="fa fa-trash"><span class="sr-only">Delete</span></i></button>
                </td>
              </tr>
              @endif
              @endif
              @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Edit Equipment Modal -->
<div class="modal fade" id="editEquip" tabindex="-1" role="dialog" aria-labelledby="edit equipment" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">Edit Equipment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form id="edit-equipment-form" class="form-inline edit-equipment-form w-100 e-form" method="POST" action="{{ route('edit_equipment_ps') }}">
          @csrf 
          <input type="hidden" value="{{ $project_service->id}}" name="id">
          <div class="form-group mt-2">
            <input type="hidden" value="" id="edit-record-id">
            <select class="form-control" name="equipment-edit" id="equipment-edit">
              <option value="">Select Equipment</option>
              @foreach($equip as $item)
              <option value="{{ $item->id }}">{{ $item->name }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group mt-2">
            <input type="number" name="quantity_requested-edit" id="quantity_requested-edit" class="form-control" placeholder="Quantity">
          </div>
        </form>
      

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button id="edit-equipment-submit" type="submit" class="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
</div>

@push('scripts')
<script>

  $('body').on('click', '.edit-equip', function(e) {
    let quantity = $(this).data('quantity')
    $('#edit-record-id').val($(this).data('equipment'))
    let equip_id = $(this).data('equipmentid')
    console.log(equip_id)
    console.log(quantity)
    $('#equipment-edit option[value=' + equip_id + ']').attr('selected','selected');
    $('#quantity_requested-edit').val(quantity)
  })

  $('#edit-equipment-submit').on('click', function(e) {
    $.ajax({
      type: 'POST',
      url:"{{route('edit_equipment_ps')}}",
      data: {
        equipment_id: $('#equipment-edit').val(),
        record_id: $('#edit-record-id').val(),
        quantity: $('#quantity_requested-edit').val(),
          _token: "{{csrf_token()}}"
      },
      success: function(data) {
        console.log(data)
        $(`#row-${data.result.id}`).html(
          ` <td>
              ${data.equipment.name}
            </td>
            <td>
              ${data.result.quantity_requested}
            </td>
            <td>
              
            </td>
            <td style="text-align:right">
              <button 
              data-toggle="modal" 
              data-target="#editEquip" 
              data-equipment="${data.result.id}" 
              data-quantity="${data.result.quantity_requestd}"
              data-equipmentid="${data.result.equipment_id}"
              class="btn btn-sm btn-info mr-3 e-form edit-equip">
              <i class="fa fa-edit"><span class="sr-only">Edit</span></i></button>
              <button data-equipment="${data.result.id}" class="btn e-form btn-sm btn-danger delete-equipment"><i class="fa fa-trash"><span class="sr-only">Delete</span></i></button>
            </td>`
        )
        $('#editEquip').modal('hide')
      }
    })
  })
    $('body').on('click', '#add-equipment', function(e) {
    e.preventDefault();
    let quantity_requested = $('#quantity_requested').val()
    let equipment = $('#equipment').val()
    console.log(quantity_requested, equipment)
    if(quantity_requested == '' || equipment == '') {
      alert('You must select equipment and quantity to continue')
      return
    }
    $.ajax({
        type:'POST',
        url:"{{route('add_equipment_to_project_service')}}",
        data:{
          equipment_id: equipment, 
          quantity_requested: quantity_requested,
          project_service_id: {{$project_service->id}},
          _token: "{{csrf_token()}}"
        },
        success:function(data){
          console.log(data)
          let equipment = $('#equipment').find(':selected').text()
          let quantity = $('#quantity_requested').val()
          $('#equipment-table tbody').append(`<tr id="row-${data.equipment.id}"><td>${equipment}</td><td>${quantity}</td><td></td><td style='text-align:right'><button data-equipment="${data.equipment.id}" data-equipmentid="${data.equipment.equipment_id}" data-quantity="${data.equipment.quantity_requested}" data-toggle="modal" data-target="#editEquip" class='edit-equip e-form btn btn-sm btn-info mr-3'><i class='fa fa-edit'><span class='sr-only'>Edit</span></i></button><button data-equipment="${data.equipment.id}" class='btn btn-sm btn-danger delete-equipment'><i class='fa fa-trash'><span class='sr-only'>Delete</span></i></button></td></tr>`)
          $('#add-equipment-form')[0].reset()
        }
      });
  })
  $(document).on('click', '.delete-equipment', function(e) {
    e.preventDefault()
    let id = $(this).data('equipment')
    let row = $(this).closest('tr')
    let res = confirm('Are you sure you want to remove this equipment?')
    if(res === true) {
      $.ajax({
        type:'DELETE',
        url:"{{route('delete_equipment_from_project_service')}}",
        data:{id:id,
          _token: "{{csrf_token()}}"
        },
        success:function(data){
          if(data.result === 1) {
            row.fadeOut()
          }
        }
      });
    }
  })
</script>
@endpush


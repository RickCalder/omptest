<div class="d-flex flex-column">
  <div class="my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
    <div class="align-middle inline-block min-w-full overflow-hidden sm:rounded-lg">
      <div class="border text-left p-4">
        <div class="d-flex justify-content-between">
          <h2 class="font-bold text-xl">Addresses</h2>
        </div>
          <form id="add-address-form" class="form-inline add-address-form w-100 e-form" method="POST" action="{{ route('add_address_to_project_service') }}">
            @csrf 
            <input type="hidden" value="{{ $project_service->id}}" name="id">
            <div class="form-group mr-2">
              <select class="form-control" name="type" id="type">
                <option value="">Select Type</option>
                <option value="source">Source</option>
                <option value="destination">Destination</option>
              </select>
            </div>
            <div class="form-group mr-2" style="flex:1">
              <!-- <select class="form-control" name="address" id="address" style="max-width: 375px">
                <option value="">Select Address</option>
                @foreach($addresses as $address)
                <option value="{{ $address->id }}">{{ $address->newAddressToString() }}</option>
                @endforeach
              </select> -->
              <input type="text" id="address-field" class="form-control w-100" name="address" placeholder="Add Address">
              <input type="hidden" id="address-id">
            </div>
            <div class="form-group mr-2">
              <select class="form-control" name="contact" id="contact">
                <option value="">Select Contact</option>
                @foreach($contacts as $contact)
                <option value="{{ $contact->id }}">{{ $contact->person->first_name }} {{ $contact->person->last_name }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group mr-2">
              <input type="text" id="note" class="form-control" name="note" placeholder="Note">
            </div>
            <button type="submit" id="add-address" class="btn btn-primary">Add</button>&nbsp;&nbsp;&nbsp;
            <div style="display:flex; justify-content:space-around; margin-top: 1rem;width: 100%">
            <button 
                    data-toggle="modal" 
                    data-target="#addAddressModal" 
                    type="button" id="new-address" class="btn btn-info">New Address</button>
            <button 
                    data-toggle="modal" 
                    data-target="#addContactModal" 
                    type="button" id="new-contact" class="btn btn-info">New Contact</button>
                    </div>
          </form>
        <div class="address-toggle">
          <table class="table table-sm table-striped table-hover mt-4" id="address-table">
            <thead>
              <tr>
                <th>Type</th>
                <th>Address</th>
                <th>Contact</th>
                <th>Note</th>
              </tr>
            </thead>
            <tbody>
              @foreach($project_service->addresses as $address)
              <tr id="row-{{$address->id}}">
                <td>
                  {{ $address->type }}
                </td>
                <td>
                  @if($address->company_address)
                  {{ $address->company_address->newAddressToString() }}
                  @endif
                </td>
                <td>
                  @if(isset($address->contact))
                  {{ $address->contact->person->first_name }} {{ $address->contact->person->last_name }}
                  @endif
                </td>
                <td>
                  {{ $address->note }}
                </td>
                <td style="text-align:right">
                  <button 
                    data-toggle="modal" 
                    data-target="#editAddressModal" 
                    class="btn btn-sm btn-info mr-3 edit-button e-form"
                    data-type="{{$address->type}}"
                    data-address_id="{{$address->company_address->id ?? ""}}"
                    data-contact="{{$address->company_person_id}}"
                    data-note="{{$address->note}}"
                    data-id="{{$address->id}}"
                  >
                    <i class="fa fa-edit"><span class="sr-only">Edit</span></i>
                  </button>
                  <button data-address="{{$address->id}}" class="btn btn-sm btn-danger delete-address e-form"><i class="fa fa-trash"><span class="sr-only">Delete</span></i></button>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- edit address modal -->
<div class="modal fade" id="editAddressModal" tabindex="-1" role="dialog" aria-labelledby="Edit address" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">Edit Address</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span >&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="edit-address-form" class="add-address-form w-100 e-form" method="POST" action="{{ route('edit_address_project_service') }}">
          @csrf 
          <input type="hidden" value="" id="latitude-edit" name="latitude-edit">
          <input type="hidden" value="" id="longitude-edit" name="longitude-edit">
          <input type="hidden" id="id-edit" name="id">
          <input type='hidden' id="address-id-edit" name="address_id_edit">
          <input type='hidden' id="c-address-id" name="address_id">

          <div class="form-group mr-2">
            <select class="form-control" name="type_edit" id="type-edit">
              <option value="">Select Type</option>
              <option value="source">Source</option>
              <option value="destination">Destination</option>
            </select>
          </div>
          <div class="form-group mr-2">
            <select class="form-control" name="contact_edit" id="contact-edit">
              <option value="">Select Contact</option>
              @foreach($contacts as $contact)
              <option value="{{ $contact->id }}">{{ $contact->person->first_name }} {{ $contact->person->last_name }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="note">Notes</label>
            <textarea name="note" id="note-edit" class="form-control" ></textarea>
          </div>
          <div class="form-group">
          <label for="building_name">Building Name</label>
          <input type="text" name="building_name" id="building_name-edit" class="form-control"  />
          </div>
          <div class="form-group">
            <label for="floor">Floor</label>
            <input type="text" name="floor" id="floor-edit" class="form-control" />
          </div>
          <div class="form-group">
            <label for="address1">Address 1</label>
            <input type="text" name="address1" id="address1-edit" class="form-control" />
          </div>
          <div class="form-group">
            <label for="address2">Address 2</label>
            <input type="text" name="address2" id="address2-edit" class="form-control" />
          </div>
          <div class="form-group">
            <label for="city">City</label>
            <input type="text" name="city" id="city-edit" class="form-control" />
          </div>
          <div class="form-group">
            <label for="state">Province</label>
            <input type="text" name="state" id="state-edit" class="form-control" />
          </div>
          <div class="form-group">
            <label for="postal_code">Postal Code</label>
            <input type="text" name="postal_code" id="postal_code-edit" class="form-control" />
          </div>
          <div class="form-group">
            <label for="description">Description</label>
            <textarea name="description" id="description-edit" class="form-control" ></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button id="edit-address-submit" type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- Add Address Modal -->
<div class="modal fade" id="addAddressModal" tabindex="-1" role="dialog" aria-labelledby="Add person" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">Add New Address</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span >&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="add-new-address-form" class="w-100 e-form" method="POST" action="{{ route('store-person-ajax') }}">
          @csrf 
          <input type="hidden" value="" id="latitude" name="latitude">
          <input type="hidden" value="" id="longitude" name="longitude">
          <input type="hidden" id="company_id" name="company_id" value="{{$project_service->project->customer_id}}">
        <div class="form-group">
          <label for="location_search" class="font-weight-bold text-center w-100">Search Location</label>
          <input type="text" name="location_search" id="pac-input" class="form-control" />
        </div>
        <hr class="my-3">
        <div class="form-group">
          <label for="building_name">Building Name</label>
          <input type="text" name="building_name" id="building_name" class="form-control"  />
        </div>
        <div class="form-group">
          <label for="floor">Floor</label>
          <input type="text" name="floor" id="floor" class="form-control" />
        </div>
        <div class="form-group">
          <label for="address1">Address 1</label>
          <input type="text" name="address1" id="address1" class="form-control" />
        </div>
        <div class="form-group">
          <label for="address2">Address 2</label>
          <input type="text" name="address2" id="address2" class="form-control" />
        </div>
        <div class="form-group">
          <label for="city">City</label>
          <input type="text" name="city" id="city" class="form-control" />
        </div>
        <div class="form-group">
          <label for="state">Province</label>
          <input type="text" name="state" id="state" class="form-control" />
        </div>
        <div class="form-group">
          <label for="postal_code">Postal Code</label>
          <input type="text" name="postal_code" id="postal_code" class="form-control" />
        </div>
        <div class="form-group">
          <label for="description">Description</label>
          <textarea name="description" id="description" class="form-control" ></textarea>
        </div>
        <div class="form-group">
          <label for="note">Notes</label>
          <textarea name="note" id="note" class="form-control" ></textarea>
        </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button id="add-new-address" type="submit" class="btn btn-primary">Add</button>
      </div>
    </div>
  </div>
</div>


<!-- Add Contact Modal -->
<div class="modal fade" id="addContactModal" tabindex="-1" role="dialog" aria-labelledby="Add contact" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">Add New Contact</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span >&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="add-new-contact-form" class="w-100 e-form" method="POST">
          @csrf 
          <input type="hidden" id="company_contact_id" name="company_contact_id" value="{{$project_service->project->customer_id}}">
          <div class="form-group">
            <label for="contact_name">First Name*</label>
            <input type="text" name="first_name" id="first_name" class="form-control" required  />
          </div>
          <div class="form-group">
            <label for="contact_name">Last Name*</label>
            <input type="text" name="last_name" id="last_name" class="form-control" required  />
          </div>
          <div class="form-group">
            <label for="contact_name">Email</label>
            <input type="email" name="email" id="email" class="form-control"  />
          </div>
          <div class="form-group">
            <label for="contact_name">Phone Number*</label>
            <input type="text" name="phone_number" id="phone_number" class="form-control" required  />
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button id="add-new-contact" type="submit" class="btn btn-primary">Add</button>
      </div>
    </div>
  </div>
</div>

@push('scripts')
<script>
  $(document).ready(function(){
  $("#address-field").click(function(){
    $("#addAddressModal").modal();
  });
});
  $('#add-new-contact').on('click', (e) => {
    e.preventDefault()
    $.ajax({
      type: 'POST',
      url: "{{route('ajax_add_new_contact')}}",
      data: $("#add-new-contact-form").serialize(),
      success: function(data) {
        // console.log(data)
        
        let optionHTML = `
        <option value="${data.contact.contact_id}" selected> 
            ${data.contact.first_name} ${data.contact.last_name} 
        </option>`;
        // console.log(optionHTML)
        $('#contact').append(optionHTML)
        $('#addContactModal').modal('hide')
      }
    })
  })
    $('#add-new-address').on('click', function(e) {
      e.preventDefault();
      $.ajax({
        type: 'POST',
        url:"{{route('ajax_add_new_address')}}",
        data:$("#add-new-address-form").serialize()
      ,
      success:function(data){
        console.log(data.address)
        $('#address-field').val(data.address.address)
        $('#address-id').val(data.address.address_id)
        $('#addAddressModal').modal('hide')
      }})
    })

  $(document).on('click', '.edit-button', function(e) {
    $.ajax({
      type: 'POST',
      url:"{{route('ajax_get_address')}}",
      data: {
        address_id : $(this).data('address_id'),
        psaddress : $(this).data('id'),
          _token: "{{csrf_token()}}"
      },
      success: function(data) {
        console.log(data)
        let address = data.address.address
        $('#c-address-id').val(data.address.id)
        $('#address-id-edit').val(address.id)
        $('#building_name').val(address.building_name)
        $('#address1-edit').val(address.address1)
        $('#address2-edit').val(address.address2)
        $('#floor-edit').val(address.floor)
        $('#city-edit').val(address.city)
        $('#postal_code-edit').val(address.postcode)
        $('#state-edit').val(address.state)
        $('#note-edit').val(data.psaddress.note)
      }
    })
    $('#id-edit').val($(this).data('id'))
    $('#type-edit option[value="' + $(this).data('type') + '"]').prop('selected', true)
    $('#contact-edit option[value="' + $(this).data('contact') + '"]').prop('selected', true)
    $('#note-edit').val($(this).data('note'))
  })

  $(document).on('click', '#edit-address-submit', function(e) {
    e.preventDefault()
    $.ajax({
        type:'POST',
        url:"{{route('edit_address_project_service')}}",
        data:{
          type: $('#type-edit option:selected').val(), 

          company_address_id: $('#c-address-id').val(), 
          address_id: $('#address-id-edit').val(),
          note: $('#note-edit').val(),
          company_person_id: $('#contact-edit').val(),
          project_service_id: {{$project_service->id}},
          address1 : $('#address1-edit').val(),
          address2 : $('#address2-edit').val(),
          city : $('#city-edit').val(),
          state : $('#state-edit').val(),
          postal_code : $('#postal_code-edit').val(),
          note : $('#note-edit').val(),
          floor : $('#floor-edit').val(),
          building_name : $('#building_name-edit').val(),
          id: $('#id-edit').val(),
          _token: "{{csrf_token()}}"
        },
        success:function(data){
          let addressString = data.address_string
          console.log(data.note)
          let contact
          let note
          if(data.note == null){
            note = ''
          } else {
            note = data.note
          }
          if(data.contact.first_name) {
            contact = data.contact.first_name + " " + data.contact.last_name
          } else {
            contact = ''
          }
          $('#row-' + data.ps_id).html(`
            <td>${data.type}</td>
            <td>${addressString}</td>
            <td>${contact}</td>
            <td>${note}</td>
            <td style='text-align:right'>
            <button class='btn btn-sm btn-info mr-3'
              data-toggle="modal" 
              data-target="#editAddressModal" 
              class="btn btn-sm btn-info mr-3 edit-button"
              data-type="${data.type}"
              data-address_id="${data.address.id}"
              data-contact="${data.address.company_person_id}"
              data-note="${data.note}"
              data-id="${data.ps_id}"
            >
            <i class='fa fa-edit'><span class='sr-only'>Edit</span></i></button>
            <button data-address="${data.address.id}" class='btn btn-sm btn-danger delete-address'><i class='fa fa-trash'><span class='sr-only'>Delete</span></i></button>
            </td>
          `)
          $('#editAddressModal').modal('hide')

        }

    })
  })

    $('#add-address').on('click', function(e) {
    e.preventDefault();
    let type = $('#type').val()
    let address = $('#address-id').val()
    let note = $('#note').val()
    let contact = $('#contact').val()
    let addressString = $('#address-field').val()
    console.log(address)
    if(type == '' || address == '') {
      alert('You must select a type and address to continue')
      return
    }
    $.ajax({
        type:'POST',
        url:"{{route('add_address_to_project_service')}}",
        data:{type:type, 
          company_address_id: address, 
          note: note,
          company_person_id: contact,
          project_service_id: {{$project_service->id}},
          _token: "{{csrf_token()}}"
        },
        success:function(data){
          console.log(data.address)
          let note;
          let name;
          let id;
          let type;
          if(data.contact) {
            let first = data.contact.first_name ? data.contact.first_name : ''
            let last = data.contact.last_name ? data.contact.last_name : ''
            id = data.address.id
            name = first + ' ' + last
          } else {
            name = ' '
            id = ' '
          }
          if(typeof(data.address.note) == 'undefined' || data.address.note === null) {
            note=''
          } else {
            note = data.address.note
          }
          
          $('#address-table tbody').append(`<tr id="row-${data.address.id}"><td>${data.address.type}</td><td>${addressString}</td><td>${name}</td><td>${note}</td><td style='text-align:right'>
          <button
            data-toggle="modal" 
              data-target="#editAddressModal" 
              class="btn btn-sm btn-info mr-3 edit-button"
              data-type="${data.address.type}"
              data-address_id="${data.address.id}"
              data-contact="${data.address.company_person_id}"
              data-note="${data.address.note}"
              data-id="${data.address.id}"
          ><i class='fa fa-edit'><span class='sr-only'>Edit</span></i></button>
          <button data-address="${id}" class='btn btn-sm btn-danger delete-address'><i class='fa fa-trash'><span class='sr-only'>Delete</span></i></button></td></tr>`)
          $('#add-address-form')[0].reset()
        }
      });
  })
  $(document).on('click', '.delete-address', function(e) {
    e.preventDefault()
    let id = $(this).data('address')
    let row = $(this).closest('tr')
    let res = confirm('Are you sure you want to remove this address?')
    if(res === true) {
      $.ajax({
        type:'DELETE',
        url:"{{route('delete_address_from_project_service')}}",
        data:{id:id,
          _token: "{{csrf_token()}}"
        },
        success:function(data){
          if(data.result === 1) {
            row.fadeOut()
          }
        }
      });
    }
  })

let autocomplete
function initAC() {
  autocomplete = new google.maps.places.Autocomplete(
    document.getElementById('pac-input'),
    {
      fields: ['place_id', 'geometry', 'name', 'address_components']
    }
  )
  autocomplete.addListener('place_changed', onPlaceChanged)
}

function onPlaceChanged() {
  // console.log('hi')
  var place = autocomplete.getPlace()

  if (!place.geometry) {
    document.getElementById('pac-input').placeholder = 'Enter an address'
  } else {
    // console.log(place.address_components)
    fillInAddress()
  }
}

function fillInAddress() {
  // Get the place details from the autocomplete object.
  const place = autocomplete.getPlace();
  let address1 = "";
  let postcode = "";

  // Get each component of the address from the place details,
  // and then fill-in the corresponding field on the form.
  // place.address_components are google.maps.GeocoderAddressComponent objects
  // which are documented at http://goo.gle/3l5i5Mr
  // console.log(place.geometry.location.lat())
  document.querySelector('#latitude').value = place.geometry.location.lat()
  document.querySelector('#longitude').value = place.geometry.location.lng()
  for (const component of place.address_components) {
    // @ts-ignore remove once typings fixed
    const componentType = component.types[0];

    switch (componentType) {
      case "street_number": {
        address1 = `${component.long_name} ${address1}`;
        break;
      }

      case "route": {
        address1 += component.short_name;
        break;
      }

      case "postal_code": {
        postcode = `${component.long_name}${postcode}`;
        document.querySelector("#postal_code").value = component.long_name;
        break;
      }

      case "postal_code_suffix": {
        postcode = `${postcode}-${component.long_name}`;
        break;
      }
      case "locality":
        document.querySelector("#city").value = component.short_name;
        break;
      case "administrative_area_level_1": {
        document.querySelector("#state").value = component.short_name;
        break;
      }
    }
    document.getElementById('address1').value = address1
  }

  // console.log(address1)
}
</script>
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDIJ1CzwFZ5LOESivAyQvGoKHcRyBG4t6E&loading=async&libraries=places&callback=initAC">
</script>
@endpush
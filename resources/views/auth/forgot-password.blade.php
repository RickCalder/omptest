@extends('layouts.no-header')

@section('content')
<div class="col-lg-6 offset-lg-3 d-flex flex-column" style="height: 100vh">
  <div class="d-flex flex-column m-auto">
  <img class="mx-auto h-50 w-auto" src="/img/omp-logo.svg" alt="Workflow" />
  <h1 class="h2 font-weight-bold my-4">
    Reset Password
  </h1>

  <div>
    <div>
      <form method="POST">
        @csrf
        <div class="form-group">
          <label for="email">Email address</label>
          <input type="email" class="form-control" id="email" value="{{ old('email') }}" name="email" autofocus placeholder="Enter email" required>
          </div>
        </div>
        <div class="mt-4">
          <button type="submit"
            class="btn btn-primary w-100">
            Submit
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
@endsection
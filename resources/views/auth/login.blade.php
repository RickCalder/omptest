@extends('layouts.no-header')

@section('content')
<div class="col-lg-6 offset-lg-3 d-flex flex-column" style="height: 100vh">
  <div class="d-flex flex-column m-auto">
  <img class="mx-auto h-50 w-auto" src="/img/omp-logo.svg" alt="Workflow" />
  <h1 class="h2 font-weight-bold my-4">
    Sign in to your account
  </h1>

  <div>
    <div>
      <form method="POST" action="{{ route('login') }}">
        @csrf
        @error('email')
        <div class="alert alert-danger" role="alert"
          role="alert">
          <strong>{{ $message }}</strong>
        </div>
        @enderror
        @error('password')
        <div class="alert alert-danger" role="alert"
          role="alert">
          <strong>{{ $message }}</strong>
        </div>
        @enderror
        <div class="form-group">
          <label for="email">Email address</label>
          <input type="email" class="form-control" id="email" value="{{ old('email') }}" name="email" autofocus placeholder="Enter email" required>
          </div>
        </div>
        
        <div class="form-group">
          <label for="password">Password</label>
          <input type="password" class="form-control" id="password" name="password" required>
          </div>
        </div>
        <div class="row">
          <div class="col-6">
            <div class="form-check">
              <input class="form-check-input" type="checkbox" value="" id="remember_me" name="remember_me">
              <label class="form-check-label" for="remember_me">
                Remember Me
              </label>
            </div>
            
          </div>
          <div class="col-6">
            <div class="text-right">
              <a href="/forgot-password">Forgot Password</a>
            </div>
            
          </div>
        </div>

        <div class="mt-4">
            <button type="submit"
              class="btn btn-primary w-100">
              Sign in
            </button>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
@endsection
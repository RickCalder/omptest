@extends('layouts.app')

@section('content')

@php 
  $payroll = false;
  foreach($project_service->branch->apis as $api) {
    if($api->api_name->name =='Payroll API') {
      $payroll = true;
    }
  }
@endphp
<div class="text-gray-900 mt-5">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  <h2 class="text-center my-8 text-3xl font-extrabold">{{$project_service->id}} -
    {{$project_service->project->customer->name}} -
    {{$project_service->project->name}}</h2>
  <div class="d-flex flex-wrap mt-3 mb-2">
    <div class="btn-nav-wrapper">
      <button class="btn btn-sm btn-primary btn-nav" data-component="all">Show All</button>
    </div>
    <div class="btn-nav-wrapper">
      <button class="btn btn-sm btn-primary btn-nav" data-component="service">Service</button>
    </div>
    <div class="btn-nav-wrapper">
      <button class="btn btn-sm btn-primary btn-nav" data-component="documents">Documents</button>
    </div>
    <div class="btn-nav-wrapper">
      <button class="btn btn-sm btn-primary btn-nav" data-component="people">People</button>
    </div>
    <div class="btn-nav-wrapper">
      <button class="btn btn-sm btn-primary btn-nav" data-component="addresses">Addresses</button>
    </div>
    <div class="btn-nav-wrapper">
      <button class="btn btn-sm btn-primary btn-nav" data-component="equipment">Equipment</button>
    </div>
    <div class="btn-nav-wrapper">
      <button class="btn btn-sm btn-primary btn-nav" data-component="supplies">Supplies</button>
    </div>
    <div class="btn-nav-wrapper">
      <button class="btn btn-sm btn-primary btn-nav" data-component="notes">Notes</button>
    </div>
    <div class="btn-nav-wrapper">
      <button class="btn btn-sm btn-primary btn-nav" data-component="specialinstructions">Special Instructions</button>
    </div>
    <div class="btn-nav-wrapper">
      <button class="btn btn-sm btn-primary btn-nav" data-component="related">Related</button>
    </div>
    @if($payroll)
    <div class="btn-nav-wrapper">
      <button class="btn btn-sm btn-primary btn-nav" data-component="schedule">Schedule</button>
    </div>
    @endif
    <div class="btn-nav-wrapper">
      <button class="btn btn-sm btn-primary btn-nav" data-component="changes">Changes</button>
    </div>
  </div>
  <div class="d-flex flex-wrap mb-3">
    <form action="{{route('supervisor-control')}}" method="POST" class="form-inline">
      @csrf
      <input type="hidden" name="sc_pid" id="sc-pid" class="form-control mr-2" value="{{ $project_service->id }}">
      <div class="btn-nav-wrapper">
        <input type="submit" class="btn btn-sm btn-dark" value="Supervisor Control Sheet">
      </div>
    </form>

    
    <form action="{{route('time-sheet')}}" method="POST" class="form-inline">
      @csrf
      <input type="hidden" name="ts_pid" id="ts-pid" class="form-control mr-2" value="{{ $project_service->id }}">
      <div class="btn-nav-wrapper">
        <input type="submit" class="btn btn-sm btn-dark" value="Time Sheet">
      </div>
    </form>

    
    <form action="{{route('work-order')}}" method="POST" class="form-inline">
      @csrf
      <input type="hidden" name="wo_pid" id="wo-pid" class="form-control mr-2" value="{{ $project_service->id }}">
      <div class="btn-nav-wrapper">
        <input type="submit" class="btn btn-sm btn-dark" value="Work Order">
      </div>
    </form>
  </div>
  <div class="service-component componentservice">
    @include('components/ps-service')
  </div>
  <div class="service-component componentdocuments">
    @include('components/ps-documents')
  </div>
  <div class="service-component componentpeople">
    @include('components/ps-people')
  </div>
  <div class="service-component componentaddresses">
    @include('components/ps-addresses')
  </div>
  <div class="service-component componentequipment">
    @include('components/ps-equipment')
  </div>
  <div class="service-component componentsupplies">
    @include('components/ps-supplies')
  </div>
  <div class="service-component componentnotes">
      @include('components/ps-notes')
  </div>
  <div class="service-component componentspecialinstructions">
      @include('components/ps-special-notes')
  </div>
  <div class="service-component componentschedule">
    @include('components/ps-schedule')
  </div>
  <div class="service-component componentrelated">
    @include('components/ps-related')
  </div>
  <div class="service-component componentchanges">
    @include('components/ps-changes')
  </div>

</div>
@endsection


@push('scripts')
<script>
  $('.toggle').on('click', function(e) {
    let target = $(this).data('target')
    $(target).slideToggle()
    if($(this).data('type') === 'add-button') return
    if($(this).find('i').hasClass('fa-chevron-down')) {
      $(this).find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up')
    } else {
      $(this).find('i').addClass('fa-chevron-down').removeClass('fa-chevron-up')
    }
    
  })

</script>

<script>
  $(document).ready(function() {
    $('.service-component').show()
    $('.e-form').hide()
  })

  $('.btn-nav').on('click', function(e) {
    e.preventDefault()
    if($(this).data('component') === 'all') {
      $('.service-component').fadeIn()
      $('.e-form').hide()
      return false;
    }
    let component = '.component' + $(this).data('component')
    $('.service-component').hide()
    $(component).fadeIn()
    $('.e-form').show()
  })
</script>
@endpush
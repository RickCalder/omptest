@extends('layouts.app')

@section('add-head')
@endsection 

@section('content')
<div class="text-gray-900 mt-5 mb-5">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  <h2 class="text-center my-8 text-3xl font-extrabold">Edit Project Service</h2>
  <div class="row">
    <div class="col-lg-8">
      <form class="mt-4" id="create-project-service-form" method="POST" action="{{route('update-project-service', $project_service->id)}}">
        @csrf
        @method('PUT')
        <div class="form-group">
          <label for="occurs-on">Nickname</label>
          <input id="nickname" maxlength="20" name="nickname" type='text' class="form-control" required value="{{$project_service->nickname}}" />
        </div>
        <div class="form-group">
          <label for="occurs-on">Occurs On</label>
          <input id="occurs-on" name="occurs_on" type='text' class="form-control" required value="{{$project_service->occurs_on}}" />
        </div>
        <div class="form-group">
          <label for="occurs-at">Occurs At</label>
          <input id="occurs-at" name="occurs_at" type='text' class="form-control" required value="{{$project_service->occurs_at}}" />
        </div>
        <div class="form-group">
          <label for="est-hours">Estimated Hours</label>
          <input id="est-hours" name="minutes_est" type="number" class="form-control" value="{{$project_service->minutes_estimate/60}}" >
        </div>
        <div class="form-group">
          <label for="service">Service</label>
          <select name="service" class="form-control" id="service" required>
            <option value="">Select Service</option>
            @foreach($services as $service)
            <option value="{{$service->id}}" @if($project_service->service_id == $service->id)selected @endif >{{$service->name}}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="branch">Branch</label>
          <select name="branch" class="form-control" id="branch" required>
            <option value="">Select Branch</option>
            @foreach($branches as $branch)
            <option value="{{$branch->id}}" @if($project_service->branch_id == $branch->id)selected @endif>{{$branch->name}}</option>
            @endforeach
          </select>
        </div>
        <input type="hidden" name="project" value="{{$project_service->project->id}}" />
        <div class="form-group">
          <label for="status">Status</label>
          <select name="status" class="form-control" id="status">
            <option value="">Select Status</option>
            <option value="entered" @if($project_service->status == 'entered')selected @endif>Entered</option>
            <option value="pending" @if($project_service->status == 'pending')selected @endif>Pending</option>
            <option value="booked" @if($project_service->status == 'booked')selected @endif>Booked</option>
            <option value="incomplete" @if($project_service->status == 'incomplete')selected @endif>Incomplete</option>
            <option value="complete" @if($project_service->status == 'complete')selected @endif>Complete</option>
            <option value="cancelled" @if($project_service->status == 'cancelled')selected @endif>Cancelled</option>
          </select>
        </div>
        <div class="form-group">
          <label for="invoice">Invoice Number</label>
          <input type="text" class="form-control" name="invoice" id="invoide" placeholder="Invoice Number" value="{{$project_service->invoice_number ?? ''}}">
        </div>
        <div class="form-group">
          <input type="submit" class="btn btn-primary" id="create-service">
          <a href="/project_service/{{$project_service->id}}" class="btn btn-danger ml-3">Cancel</a>
        </div>
      </form>
      <div class="alert alert-info no-projects" role="alert" style="display:none">
        {{ session('status') }}
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script>

  $('#occurs-on').change(function(){
    $('#occurs-detail').html('<strong>Occurs On: </strong>' + $(this).val())
  })
  $('#est-hours').change(function(){
    $('#hours-detail').html('<strong>Est. Hours: </strong>' + $(this).val())
  })
  $('#status').change(function(){
    $('#status-detail').html('<strong>Status: </strong>' + $(this).val())
  })
  $('#service').change(function(){
    $('#service-detail').html('<strong>Service: </strong>' + $(this).find('option:selected').text())
  })
  $(document).ready(function() {
    $('#occurs-on').datetimepicker({
      timepicker: false,
      format:'Y-m-d',
      step: 15
    });
    $('#occurs-at').datetimepicker({
      datepicker: false,
      format:'H:i',
      step: 15
    });
  })
  
</script>


@endpush 
@extends('layouts.app')

@section('add-head')
@endsection 

@section('content')
<div class="text-gray-900 mt-5 mb-5">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  <h2 class="text-center my-8 text-3xl font-extrabold">Create New Project Service</h2>
  <div class="row">
    <div class="col-lg-8">
      <form class="mt-4" id="create-project-service-form" method="POST" action="{{route('store-project-service')}}">
        @csrf
        <div class="form-group">
          <label for="occurs-on">Nickname </label>
          <input id="nickname" maxlength="20" name="nickname" type='text' class="form-control" required />
        </div>
        <div class="form-group">
          <label for="occurs-on">Occurs On</label>
          <input id="occurs-on" name="occurs_on" type='text' class="form-control" required />
        </div>
        <div class="form-group">
          <label for="occurs-at">Occurs At</label>
          <input id="occurs-at" name="occurs_at" type='text' class="form-control" required />
        </div>
        <div class="form-group">
          <label for="est-hours">Estimated Hours</label>
          <input id="est-hours" name="minutes_est" type="number" class="form-control" >
        </div>
        <div class="form-group">
          <label for="service">Service</label>
          <select name="service" class="form-control" id="service" required>
            <option value="">Select Service</option>
            @foreach($services as $service)
            <option value="{{$service->id}}">{{$service->name}}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          @if(isset($customer))
          <label>Branch</label>
          <div>
            <p>{{$project->branch->name}}</p>
            <input type="hidden" name="branch" value="{{$project->branch_id}}" />
          </div>

          @else
            @if($user_branch === '')
              <label for="branch">Branch</label>
              <select name="branch" class="form-control" id="branch" required>
                <option value="">Select Branch</option>
                @foreach($branches as $branch)
                <option value="{{$branch->id}}">{{$branch->name}}</option>
                @endforeach
              </select>
            @else
            Branch: {{$user_branch->name ?? ''}}
            <input type="hidden" name="branch" value="{{$user_branch->id ?? ''}}">
            @endif
          @endif
        </div>
        <div class="form-group">
          @if(isset($customer))
          <label>Customer</label>
          <div>
            <p>{{$project->customer->name}}</p>
            <input type="hidden" name="customer" value="{{$project->customer_id}}" />
          </div>
          @else
          <label for="customer">Customer</label>
          <div id="customers-div">
            <input class="typeahead form-control" type="text" name="customer" id="customer" />
            <input type="hidden" id="customer-id" name="customer_id"/>
          </div>
          @endif
        </div>
        <div class="form-group">
          @if(isset($customer))
          <label>Sub Customer</label>
          <div>
            @if($sub_customer)
            <p>{{$project->sub_customer->name}}</p>
            <input type="hidden" name="sub_customer" value="{{$project->sub_customer->id}}" />
            @else
            <p>None</p>
            @endif
          </div>
          @else
          <label for="sub_customer">Sub Customer</label>
          <select name="sub_customer" class="form-control" id="sub_customer" disabled>
            <option value="">Select Sub Customer</option>
            @foreach($sub_customers as $sub_customer)
            <option value="{{$sub_customer->id}}">{{$sub_customer->name}}</option>
            @endforeach
          </select>
          @endif
        </div>
        <div class="form-group">
          @if(isset($customer))
          <label>Project</label>
          <div>
            <p>{{$project->name}}</p>
            <input type="hidden" name="project" value="{{$project->id}}" />
          </div>
          @else
          <label for="project">Project</label>
          <select name="project" class="form-control" id="project" disabled required>
            <option value="">Select Project</option>
          </select>
          @endif
        </div>
        <div class="form-group">
          <label for="status">Status</label>
          <select name="status" class="form-control" id="status">
            <option value="">Select Status</option>
            <option value="entered">Entered</option>
            <option value="pending">Pending</option>
            <option value="booked">Booked</option>
            <option value="incomplete">Incomplete</option>
            <option value="complete">Complete</option>
            <option value="cancelled">Cancelled</option>
          </select>
        </div>
        <div class="form-group">
          <label for="invoice">Invoice Number</label>
          <input type="text" class="form-control" name="invoice" id="invoide" placeholder="Invoice Number">
        </div>
        <div class="form-group">
          <input type="submit" class="btn btn-primary" id="create-service">
        </div>
      </form>
      <div class="alert alert-info no-projects" role="alert" style="display:none">
        {{ session('status') }}
      </div>
    </div>
    @if(! isset($customer))
    <div class="col-lg-4 mt-4">
      <p class="mb-0" id="occurs-detail"></p>
      <p class="mb-0" id="hours-detail"></p>
      <p class="mb-0" id="status-detail"></p>
      <p class="mb-0" id="service-detail"></p>
      <p class="mb-0" id="customer-detail"></p>
      <p class="mb-0" id="sub-customer-detail"></p>
      <p class="mb-0" id="branch-detail"></p>
      <p class="mb-0" id="project-detail"></p>
    </div>
    @else
    <div class="col-lg-4 mt-4">
      <p class="mb-0" id="occurs-detail"></p>
      <p class="mb-0" id="hours-detail"></p>
      <p class="mb-0" id="status-detail"></p>
      <p class="mb-0" id="service-detail"></p>
      <p class="mb-0"><strong>Branch:</strong> {{$project->branch->name}}</p>
      <p class="mb-0"><strong>Customer:</strong> {{$project->customer->name}}</p>
      @if($sub_customer)
      <p class="mb-0"><strong>Sub Customer:</strong> {{$project->sub_customer->name}}</p>
      @else
      <p class="mb-0"><strong>Sub Customer:</strong> None</p>
      @endif
      <p class="mb-0"><strong>Project:</strong> {{$project->name}}</p>
    </div>
    @endif
  </div>
</div>
@endsection

@push('scripts')
<script>
  function subcustomers() {
    let customer = $('#customer-id').val()
    if(customer !== '') {
      $('#customer-detail').html('<strong>Customer: </strong>' + $(this).find('option:selected').text())
      $('#sub_customer').prop('disabled', true)
      $('.no-projects').fadeOut()
      $('#project option').remove()
      $('#project').prop('disabled', true)
      $.ajax({
        type:'POST',
        url:"{{ route('get-sub-customers') }}",
        data:{customer: customer},
        success:function(data){
          console.log(data)
          $('#sub_customers option').remove()
          if(data.sub_customers.length > 0 ){
            $("#sub_customers").append('<option value="">Select Sub Customer</option>')
            $.each(data.sub_customers, function(){
                $("#sub_customer").append('<option value="'+ this.id +'">'+ this.name +'</option>')
            })
            $('#sub_customer').prop('disabled', false)
          }
          if(data.projects.length > 0) {
            $('#project option').remove()
            $("#project").append('<option value="">Select Project</option>')
            $.each(data.projects, function(){
                $("#project").append('<option value="'+  this.id +'">'+ this.po_number + ' &mdash; ' +this.name +'</option>')
            })
            $('#project').prop('disabled', false)
          } else {
            $('.no-projects').html('There are no projects matching these parameters, would you like to create a new project?').fadeIn()
          }
        }
      });
    }
  }

  $('#branch').change( function(e) {
    $('#project').prop('disabled', true)
    $('.no-projects').fadeOut()
    if(branch !== '') {
      $('#branch-detail').html('<strong>Branch: </strong>' + $(this).find('option:selected').text())
    }
  })
  
  $('#sub_customer').change(function(){
    let branch = $('#branch').find('option:selected').val()
    let customer = $('#customer-id').val()
    let sub_customer = $('#sub_customer').find('option:selected').val()
    $('#project').prop('disabled', true)
    if(branch !== '') {
      $('.no-projects').fadeOut()
      $('#sub-customer-detail').html('<strong>Sub Customer: </strong>' + $(this).find('option:selected').text())
      $.ajax({
        type:'POST',
        url:"{{ route('get-projects') }}",
        data:{branch:branch, customer: customer, sub_customer: sub_customer},
        success:function(data){
          console.log(data)
          if(data.projects.length > 0) {
            $('#project option').remove()
            $("#project").append('<option value="">Select Project</option>')
            $.each(data.projects, function(){
                $("#project").append('<option value="'+ this.id +'">'+ this.po_number + ' &mdash; ' +  this.name +'</option>')
            })
            $('#project').prop('disabled', false)
          } else {
            $('.no-projects').html('There are no projects matching these parameters, would you like to create a new project?').fadeIn()
          }
        }
      });
    }
  })


  $('#project').change(function(e) {
    let project = $(this).find('option:selected').val()
    if( project !== '' ) {
      $('#project-detail').html('<strong>Project: </strong>' + $(this).find('option:selected').text())
    }
  })

  $('#occurs-on').change(function(){
    $('#occurs-detail').html('<strong>Occurs On: </strong>' + $(this).val())
  })
  $('#est-hours').change(function(){
    $('#hours-detail').html('<strong>Est. Hours: </strong>' + $(this).val())
  })
  $('#status').change(function(){
    $('#status-detail').html('<strong>Status: </strong>' + $(this).val())
  })
  $('#service').change(function(){
    $('#service-detail').html('<strong>Service: </strong>' + $(this).find('option:selected').text())
  })
  $(document).ready(function() {
    $('#occurs-on').datetimepicker({
      timepicker: false,
      format:'Y-m-d',
      step: 15
    });
    $('#occurs-at').datetimepicker({
      datepicker: false,
      format:'H:i',
      step: 15
    });
  })

  // $('#create-project-service-form').submit(function(e) {
  //   e.preventDefault()

  // })
  let path = "{{ route('customer_autocomplete') }}";
    $('input.typeahead').typeahead({
        items: 50,
        source:  function (query, process) {
            return $.get(path, { query: query }, function (data) {
                return process(data);
            });
        },
        highlighter: function (item, data) {
            var parts = item.split('#'),
                html = '<div class="row">';
                html += '<div class="col-md-2">';
                html += '</div>';
                html += '<div class="col-md-10 pl-0">';
                html += '<span>'+data.name+'</span>';
                html += '</div>';
                html += '</div>';

            return html;
        },
        afterSelect:function(data,value,text){
            $('#customer-id').val(data.id)
            subcustomers()
        },
    })
  
</script>


@endpush 
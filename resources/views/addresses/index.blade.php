@extends('layouts.app')

@section('content')

<div class="text-gray-900 mt-5">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  <div>
    <h2 class="mt-4 mb-2 h1 font-weight-bold text-center">Addresses</h2>
    {{-- @include('components/people-filter') --}}
    
    <table class="table table-sm table-striped table-hover mt-4">
      <thead>
        <tr>
          <th>
            Address
          </th>
        </tr>
      </thead>
      <tbody>
        @foreach($addresses as $address)
        <tr>
          <td>
            {{ $address->addressToString() }}
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <div class="d-flex justify-end my-4">
    @if ($addresses->hasPages())
    {{ $addresses->appends(Request::except('page'))->links() }}
    @endif
  </div>
</div>
@endsection

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Office Move Pro') }}</title>
  


  <!-- Styles -->
  <link href="{{ mix('css/app.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body class="bg-gray-50">
  @php 
  $user = auth()->user() ? auth()->user() : [];
  @endphp
  <div id="app">
    @if($user)
    @include('components/navbar')
    @endif
  </div>

  <main class="container mx-auto h-100">
    @yield('content')
  </main>

<!-- Scripts -->
<script src="{{ mix('/js/app.js') }}"></script>
@stack('scripts')
</body>

</html>
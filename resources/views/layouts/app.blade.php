<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Office Move Pro') }}</title>
  


  <!-- Styles -->
  <link href="{{ mix('css/app.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  @livewireStyles
  @yield('add-head')
</head>

<body class="bg-gray-50">
  @php 
  $user = auth()->user() ? auth()->user() : [];
  @endphp
  <div id="app">
    @if($user)
    @include('components/navbar')
    @endif
  </div>

  <main class="container mx-auto">
    @yield('content')
  </main>

<!-- Scripts -->
<script src="{{ mix('/js/app.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
<script>
  $('a[href="' + this.location.href + '"]').parents('li,ul').addClass('active');
  $('a[href="' + this.location.href + '"]').each(function(){
    if($(this).hasClass('dropdown-item')) {
      $(this).addClass('active-sub');
    }
  })
  
</script>
@stack('scripts')
@livewireScripts
</body>

</html>

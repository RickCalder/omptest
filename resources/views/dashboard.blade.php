@extends('layouts.app')

@section('content')

<div class="text-center text-gray-900 mt-5">
  {{-- @if (session('status'))
  <div class="alert alert-danger" role="alert">
    {{ session('status') }}
  </div>
  @endif --}}
  @if(!empty($current_branch))
  <h2 class="mt-4 mb-2 h1 font-weight-bold">Services for {{$current_branch->name}}</h2>
  @elseif(!empty($current_customer))
  <h1 class="mt-4 mb-2 h1 font-weight-bold">Services for {{$current_customer->name}}</h1>
  @else
  <h2 class="mt-4 mb-2 h1 font-weight-bold">Services</h2>
  @endif

  <div class="d-flex flex-column">
    <div class="my-2 py-2">
    @if(count($branches) !== 0 )
      @include('components/service-filter')
    @endif
      @include('components/flash-message')
      @if(count($branches) !== 0)
        @if(count($project_services))
        @include('components/dashboard-service')

        @else 
        <h2 class="h1">No Services match your filter parameters</h2>
        
        @endif
      @else 
        <h2 class="h1">You do not have access to any locations, please contact an admin!</h2>
      @endif
      @if(count($project_services) > 1)
      <div class="d-flex justify-end my-4">
        @if ($project_services->hasPages())
        {{ $project_services->appends(Request::except('page'))->links() }}
        @endif
      </div>
      @endif
    </div>
    @endsection

    @push('scripts')
    <script>
      $('.filter-toggle').on('click', function(e) {
      e.preventDefault()
      $('#filter-panel').toggle('fast')
      if($('.filter-toggle i').hasClass('fa-chevron-up')) {
        $('.filter-toggle i').removeClass('fa-chevron-up').addClass('fa-chevron-down')
      } else {
        $('.filter-toggle i').removeClass('fa-chevron-down').addClass('fa-chevron-up')
      }
    })
    
    $('.expand-this').on('click', function(e) {
      e.preventDefault()
      let expandId = $(this).data('expand')
      $('.' + expandId ).slideToggle()



      if($(this).find('i').hasClass('fa-chevron-down')) {
        $('.expand-this[data-expand="' + expandId +'"] > i').removeClass('fa-chevron-down').addClass('fa-chevron-up')
      } else {
        $('.expand-this[data-expand="' + expandId +'"] > i').removeClass('fa-chevron-up').addClass('fa-chevron-down')
      }
    })

    </script>
    @endpush
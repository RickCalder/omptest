@extends('layouts.app')

@section('content')

<div class="text-gray-900 mt-5">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  <div>
    <h2 class="mt-4 mb-2 h1 font-weight-bold text-center">
      {{ $company->name }}
    </h2>
    <div class="d-flex flex-column">
      <div class="my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
        <div class="align-middle inline-block min-w-full overflow-hidden sm:rounded-lg">
          <div class="border text-left p-4">
            <div class="d-flex justify-content-between align-items-start">
            <table>
              <tr>
                <td>
                  <span class="font-weight-bold col">Name: </span>
                </td>
                <td>
                  <span>{{$company->name}} </span>
                </td>
              </tr>
              @if(isset($parent->name))
              <tr>
                <td>
                  <span class="font-weight-bold col">Parent Company: </span>
                </td>
                <td>
                  @if(isset($parent->name))
                  <span>{{$parent->name}} </span>
                  @endif
                </td>
              </tr>
              @endif
              <tr>
                <td>
                  <span class="font-weight-bold col">Phone: </span>
                </td>
                <td>
                  <span><a href="tel:{{$company->phone}}"> {{$company->phone}}</a></span>
                </td>
              </tr>
              <tr>
                <td>
                  <span class="font-weight-bold col">Email: </span>
                </td>
                <td>
                  <span><a href="mailto:{{$company->email}}"> {{$company->email}}</a></span>
                </td>
              </tr>
              <tr>
                <td>
                  <span class="font-weight-bold col">Website: </span>
                </td>
                <td>
                  <span><a href="{{$company->url}}" target="_blank"> {{$company->url}}</a></span>
                </td>
              </tr>
              <tr>
                <td>
                  <span class="font-weight-bold col">Notes: </span>
                </td>
                <td>
                  <span>{{$company->note}}</span>
                </td>
              </tr>
            </table>
            <div class="d-flex flex-column">
              <a href="{{route('create-project', ['customer_id' => $company->id])}}" class="btn btn-sm btn-info">Add Project</a>
              <a href="/companies/{{$company->id}}/edit" class="mt-2 btn btn-sm btn-info">Edit Company</a>
            </div>
          </div>
          </div>
        </div>
      </div>

      <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
        <div class="align-middle inline-block min-w-full overflow-hidden sm:rounded-lg">
          <div class="border mb-4 text-left p-4">
            <h2 class="mb-4 font-bold text-xl">Projects</h2>
            <ul>
              @foreach($projects as $project)
              <li>
                {{$project->id}} - {{$project->name}}- <a href="/projects/{{$project->id}}">View</a>
              </li>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
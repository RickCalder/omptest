@extends('layouts.app')

@section('content')

<div class="text-gray-900 mt-5">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  <div>
    <h2 class="mt-4 mb-2 h1 font-weight-bold text-center">Companies</h2>
    {{-- @include('components/people-filter') --}}
    
    <div class="d-flex flex-column">
      <div class="my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
        <div class="align-middle inline-block min-w-full overflow-hidden sm:rounded-lg">
          <div class="border text-left p-4">
            <h3 class="h5 font-weight-normal">Find</h3>
            <input class="form-control" type="text" name="company" id="company" required />
            <div class="d-flex justify-content-end mt-2">
              <a href="/companies/create" class="btn btn-sm btn-info">Add Company</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <table class="table table-sm table-striped table-hover mt-4">
      <thead>
        <tr>
          <th>
            Name
          </th>
          <th>
            Email
          </th>
          <th>
            Phone
          </th>
          <th>

          </th>
        </tr>
      </thead>
      <tbody>
        @foreach($companies as $company)
        <tr>
          <td>
            {{ $company->name }}
          </td>
          <td>
            <a href="mailto:{{$company->email}}">
            {{ $company->email }}
            </a>
          </td>
          <td>
            <span><a href="tel:{{$company->phone}}"> {{$company->phone}}</a></span>
          </td>
          <td>
            <a href="/companies/{{$company->id}}" class="btn btn-sm btn-primary">View</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <div class="d-flex justify-end my-4">
    @if ($companies->hasPages())
    {{ $companies->appends(Request::except('page'))->links() }}
    @endif
  </div>
</div>
@endsection

@push('scripts')
<script>
  let path = "{{ route('customer_autocomplete') }}";
  $('#company').typeahead({
      source:  function (query, process) {
          return $.get(path, { query: query }, function (data) {
              return process(data);
          });
      },
      highlighter: function (item, data) {
          var parts = item.split('#'),
              html = '<div class="row">';
              html += '<div class="col-md-2">';
              html += '</div>';
              html += '<div class="col-md-10 pl-0">';
              html += '<span>'+data.name+'</span>';
              html += '</div>';
              html += '</div>';

          return html;
      },
      afterSelect:function(data,value,text){
          window.location.href = "/companies/" + data.id
      },
  })
</script>
@endpush
@extends('layouts.app')

@section('content')

<div class="text-gray-900 mt-5">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  <div>
    <h2 class="mt-4 mb-2 h1 text-center">Add New Company</h2>
    <form class="mt-4" id="create-company-form" action="{{route('companies.store')}}" method="POST">
      @csrf 
      <div class="form-group">
        <label for="name">Name</label>
        <input class="form-control" type="text" name="name" id="name" required />
      </div>
      <div class="form-group">
        <label for="type_id">Company Type</label>
        <select class="form-control" type="text" name="type_id" id="type_id">
          <option value="1">Customer</option>
          <option value="2">Branch</option>
        </select>
      </div>
      <div class="form-group">
        <label for="email">Email</label>
        <input class="form-control" type="email" name="email" id="email" />
      </div>
      <div class="form-group">
        <label for="url">Website</label>
        <input class="form-control" type="text" name="url" id="url" />
      </div>
      <div class="form-group">
        <label for="phone">Phone</label>
        <input class="form-control" type="text" name="phone" id="phone" />
      </div>
      <div class="form-group">
        <label for="parent_company">Parent Company</label>
        <input class="form-control" type="text" name="parent_company_name" id="parent_company" />
        <input type="hidden" name="parent_company" id="parent-company-id"/>
      </div>
      <div class="form-group">
        <label for="note">Notes</label>
        <textarea class="form-control" type="text" name="note" id="note"></textarea>
      </div>
      <div class="form-group">
        <input type="submit" class="btn btn-primary" id="create-service">
      </div>
    </form>
  </div>
</div>
@endsection

@push('scripts')
<script>
  
let path = "{{ route('customer_autocomplete') }}";
  $('#parent_company').typeahead({
        items: 50,
      source:  function (query, process) {
          return $.get(path, { query: query }, function (data) {
              return process(data);
          });
      },
      highlighter: function (item, data) {
          var parts = item.split('#'),
              html = '<div class="row">';
              html += '<div class="col-md-2">';
              html += '</div>';
              html += '<div class="col-md-10 pl-0">';
              html += '<span>'+data.name+'</span>';
              html += '</div>';
              html += '</div>';

          return html;
      },
      afterSelect:function(data,value,text){
          $('#parent-company-id').val(data.id)
      },
  })
</script>
@endpush
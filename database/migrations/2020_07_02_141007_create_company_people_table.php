<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_people', function (Blueprint $table) {
            $table->id();
            $table->integer('old_id');
            $table->string('custom_id')->nullable();
            $table->integer('company_id')->nullable();
            $table->integer('person_id')->nullable();
            $table->string('position')->nullable();
            $table->integer('reports_to')->nullable();
            $table->string('phone')->nullable();
            $table->string('phone1')->nullable();
            $table->string('email')->nullable();
            $table->string('status')->nullable();
            $table->date('hired_on')->nullable();
            $table->boolean('is_payroll')->nullable();
            $table->enum('is_salary', ['false','true','true+evenings','true+weekends','true+evenings+weekends'])->nullable();
            $table->boolean('is_wheniwork')->nullable();
            $table->integer('wheniwork_id')->nullable();
            $table->boolean('is_contractor')->nullable();
            $table->integer('payweb_emp_id')->nullable();
            $table->string('department')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_people');
    }
}

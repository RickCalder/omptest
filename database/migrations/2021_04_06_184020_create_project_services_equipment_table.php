<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectServicesEquipmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_services_equipment', function (Blueprint $table) {
            $table->id();
            $table->integer('old_id');
            $table->integer('project_service_id')->nullable();
            $table->integer('equipment_id')->nullable();
            $table->integer('quantity_requested')->nullable();
            $table->integer('quantity_actual')->nullable();
            $table->integer('hours_requested')->nullable();
            $table->integer('hours_actual')->nullable();
            $table->mediumText('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_services_equipment');
    }
}

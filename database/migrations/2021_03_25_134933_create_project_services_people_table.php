<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectServicesPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_services_people', function (Blueprint $table) {
            $table->id();
            $table->integer('old_id');
            $table->integer('person_id')->nullable();
            $table->integer('company_person_id')->nullable();
            $table->integer('project_service_id')->nullable();
            $table->integer('service_role_id')->nullable();
            $table->integer('payroll_run_id')->nullable();
            $table->time('scheduled_in_at')->nullable();
            $table->time('clocked_in_at')->nullable();
            $table->time('scheduled_out_at')->nullable();
            $table->time('clocked_out_at')->nullable();
            $table->decimal('breaked_for', $precision = 8, $scale = 2)->nullable();
            $table->decimal('travelled_for', $precision = 8, $scale = 2)->nullable();
            $table->integer('wheniwork_id')->nullable();
            $table->enum('is_no_top_up', ['false','true'])->default('false')->nullable();
            $table->enum('is_refused_shift', ['false','true'])->default('false')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_services_people');
    }
}

$table<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_roles', function (Blueprint $table) {
            $table->id();
            $table->integer('old_id');
            $table->string('name');
            $table->float('rank');
            $table->integer('wheniwork_id')->nullable();
            $table->integer('omn_tenant_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_roles');
    }
}

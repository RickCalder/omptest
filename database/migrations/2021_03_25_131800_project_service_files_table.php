<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProjectServiceFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_services_files', function (Blueprint $table) {
            $table->id();
            $table->integer('old_id');
            $table->integer('project_service_id')->nullable();
            $table->string('file_url')->nullable();
            $table->string('name')->nullable();
            $table->string('mime_type');
            $table->integer('owned_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

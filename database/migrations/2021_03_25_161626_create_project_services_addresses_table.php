<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectServicesAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_services_addresses', function (Blueprint $table) {
            $table->id();
            $table->integer('old_id')->nullable();
            $table->integer('project_service_id')->nullable();
            $table->enum('type', ['source','destination'])->nullable();
            $table->mediumText('note')->nullable();
            $table->integer('company_address_id')->nullable();
            $table->integer('company_person_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_services_addresses');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_addresses', function (Blueprint $table) {
            $table->id();
            $table->integer('old_id')->nullable();
            $table->integer('company_id')->nullable();
            $table->integer('address_id')->nullable();
            $table->mediumText('notes')->nullable();
            $table->enum('is_primary',['false','true'])->default('false')->nullable();
            $table->integer('wheniwork_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_addresses');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBuildingToNewAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('new_addresses', function (Blueprint $table) {
            $table->string('unit_number')->nullable();
            $table->string('building_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('new_addresses', function (Blueprint $table) {
            $table->dropColumn('unit_number');
            $table->dropColumn('building_name');
        });
    }
}

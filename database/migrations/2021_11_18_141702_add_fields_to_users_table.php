<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('person_id')->nullable();
            $table->integer('primary_company_id')->nullable();
            $table->string('username')->nullable();
            $table->string('oauth2_google_email')->nullable();
            $table->string('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('person_id');
            $table->dropColumn('primary_company_id');
            $table->dropColumn('username');
            $table->dropColumn('oauth2_google_email');
            $table->dropColumn('status');
        });
    }
}

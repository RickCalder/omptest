<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddArchiveLocationToServiceRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_roles', function (Blueprint $table) {
            $table->boolean('archived')->default(false);
            $table->integer('location_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_roles', function (Blueprint $table) {
            $table->dropColumn('archived');
            $table->dropColumn('location_id');
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddApiFieldsToProjectServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_services', function (Blueprint $table) {
            $table->string('nickname')->nullable();
            $table->date('api_updated')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_services', function (Blueprint $table) {
            $table->dropColumn('nickname');
            $table->dropColumn('api_updated');
        });
    }
}

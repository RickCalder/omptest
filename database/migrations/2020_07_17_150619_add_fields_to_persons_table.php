<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToPersonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('persons', function (Blueprint $table) {
            $table->string('custom_id')->nullable();
            $table->integer('company_id')->nullable();
            $table->string('position')->nullable();
            $table->integer('reports_to')->nullable();
            $table->string('phone_1')->nullable();
            $table->string('status')->nullable();
            $table->date('hired_on')->nullable();
            $table->integer('is_payroll')->nullable();
            $table->enum('is_salary', ['false','true','true+evenings','true+weekends','true+evenings+weekends'])->nullable();
            $table->boolean('is_wheniwork')->nullable();
            $table->integer('wheniwork_id')->nullable();
            $table->boolean('is_contractor')->nullable();
            $table->integer('payweb_emp_id')->nullable();
            $table->string('department')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('persons', function (Blueprint $table) {
            $table->dropColumn('custom_id');
            $table->dropColumn('company_id');
            $table->dropColumn('position');
            $table->dropColumn('reports_to');
            $table->dropColumn('phone_1');
            $table->dropColumn('status');
            $table->dropColumn('hired_on');
            $table->dropColumn('is_payroll');
            $table->dropColumn('is_salary');
            $table->dropColumn('is_wheniwork');
            $table->dropColumn('wheniwork_id');
            $table->dropColumn('is_contractor');
            $table->dropColumn('payweb_emp_id');
            $table->dropColumn('department');
        });
    }
}

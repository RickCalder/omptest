<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persons', function (Blueprint $table) {
            $table->id();
            $table->integer('old_id');
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('common_name')->nullable();
            $table->enum('title',['Mr.', 'Mrs.', 'Ms.', 'Miss'])->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile_phone')->nullable();
            $table->string('phone_email')->nullable();
            $table->text('notes')->nullable();
            $table->date('born_on')->nullable();
            $table->integer('social_insurance_number')->nullable();
            $table->string('drivers_license_number')->nullable();
            $table->integer('emergency_contact_id')->nullable();
            $table->boolean('is_canadian_resident')->nullable();
            $table->boolean('license_abstract_permission')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persons');
    }
}

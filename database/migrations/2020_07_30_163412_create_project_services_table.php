<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_services', function (Blueprint $table) {
            $table->id();
            $table->integer('old_id');
            $table->integer('project_id')->nullable();
            $table->integer('project_quote_id')->nullable();
            $table->integer('service_id')->nullable();
            $table->integer('branch_id')->nullable();
            $table->date('occurs_on')->nullable();
            $table->time('occurs_at')->nullable();
            $table->enum('status', ['pending','incomplete','booked','completed','cancelled','entered'])->nullable();
            $table->string('invoice_number')->nullable();
            $table->integer('created_by')->nullable();
            $table->string('google_calendar_event_id')->nullable();
            $table->integer('minutes_estimate')->nullable();
            $table->integer('minutes_actual')->nullable();
            $table->date('archived_on')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_services');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();
            $table->integer('old_id');
            $table->string('building_name')->nullable();
            $table->text('description')->nullable();
            $table->enum('unit_designator', ['Apartment', 'Suite', 'Unit'])->nullable();
            $table->string('unit')->nullable();
            $table->integer('floor')->nullable();
            $table->integer('civic_number');
            $table->enum('civic_number_suffix',['¼','½','¾','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'])->nullable();
            $table->string('street_name');
            $table->integer('address_street_type_id')->nullable();
            $table->enum('street_direction', ['E','N','NE','NW','S','SE','SW','W'])->nullable();
            $table->integer('rural_route')->nullable();
            $table->string('station')->nullable();
            $table->integer('city_id')->nullable();
            $table->string('postal_code')->nullable();
            $table->text('notes')->nullable();
            $table->decimal('latitude', 10, 7)->nullable();
            $table->decimal('longitude', 10, 7)->nullable();
            $table->integer('omn_tenant_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}

<?php
  
namespace Database\Seeders;
  
use Illuminate\Database\Seeder;
use App\ApiLocation;
use File;
  
class APIlocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ApiLocation::truncate();
  
        $json = File::get("database/data/api.json");
        $locations = json_decode($json);
  
        foreach ($locations as $key => $value) {
            ApiLocation::create([
                'api_id' => $value->api_id,
                "location_id" => $value->location_id,
                "api_key" => $value->api_key,
                "api_url" => $value->api_url
            ]);
        }
    }
}